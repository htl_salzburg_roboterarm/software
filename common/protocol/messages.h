#ifndef MESSAGES_H
#define MESSAGES_H

#include "inttypes.h"
#include "shared_types.h"

enum class msg: uint8_t {
  //Lifecycle messages
  init = 0x01,
  init_ack,
  heartbeat,
  connection_lost,

  //Motion message
  imu_update = 0x10,
  drive_velocity,
  robot_move,
  finger_update,

  //Feedback messages
  brake_update = 0x20,
  vibro_trigger,
  
  //Information messages
  battery_state = 0x30,
  robot_state,
  distance_readings,

  //Control Configuration messages
  config_sensor = 0x40,
  config_data_mode,

  //Robot Setup messages
  enable_drive = 0x50,
  enable_arm,
  confirm_home,
  reset_error,
  request_position,
  transport_mode
};

template<msg msg_code>
struct msg_data {
  static const msg MSG_CODE = msg_code;
};

//Lifecycle messages
struct __packed msg_data_init_t : msg_data<msg::init> {
  uint8_t version_code;
};

struct __packed msg_data_init_ack_t : msg_data<msg::init_ack> {
  bool success = false;
};

struct __packed msg_data_heartbeat_t : msg_data<msg::heartbeat> {
  bool link_up; //Used by receiver to indicate whether the other link is up (PLC or control)
};

struct __packed msg_data_connection_lost_t : msg_data<msg::connection_lost> {
  
};

//Motion messages
struct __packed msg_data_imu_update_t : msg_data<msg::imu_update> {
  int16_t orientation_x;
  int16_t orientation_y;
  int16_t orientation_z;
  int16_t orientation_w;
  uint8_t cal_sys, cal_gyro, cal_accel, cal_mag;
  int16_t accel_x, accel_y, accel_z;
};

struct __packed msg_data_drive_velocity_t : msg_data<msg::drive_velocity> {
  int8_t velocity_x;  //velocity from -100% to 100%
  int8_t velocity_y;  //velocity from -100% to 100%
  int16_t angle;      //angle in degrees
};

struct __packed msg_data_robot_move_t : msg_data<msg::robot_move> {
  int8_t position_x;  //position from -100% to 100%
  int8_t position_y;  //position from -100% to 100%
  int8_t position_z;  //position from -100% to 100%
};

struct __packed msg_data_finger_update_t : msg_data<msg::finger_update> {
  uint8_t finger_levels[5];
};

//Feedback messages
struct __packed msg_data_brake_update_t : msg_data<msg::brake_update> {
  uint8_t brake_levels[5];
};

struct __packed msg_data_vibro_trigger_t : msg_data<msg::vibro_trigger> {
  uint8_t mask;
  VibrateMode mode;
};

//Information messages
struct __packed msg_data_battery_state_t : msg_data<msg::battery_state> {
  bool charging;
  uint8_t level;
};

struct __packed msg_data_robot_state_t : msg_data<msg::robot_state> {
  DriveState drive;
  RobotArmState arm;
};

struct __packed msg_data_distance_readings_t : msg_data<msg::distance_readings> {
  uint8_t distances[16];
};

//Control Configuration Messages
#define MSG_CONFIG_SENSOR_TYPE_MASK  0xF0
#define MSG_CONFIG_SENSOR_LIMIT_MASK 0x0F
#define CONFIG_SENSOR_BEND 0x10
#define CONFIG_SENSOR_FORCE 0x20
#define CONFIG_SENSOR_LOW 0x01
#define CONFIG_SENSOR_HIGH 0x02
#define CONFIG_SENSOR_INDEX_ALL 0xFF
struct __packed msg_data_config_sensor_t : msg_data<msg::config_sensor> {
  uint8_t config_type;
  uint8_t sensor_index;
};

struct __packed msg_data_config_data_mode_t : msg_data<msg::config_data_mode> {
  bool send_imu_data = false;
};

//Robot Setup Messages
struct __packed msg_data_enable_drive_t : msg_data<msg::enable_drive> {
  bool enabled;
};

struct __packed msg_data_enable_arm_t : msg_data<msg::enable_arm> {
  bool enabled;
  bool try_auto_home;
};

struct __packed msg_data_confirm_home_t : msg_data<msg::confirm_home> {
  bool home_flags[5];
};

struct __packed msg_data_reset_error_t : msg_data<msg::reset_error> {
  RobotComponent component;
};

struct __packed msg_data_request_position_t : msg_data<msg::request_position> {

};

struct __packed msg_data_transport_mode_t : msg_data<msg::transport_mode> {
  TransportMode mode;
};

#endif