#ifndef PROTOCOL_H
#define PROTOCOL_H

#include "string.h"
#include "messages.h"

#define PROTOCOL_VERSION 9
#define PROTOCOL_MAX_DATA_SIZE 200
#define PROTOCOL_START_BYTE 0xFF
#define PROTOCOL_HEADER_SIZE 4
#define PROTOCOL_HEARTBEAT_TIME 1000
#define PROTOCOL_TIMEOUT_TIME 3000
#define PROTOCOL_BLE_DEVICE_NAME "HTLSalzburgRobotControl"

class Protocol;
struct registered_callback_t;

typedef void(*msg_callback_t)(uint8_t* msg_data, void* user_data);
typedef void(*default_callback_t)(bool handled, msg msg_code, uint16_t msg_size, uint8_t* msg_data, void* user_data);
typedef void(*write_func_t)(uint8_t* data, uint16_t length, void* user_data);

/*
This class is responsible for processing messages according to the shared protocol.
*/
class Protocol {

  public:
    Protocol();
    ~Protocol();

    //Set the function which is called to send the raw data over the interface
    //This is called when the user invokes send()
    void setWriteFunction(write_func_t write_function, void* write_func_user_data);
    //Register a message callback for a specific message type
    void registerMessageCallback(msg msg_type, msg_callback_t callback, void* user_data = nullptr);
    //Remove a message callback for a message type
    void unregisterMessageCallback(msg msg_type);
    //Register a callback which is called for any message
    void registerDefaultCallback(default_callback_t callback, void* user_data = nullptr);
    //Remove the default callback
    void unregisterDefaultCallback();
    //Pass the raw data from the communication interface to the Protocol class for decoding.
    //Internally this function then calles the registered message callbacks
    void handle(uint8_t* data, uint16_t length);
    //Send a message with the specified code, message size, and payload
    void sendRaw(msg msg_code, uint16_t size, const void* data);

    //Sends a message, based on the payload struct the message code and size are determined automatically
    template<class M>
    void send(M* msg) {
        sendRaw(M::MSG_CODE, (uint16_t)sizeof(*msg), msg);
    }

  private:
    write_func_t _write_func;
    void* _write_func_user_data;
    registered_callback_t* callbacks[256];
    default_callback_t default_callback;
    void* default_callback_user_data;
    
    uint8_t rec_buf[PROTOCOL_MAX_DATA_SIZE];
    uint8_t send_buf[PROTOCOL_MAX_DATA_SIZE];
    int16_t _msgIdx = -PROTOCOL_HEADER_SIZE;
    uint8_t _msgSize = 0;
    uint8_t _msgCode = 0;
    registered_callback_t* current_callback = nullptr;

};

struct registered_callback_t {
  const msg code;
  void* user_data = nullptr;
  msg_callback_t callback = nullptr;

  registered_callback_t(msg _code, void* _user_data, msg_callback_t _callback)
    : code(_code), user_data(_user_data), callback(_callback) {}
};

#endif
