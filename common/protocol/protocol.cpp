#include "protocol.h"

#ifndef ARDUINO
#define PROTOCOL_DISABLE_LOG
#endif

#ifndef PROTOCOL_DISABLE_LOG
#include "ArduinoLog.h"
#endif

//const ProtocolData Protocol::DATA;

Protocol::Protocol() {
  for(uint16_t i = 0;i < 256;i++) {
    callbacks[i] = nullptr;
  }
}

Protocol::~Protocol() {
  for(uint8_t i = 0;i < 256;i++) {
    unregisterMessageCallback(static_cast<msg>(i));
  }
  unregisterDefaultCallback();
}

void Protocol::setWriteFunction(write_func_t write_func, void* write_func_user_data) {
  _write_func = write_func;
  _write_func_user_data = write_func_user_data;
}

void Protocol::registerMessageCallback(msg msg_code, msg_callback_t callback, void* user_data) {
  #ifndef PROTOCOL_DISABLE_LOG
    Log.verbose(F("Registering callback for message: code = %d"), msg_code);
  #endif
  unregisterMessageCallback(msg_code);
  registered_callback_t* rc = new registered_callback_t {
    msg_code, user_data, callback
  };
  callbacks[static_cast<uint8_t>(msg_code)] = rc;
}

void Protocol::unregisterMessageCallback(msg msg_code) {
  uint8_t msg_code_int = static_cast<uint8_t>(msg_code);
  if(callbacks[msg_code_int] != nullptr) {
    delete callbacks[msg_code_int];
    callbacks[msg_code_int] = nullptr;
  }
}

void Protocol::registerDefaultCallback(default_callback_t callback, void* user_data) {
  default_callback = callback;
  default_callback_user_data = user_data;
}

void Protocol::unregisterDefaultCallback() {
  default_callback = nullptr;
  default_callback_user_data = nullptr;
}

void Protocol::handle(uint8_t* data, uint16_t length) {
    for(int i = 0; i < length; i++) {
        uint8_t byte = data[i];
        //Safeguards
        if(_msgIdx < -PROTOCOL_HEADER_SIZE || _msgIdx >= PROTOCOL_MAX_DATA_SIZE) {
          _msgIdx = -PROTOCOL_HEADER_SIZE;
          _msgCode = 0;
          _msgSize = 0;
        }

        if(_msgIdx < -2) { //Process 2 start bytes
            if(byte == PROTOCOL_START_BYTE) {
              _msgIdx++;
            }
        } else if(_msgIdx < -1) { //Process 1 size byte
            _msgSize = byte;
            _msgIdx++;
        } else if(_msgIdx < 0) { //Process message code byte
            _msgCode = byte;
            _msgIdx++;
        } else { //Process message data
            rec_buf[_msgIdx] = byte;
            _msgIdx++;
            if(_msgIdx == _msgSize) {
                //Handle Message
                #ifndef PROTOCOL_DISABLE_LOG
                  Log.verbose(F("MSG received: code = %d, len = %d"), _msgCode, _msgIdx);
                #endif
                current_callback = callbacks[_msgCode];
                bool handled = current_callback != nullptr;
                if(handled) {
                  current_callback->callback(rec_buf, current_callback->user_data);
                }
                if(default_callback != nullptr) {
                  default_callback(handled, static_cast<msg>(_msgCode), _msgSize, rec_buf, default_callback_user_data);
                } else if(!handled) {
                  #ifndef PROTOCOL_DISABLE_LOG
                    Log.verbose(F("No callback for msg_code = %X found"), _msgCode);
                  #endif
                }

                _msgIdx = -PROTOCOL_HEADER_SIZE;
                _msgCode = 0;
                _msgSize = 0;
            }
        }
    }
}

/*
void Protocol::send(msg msg_type, const void* data) {
    uint8_t msg_code = static_cast<uint8_t>(msg_type);
    uint8_t size = Protocol::DATA.message_data_sizes[msg_code];//datasize of msg_code
    sendRaw(msg_code, size, (const uint8_t*) data);
}*/

void Protocol::sendRaw(msg msg_code, uint16_t size, const void* data) {
    uint8_t code = static_cast<uint8_t>(msg_code);

    if(size > PROTOCOL_MAX_DATA_SIZE) {
        return;
    }

    send_buf[0] = PROTOCOL_START_BYTE;
    send_buf[1] = PROTOCOL_START_BYTE;
    send_buf[2] = size;
    send_buf[3] = code;
    uint8_t* buffer = &send_buf[4];
    memcpy(buffer, data, size);

    _write_func(send_buf, size + 4, _write_func_user_data);

    #ifndef PROTOCOL_DISABLE_LOG
      Log.verbose(F("MSG sent: code = %d, len = %d"), code, size);
    #endif 
}

/*
#define MSG(type, data) message_data_sizes[static_cast<uint8_t>(type)] = sizeof(data)

ProtocolData::ProtocolData() {
  //Lifecycle messages
  MSG(msg::init, msg_data_init_t);
  MSG(msg::init_ack, msg_data_init_ack_t);
  MSG(msg::heartbeat, msg_data_heartbeat_t);
  MSG(msg::connection_lost, msg_data_connection_lost_t);
  //Motion messages
  MSG(msg::imu_update, msg_data_imu_update_t);
  MSG(msg::imu_calibrated, msg_data_imu_calibrated_t);
  MSG(msg::imu_update, msg_data_imu_motion_t);
  MSG(msg::finger_update, msg_data_finger_update_t);
  //Feedback messages
  MSG(msg::brake_update, msg_data_brake_update_t);
  MSG(msg::vibro_trigger, msg_data_vibro_trigger_t);
  //Configuration messages
  MSG(msg::config_sensor, msg_data_config_sensor_t);
}
*/
