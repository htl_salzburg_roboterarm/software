#ifndef SHARED_TYPES_H
#define SHARED_TYPES_H

#include "inttypes.h"

enum class FingerType: uint8_t {
    Pinky, Ring, Middle, Index, Thumb, COUNT
};

enum class VibrateMode: uint8_t {
    None, Cancel, Short, Long, Double, NUM_MODES
};

enum class DriveState : uint8_t {
    Off, On, Error
};

enum class RobotArmState : uint8_t {
    Off, HomeDirectionConfirmation, Homing, On, Error
};

enum class RobotComponent : uint8_t {
    Drive, RobotArm
};

enum class TransportMode : uint8_t {
    None, Mode1, Mode2
};

#endif