#ifndef SYSTEM_H
#define SYSTEM_H

#include "inttypes.h"

#define LEVEL_ERROR 1
#define LEVEL_WARNING 2

#define EC_NONE 0x00
#define EC_RATE_DROP 0x01

class SystemManager;
class System;

typedef void(*error_callback_t)(uint8_t error_level, uint8_t error_code);

/*
This class represents a system, which is a modular unit of the application.
Systems are added to a SystemManager, which calls the init() function to
initialize the Systems, and calls the update() function at the specified 
interval.
*/
class System {

    public:
        System(uint32_t update_rate);

        //Implemented by subclass to initialize themselves
        virtual void init(SystemManager* manager) = 0;
        //Implemented by subclasses to execute the cyclic code
        virtual void update() = 0;
        //Called by the SystemManger, checks if the appropriate time has passed
        //and the calls update()
        void update(uint32_t time, bool drop_updates);

    protected:
        uint32_t _update_rate;
        uint32_t _last_update;

};

/*
This class is responsible for managing a set of Systems.
*/
class SystemManager {
    public:
        SystemManager(System** systems, uint16_t num_systems, uint32_t min_update_rate);

        //Initializes all Systems, called from the setup() part of the program
        void init(uint32_t time);
        //Updates the Systems, called from the loop() part of the program
        void update(uint32_t time);

        void notify_error(uint8_t error_code);
        void notify_warning(uint8_t error_code);

        System* getSystem(uint8_t index);

        void setErrorCallback(error_callback_t error_callback);

    private:
        System** _systems;
        uint16_t _num_systems;
        uint32_t _last_update;
        uint32_t _min_update_rate;

        error_callback_t _error_callback = nullptr;

};

#endif