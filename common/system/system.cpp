#include "system.h"

System::System(uint32_t update_rate) : _update_rate(update_rate) {
    
}

void System::update(uint32_t time, bool drop_updates) {
    if(time - _last_update > _update_rate) {
        if(drop_updates) {
            _last_update = time;
        } else {
            _last_update += _update_rate;
        }
        update();
    }
}

SystemManager::SystemManager(System** systems, uint16_t num_systems, uint32_t min_update_rate) :
_systems(systems), _num_systems(num_systems), _min_update_rate(min_update_rate)
{

}

void SystemManager::init(uint32_t time) {
    for(int i = 0; i < _num_systems; i++) {
        _systems[i]->init(this);
    }

    //First update
    _last_update = time;
    for(int i = 0; i < _num_systems; i++) {
        _systems[i]->update(time, true);
    }
}

void SystemManager::update(uint32_t time) {
    /* Update Rate check */
    bool drop_updates = false;
    if(time - _last_update > _min_update_rate) {
        notify_warning(EC_RATE_DROP);
        drop_updates = true;
    }
    _last_update = time;
    /* End Update Rate check */

    for(int i = 0; i < _num_systems; i++) {
        _systems[i]->update(time, drop_updates);
    }
}

System* SystemManager::getSystem(uint8_t index) {
    return _systems[index];
}

void SystemManager::setErrorCallback(error_callback_t callback) {
    _error_callback = callback;
}

void SystemManager::notify_error(uint8_t error_code) {
    if(_error_callback != nullptr) {
        _error_callback(LEVEL_ERROR, error_code);
    }
}

void SystemManager::notify_warning(uint8_t error_code) {
    if(_error_callback != nullptr) {
        _error_callback(LEVEL_WARNING, error_code);
    }
}