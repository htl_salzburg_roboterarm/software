#ifndef PATTERN_H
#define PATTERN_H

#include "inttypes.h"

/*
This class can play a sequence of on/off signals with variable time.
The patterns are represented by arrays which have the following structure:
[t_on1, t_off1, t_on2, toff2, ..., 0]
*/
class PatternPlayer {
    public:
        PatternPlayer();
        //Update the pattern player with the current time, the return value is the current output state (on/off)
        bool update(uint32_t time);
        //Play a new pattern, optionally looping it forever
        void play(const uint32_t* pattern, bool loop = false);
        //Get the current output state
        bool getState();
        //Get the currently played pattern
        const uint32_t* getCurrentPattern();

    private:
        const uint32_t* _currentPattern;
        uint32_t _last_time;
        uint8_t _index;
        bool _state;
        bool _loop;
};

#endif