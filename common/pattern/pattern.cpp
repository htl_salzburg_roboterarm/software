#include "pattern.h"

static const uint8_t INDEX_START = 0xFF;

PatternPlayer::PatternPlayer() {
    _index = 0;
    _last_time = 0;
    _currentPattern = nullptr;
}

void PatternPlayer::play(const uint32_t* pattern, bool loop) {
    _currentPattern = pattern;
    _index = INDEX_START; //Start pattern on next update call
    _loop = loop;
}

bool PatternPlayer::update(uint32_t time) {
    if(_currentPattern != nullptr) {
        if(_index == INDEX_START) {
            //Pattern start
            _index = 0;
            _state = true;
            _last_time = time;
        } else if(time - _last_time > _currentPattern[_index]) {
            _last_time = time;
            _index++;
            _state = !_state; //Invert output state
            if(_currentPattern[_index] == 0) {
                //Pattern end
                if(_loop) {
                    _index = INDEX_START;
                } else {
                    _state = false;
                    _currentPattern = nullptr;
                    _index = 0;
                }
            }
        }
    } else {
        _state = false;
    }
    return _state;
}

bool PatternPlayer::getState() {
    return _state;
}

const uint32_t* PatternPlayer::getCurrentPattern() {
    return _currentPattern;
}