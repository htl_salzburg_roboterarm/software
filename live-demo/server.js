let serialserver = require('p5.serialserver');
let httpserver   = require('http-server');
let liveserver    = require('live-server');
var colors       = require('colors/safe');

serialserver.start(8081);
liveserver.start();

/*
let options = {
    cache: -1
};
let server = httpserver.createServer(options);
let port = 8080;
let host = '0.0.0.0';
let ssl = false;

logger = {
    info: console.log,
    request: function (req, res, error) {
      var date = utc ? new Date().toUTCString() : new Date();
      var ip = argv['log-ip']
          ? req.headers['x-forwarded-for'] || '' +  req.connection.remoteAddress
          : '';
      if (error) {
        logger.info(
          '[%s] %s "%s %s" Error (%s): "%s"',
          date, ip, colors.red(req.method), colors.red(req.url),
          colors.red(error.status.toString()), colors.red(error.message)
        );
      }
      else {
        logger.info(
          '[%s] %s "%s %s" "%s"',
          date, ip, colors.cyan(req.method), colors.cyan(req.url),
          req.headers['user-agent']
        );
      }
    }
};

server.listen(port, host, function () {
    var canonicalHost = host === '0.0.0.0' ? '127.0.0.1' : host,
        protocol      = ssl ? 'https://' : 'http://';

    logger.info([colors.yellow('Starting up http-server, serving '),
      colors.cyan(server.root),
      ssl ? (colors.yellow(' through') + colors.cyan(' https')) : '',
      colors.yellow('\nAvailable on:')
    ].join(''));

    logger.info(('  ' + protocol + canonicalHost + ':' + colors.green(port.toString())));

    if (typeof proxy === 'string') {
      logger.info('Unhandled requests will be served from: ' + proxy);
    }

    logger.info('Hit CTRL-C to stop the server');
});*/