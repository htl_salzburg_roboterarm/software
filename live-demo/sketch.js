'use strict';

let serial;
let serialOptions = {
  baudRate: 115200,
  hupcl: false, //disable dtr, which makes esp32 go into bootloader mode
};
let printSerial = false;
let connected = false;

let roll  = 0.0;
let pitch = 0.0;
let yaw   = 0.0;
let bend  = [0.0, 0.0, 0.0, 0.0, 0.0];
let dist = Array(16);

let last_touch = [false, false, false, false, false];
let touch = [false, false, false, false, false];

let sysCal = 0;
let gyroCal = 0;
let accelCal = 0;
let magCal = 0;

let yaw_offset = 0.0;

let port_select;
let show_select;

const width = 52;
const height = 69;
let sensors;

let show_mode;

function setup()
{  
  let canvas_holder = select('#canvas-holder');
  let canvas = createCanvas(900, 600, WEBGL);
  canvas.parent(canvas_holder);
  canvas.removeAttribute('style');

  select('#refresh-button').mousePressed(onRefreshPressed);
  port_select = select('#port-select');
  select('#connect-button').mousePressed(onConnectPressed);
  select('#disconnect-button').mousePressed(onDisonnectPressed);

  show_select = select('#show-select');
  show_select.changed(onShowModeChanged);
  onShowModeChanged();
  select('#reset-orientation-button').mousePressed(onResetOrientationPressed);

  frameRate(30);

  serial = new p5.SerialPort();
  serial.on('data', gotData);
  serial.on('open', onSerialOpen);
  serial.on('close', onSerialClose);
  serial.on('list', onList);

  onSerialClose(); //Reset connection status label
  serial.list();

  //Position of the sensors (in a y-up coordinate system)
  sensors = [
    //Right side
    {x: +width/2, y: +height/2, a: radians(45)},
    {x: +width/2, y: +height/4, a: radians(0)},
    {x: +width/2, y:         0, a: radians(0)},
    {x: +width/2, y: -height/4, a: radians(0)},
    {x: +width/2, y: -height/2, a: radians(-45)},
    //Back Side
    {x: +width/6, y: -height/2, a: radians(-90)},
    {x: -width/6, y: -height/2, a: radians(-90)},
    //Left side
    {x: -width/2, y: -height/2, a: radians(-135)},
    {x: -width/2, y: -height/4, a: radians(180)},
    {x: -width/2, y:         0, a: radians(180)},
    {x: -width/2, y: +height/4, a: radians(180)},
    {x: -width/2, y: +height/2, a: radians(135)},
    //Top Side
    {x: -width/6, y: +height/2, a: radians(90)},
    {x: +width/6, y: +height/2, a: radians(90)},
  ];
}

function onRefreshPressed() {
  serial.list();
}

function onList(ports) {
  for (const element of port_select.child()) {
    element.remove();
  }
  for (const port of ports) {
    port_select.option(port, port);
  }
}

function onConnectPressed() {
  connectSerial(port_select.value());
}

function onDisonnectPressed() {
  serial.close();
}

function onShowModeChanged() {
  show_mode = show_select.value();
  if(show_mode == "hand") {
    select('#reset-orientation-button').show();
  } else {
    select('#reset-orientation-button').hide();
  }
}

function onResetOrientationPressed() {
  yaw_offset = yaw;
}

function connectSerial(port) {
  console.log("Connecting to port: "+port);
  if(serial) {
    serial.close();
  }
  serial.open(port, serialOptions);
}

function onSerialOpen() {
  select('#connect-div').hide();
  select('#control-div').show();
  select('#connection-label').html("Connected");
  connected = true;
  serial.write("output motion demo\n");
  serial.write("output finger on\n");
  serial.write("output distance on\n");
}

function onSerialClose() {
  select('#connect-div').show();
  select('#control-div').hide();
  select('#connection-label').html("Not Connected");
  connected = false;
}

const t = 40;
const w_hand = 200;
const l_hand = 150;
const l_finger = 100;
const l_finger2 = 50;
const w_finger = 40;

function drawFinger(bend) {
  push();
  translate(0, 0, l_finger/2);
  
  translate(0, 0, -l_finger/2);
  rotateX(map(bend, 0, 100, 0, -PI/2));
  translate(0, 0, l_finger/2);
  box(w_finger, t, l_finger);
  
  translate(0, 0, l_finger/2+l_finger2/2);
  
  translate(0, 0, -l_finger2/2);
  rotateX(map(bend, 0, 100, 0, -PI/2));
  translate(0, 0, l_finger2/2);
  box(w_finger, t, l_finger2);
  
  pop();
}

function drawHand() {
  // Set a new co-ordinate space
  push();

  // Simple 3 point lighting for dramatic effect.
  // Slightly red light in upper right, slightly blue light in upper left, and white light from behind.
  pointLight(255, 200, 200,  400, 400,  500);
  pointLight(200, 200, 255, -400, 400,  500);
  pointLight(255, 255, 255,    0,   0, -500);
  
  rotateY(-radians(yaw-yaw_offset)+PI);
  rotateX(-radians(pitch));
  rotateZ(radians(roll));
  
  box(w_hand, t, l_hand);
  push();
  translate(0, 30, 0);
  box(10, 50, 10);
  pop();
  for(let i = 0; i < 4; i++) {
    let x = map(i, 0, 3, -w_hand/2+w_finger/2, w_hand/2-w_finger/2);
    push();
    translate(x, 0, l_hand/2);
    drawFinger(bend[i]);
    pop();
  }
  
  push();
  translate(w_hand/2, 0, 0);
  rotateY(radians(30));
  drawFinger(bend[4]);
  pop();
  
  pop();
}

function getSensorEndpoint(i) {
  const s = sensors[i];
  return {
    x: s.x + cos(s.a)*dist[i],
    y: s.y + sin(s.a)*dist[i]
  };
}

function drawUltrasonic() {
  push();

  const pixelWidth = 50;
  const scale_factor = pixelWidth / width;
  scale(scale_factor, scale_factor);

  //Draw sensor lines
  noFill();
  stroke(0);
  strokeWeight(1);
  for (let i = 0; i < sensors.length; i++) {
    const s = sensors[i];
    const p = getSensorEndpoint(i);
    line(s.x, -s.y, p.x, -p.y);
  }

  //Draw distance shape fill
  fill('rgba(0%,0%,100%,0.5)');
  noStroke();
  beginShape(TRIANGLE_STRIP);
  for (let i = 0; i <= sensors.length; i++) {
    const s = sensors[i%sensors.length];
    const p = getSensorEndpoint(i%sensors.length);
    vertex(s.x, -s.y);
    vertex(p.x, -p.y);
  }
  endShape();

  //Draw distance shape outline
  noFill();
  stroke(0);
  strokeWeight(1);
  beginShape();
  for (let i = 0; i <= sensors.length; i++) {
    const p = getSensorEndpoint(i%sensors.length);
    vertex(p.x, -p.y);
  }
  endShape();

  //Draw Robot
  fill(200);
  stroke(0);
  strokeWeight(5);
  rect(-width/2, -height/2, width, height);

  pop();
}

function draw()
{  
  background(255);
  fill(255);
  strokeWeight(1);
  stroke(0);

  if(show_mode == "hand") {
    drawHand();
  } else if(show_mode == "distance") {
    drawUltrasonic();
  }
  //Feedback functions
  const bend_vibro_limit = 70;
  let vibro_mask = "";
  for (let i = 0; i < 5; i++) {
    touch[i] = bend[i] > bend_vibro_limit;
    if(touch[i] && !last_touch[i]) {
      vibro_mask += "1";
    } else {
      vibro_mask += "0";
    }
    last_touch[i] = touch[i];
  }

  if(vibro_mask != "00000" && connected) {
    //console.log("Vibro: "+vibro_mask);
    //serial.write("vibro short "+vibro_mask+"\n");
  }
}

function handleLine(incoming) {
  if(incoming.startsWith("[STARTUP]")) {
    console.log("Receiver started!");
    serial.write("output motion demo\n");
    serial.write("output finger on\n");
    serial.write("output distance on\n");
    return;
  }

  if(!incoming.startsWith("[DATA]")) {
    print(incoming);
    return;
  }
  incoming = incoming.substring("[DATA]".length);
  
  if (printSerial) {
    print(incoming);
  }
  
  if ((incoming.length > 1))
  {
    var list = split(incoming, ";");
    if ( (list.length > 0) && (list[0] == "O") ) //Orientation
    {
      let tmp_roll  = float(list[3]); // Roll = Z
      let tmp_pitch = float(list[2]); // Pitch = Y 
      let tmp_yaw   = float(list[1]); // Yaw/Heading = X
      if(tmp_roll == 0 && tmp_pitch == 0 && tmp_yaw == 0) {
        print("All zero");
      } else {
        roll = tmp_roll;
        yaw = tmp_yaw;
        pitch = tmp_pitch;
      }
    }
    if ( (list.length > 0) && (list[0] == "C") ) //Calibration
    {
      sysCal   = int(list[1]);
      gyroCal  = int(list[2]);
      accelCal = int(list[3]);
      magCal   = int(trim(list[4]));
    }
    if ( (list.length > 0) && (list[0] == "F") ) //Fingers
    {
      for (let i = 0; i < 5; i++) {
        bend[i] = float(list[i+1]);    
      }
    }
    if ( (list.length > 0) && (list[0] == "D") ) //Fingers
    {
      for (let i = 0; i < 16; i++) {
        dist[i] = float(list[i+1]); 
      }
    }

  }
}

let data_buf = "";
function gotData() {
  while(serial.available() > 0) {
    let char = serial.readChar();
    if(char == '\r') {
      continue;
    } else if(char == '\n') {
      if(data_buf.length > 0) {
        handleLine(data_buf);
        data_buf = "";
      }
    } else {
      data_buf += char;
    }
  }
}