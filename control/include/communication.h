#ifndef COMMUNICATION_H
#define COMMUNICATION_H

#include "system.h"
#include "shared_data.h"
#include "protocol.h"
#include "movement.h"

#include "Arduino.h"
#include "BluefruitConfig.h"
#include "Adafruit_BLE.h"
#include "SPI.h"
#include "Adafruit_BluefruitLE_SPI.h"

static const uint32_t UPDATE_HZ = 50;
static const uint32_t COMM_UPDATE_RATE_US = 1000000 / UPDATE_HZ;
#define MOVE_SEND_INTERVAL 5
#define IMU_SEND_INTERVAL 10
#define MOVE_REQUEST_INTERVAL 10

#define TIMEOUT_TICKS ( PROTOCOL_TIMEOUT_TIME / (COMM_UPDATE_RATE_US / 1000) )
#define HEARTBEAT_TICKS ( PROTOCOL_HEARTBEAT_TIME / (COMM_UPDATE_RATE_US / 1000) )

static const int16_t ALIGN_THRESHOLD = 9;
static const uint16_t ALIGN_TIME = 2 * UPDATE_HZ;

static const int16_t Z_DELTA = 5;

//Set to true to do a factory reset of the BLE module at boot, this increases the boot time
//This should only be needed on the very first boot or if the device is misconfigured
static const bool do_ble_factory_reset = false;

/*
This system is responsible for the communication over bluetooth low energy:
1) Forwarding raw data to / from the Protocol object
2) Handling the initialization / heartbeat sequence
3) Sending and receiving messages and forwarding the data to the SharedData struct
*/
class CommunicationSystem : public System {
    public:
        CommunicationSystem(SharedData* sharedData);

        //System implementation
        void init(SystemManager* manager) override;
        void update() override;

        void requireAlignment();

        //Protocol implementation
        static void handleWrite(uint8_t* data, uint16_t length, void* user_data);

        //Lifecycle message handlers
        static void handleInitMsg(uint8_t* msg_data, void* user_data);
        static void handleHeartbeatMsg(uint8_t* msg_data, void* user_data);

        //Message handlers
        static void handleVibroMsg(uint8_t* msg_data, void* user_data);
        static void handleBrakeMsg(uint8_t* msg_data, void* user_data);
        static void handleConfigDataModeMsg(uint8_t* msg_data, void* user_data);
        static void handleBatteryStateMsg(uint8_t* msg_data, void* user_data);
        static void handleRobotStateMsg(uint8_t* msg_data, void* user_data);
        static void handleRobotMoveMsg(uint8_t* msg_data, void* user_data);

    private:
        SharedData* _sharedData;
        Adafruit_BluefruitLE_SPI _ble;
        Protocol _protocol;
        uint8_t rx_buffer[256];
        
        uint16_t _heartBeatCounter = 0; //Number of update cycles since the last heartbeat was sent
        uint16_t _timeOutCounter = 0;   //Number of update cycles since the last heartbeat was received

        bool _sendImuData = false;      //Should the full imu data be sent?

        uint16_t _moveSendCounter = 0;  //Number of update cycles since the last movement command update
        uint16_t _imuSendCounter = 0;   //Number of update cycles since the last full imu update
        uint16_t _moveRequestCounter = 0;   //Number of update cycles since the last request of the current robot position

        uint16_t _alignCountDown = ALIGN_TIME;
        OperationMode _lastOpMode = OperationMode::none;

        int16_t _z_pos = 0;
};

#endif