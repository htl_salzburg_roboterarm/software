#ifndef FEEDBACK_H
#define FEEDBACK_H

#include "system.h"
#include "shared_data.h"
#include "Adafruit_MCP23017.h"
#include "pattern.h"

#define FEEDBACK_SYSTEM_UPDATE_RATE 100000 //100ms cycle time, 10Hz

//Vibration patterns, times in milliseconds, all patterns have to end with 0
const uint32_t PATTERN_SHORT[] = {500, 0}; //500ms on, end
const uint32_t PATTERN_LONG[] = {2000, 0}; //2000ms on, end
const uint32_t PATTERN_DOUBLE[] = {500, 500, 500, 0}; //500ms on, 500ms off, 500ms on, end

/*
This system is responsible for controlling the feedback (vibration motors and electrostatic brakes)
*/
class FeedbackSystem : public System {
    public:
        FeedbackSystem(SharedData* sharedData);

        //System implementation
        void init(SystemManager* systemManager) override;
        void update();

    private:
        SharedData* _sharedData;
        Adafruit_MCP23017 _mcp;
        uint16_t _mcpPortState = 0;
        PatternPlayer _patternPlayers[5];
};

#endif