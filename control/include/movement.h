#ifndef MOVEMENT_H
#define MOVEMENT_H

#include "system.h"
#include "shared_data.h"
#include "protocol.h"
#include "Adafruit_Sensor.h"
#include "Adafruit_BNO055.h"
#include "inttypes.h"

#define IMU_UPDATE_RATE_US 20000 //50Hz
#define IMU_UPDATE_RATE_MS (IMU_UPDATE_RATE_US/1000)
#define IMU_UPDATE_RATE (IMU_UPDATE_RATE_US/1000000.0)

#define IMU_SENSOR_ID 55
#define IMU_ADDRESS BNO055_ADDRESS_A

/*
This system is responsible for retrieving and processing the movement data from the BNO055 sensor.
It generates a movement command with a value in percent, which tells the robot how much to move in the x/y/z directions.
*/
class MovementSystem : public System {
    public:
        MovementSystem(SharedData* sharedData);
        ~MovementSystem();

        //System implementation
        void init(SystemManager* manager) override;
        void update() override;
    private:
        SharedData* _sharedData;
        Adafruit_BNO055 _bno;
        uint32_t _last_update;
        uint32_t _calib_count;
};

#endif