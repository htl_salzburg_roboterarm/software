#ifndef DISPLAY_H
#define DISPLAY_H

#include "system.h"
#include "shared_data.h"
#include "Adafruit_ILI9341.h"
#include "Adafruit_STMPE610.h"
#include "SdFat.h"                // SD card & FAT filesystem library
#include "Adafruit_ImageReader.h" // Image-reading functions

#define DISPLAY_UPDATE_RATE_US 50000 //20Hz

#define STMPE_CS 6
#define TFT_CS   13
#define TFT_DC   10
#define SD_CS    5

//Utility rectangle class
struct Rect {
    int16_t x, y, w, h;

    void fill(Adafruit_SPITFT &tft, uint16_t color) const {
        tft.fillRect(x, y, w, h, color);
    }

    void draw(Adafruit_SPITFT &tft, uint16_t color) const {
        tft.drawRect(x, y, w, h, color);
    }

    void drawCenteredText(Adafruit_SPITFT &tft, const char* text) const {
        int16_t tx, ty;
        uint16_t tw, th;
        tft.getTextBounds(text, 0, 0, &tx, &ty, &tw, &th);
        tft.setCursor(x + (w - tw) / 2, y + (h - th) / 2);
        tft.print(text);
    }

    bool inside(int16_t px, int16_t py) const {
        if(px < x || px > x+w) return false;
        if(py < y || py > y+h) return false;
        return true;
    }
};

//This struct contains all values which are monitored by the display and are updated when they change
struct DisplayValues {
    enum class Connection {
        disconnected,
        receiver_connected,
        connected
    } connection;
    OperationMode operation_mode;
    uint32_t battery_level;
    DriveState drive_state;
    RobotArmState arm_state;
    FingerCalibrationState calibration_state;
    bool hand_closed;

    uint8_t align_countdown;
    bool align_positions;
    bool imu_calibrated;

    uint8_t robot_home_index = 255;
    bool flag = false;
};

/*
This system is responsible for displaying the graphical user interface on the touch display
*/
class DisplaySystem : public System {
    public:
        DisplaySystem(SharedData* sharedData);

        //Called from main.cpp to display the boot splash screen
        void preBoot();
        //System implementation
        void init(SystemManager* manager) override;
        void update() override;

    private:
        SharedData* _sharedData;
        Adafruit_ILI9341 _tft;
        Adafruit_STMPE610 _ts;
        SdFat _sd;
        Adafruit_ImageReader _reader;

        Adafruit_Image _image_conn_on;
        Adafruit_Image _image_conn_off;
        Adafruit_Image _image_conn_recv;

        DisplayValues _last_values;        //Contains the values which are currently displayed
        DisplayValues _current_values;     //Contains the values which should now be displayed
        bool _update_all_values = true;
        bool _update_flag_value = true;
        bool _update_page = true;
        bool _update_alignment_data = true;

        bool _showImuAlignment = false;

        TS_Point _point;
        bool _last_pressed = false;

        uint32_t measured_vbat_average = 0;
        uint32_t last_vbat = 0;

        bool _robot_home_flags[5];

        TransportMode _transport_mode = TransportMode::Mode1;

        void handleTouch(TS_Point p, bool justPressed);

        void setInfoCursor(uint8_t line);
        void clearInfoLine(uint8_t line);
        void advanceCursor(const char* text);

        void drawAlignmentMarker(int16_t x, int16_t y, uint16_t color);

};

#endif