#ifndef SHARED_DATA_H
#define SHARED_DATA_H

#include "inttypes.h"
#include "shared_types.h"

//Current operation mode of the control
enum class OperationMode {
    none,
    drive,
    move,
    info
};

//Current connection status
enum class ConnectionStatus {
    //General connection states
    disconnected = 0x01,
    connecting,
    connected,

    //Connetion error states
    error_version_mismatch = 0xE1,
    error_timeout,
    error_unknown
};

//Data which is associated with each finger
struct Finger {
    uint16_t force = 0;
    uint8_t bendLevel = 0;
    uint8_t brakeLevel = 0;
    VibrateMode vibrateMode = VibrateMode::None;
    bool brake = false;
    bool vibrate = false;
};

//Calibration state of the bend sensors
enum class FingerCalibrationState {
    CalibrationDone,
    CalibrationOpenHand,
    CalibrationCloseHand,
    CalibrationWorking
};

//Struct which contains the data for all 5 fingers
struct FingerData {
    Finger fingers[static_cast<uint8_t>(FingerType::COUNT)];
    FingerCalibrationState calibration_state = FingerCalibrationState::CalibrationDone;
    bool cmd_advance_calibration = false;
    bool hand_closed;
};

/* Imu motion / orientation data structs */
struct ImuMove {
    int16_t x, y;
};

struct ImuOrientation {
    double yaw, pitch, roll;
};

struct ImuAcceleration {
    double x, y, z;
};

struct ImuCalibration {
    uint8_t sys, gyro, accel, mag;
    uint8_t data[22];
};

struct ImuData {
    ImuMove move;
    ImuOrientation orientation;
    ImuAcceleration acceleration;
    ImuCalibration calibration;
    bool available = false;
};

//Struct which contains the state of the robot
struct RobotData {
    uint8_t battery_level = 0;
    bool charging = false;
    DriveState drive_state = DriveState::Off;
    RobotArmState arm_state = RobotArmState::Off;
    bool align_positions = true;
    //Default to unreachable positions, this way at least one positions update is needed
    int16_t alignment_x = 255, alignment_y = 255;
    uint16_t align_countdown;

    bool cmd_enable_drive = false;
    bool cmd_enable_arm = false;
    bool cmd_disable_drive = false;
    bool cmd_disable_arm = false;
    bool cmd_reset_error_drive = false;
    bool cmd_reset_error_arm = false;
    TransportMode cmd_transport_mode = TransportMode::None;
    bool cmd_arm_down = false;
    bool cmd_arm_up = false;
    bool* home_flags = nullptr;
};

//SharedData struct, contains data which is used by the systems to communicate with each other
struct SharedData {
    ConnectionStatus connection_status = ConnectionStatus::disconnected;
    bool robot_link_up = false;
    OperationMode operation_mode = OperationMode::info;
    ImuData imu;
    RobotData robot;
    FingerData finger_data;
};

#endif