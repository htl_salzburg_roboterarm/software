#ifndef SENSORS_H
#define SENSORS_H

#include "system.h"
#include "shared_data.h"
#include "Arduino.h"

#define MCP3208_SPI_CS 11

#define SENSOR_SYSTEM_UPDATE_RATE 20000 //50Hz

/*
This system is responsible for reading and processing the values from the force and flex sensors
*/
class SensorSystem : public System {
    public:
        SensorSystem(SharedData* sharedData);

        //System implementation
        void init(SystemManager* systemManger) override;
        void update() override;

    private:
        SharedData* _sharedData;
        uint16_t _rawBend[5];
        uint16_t _rawForce[5];

        uint8_t _sendUpdateCounter = 0;

        uint16_t _raw_bend_average[5];
        uint8_t _raw_output_counter = 0;

        //Read from a channel of the external ADC
        uint16_t readADC(uint8_t channel);
};

#endif