#include "sensors.h"
#include "SPI.h"
#include "ArduinoLog.h"

//#define READ_BEND_INTERNAL
//#define OUTPUT_RAW_VALUES_INTERVAL 100

//Pin mappings 0 = pinky, 5 = thumb
#ifdef READ_BEND_INTERNAL
const uint8_t BEND_CHANNEL_MAPPING[] = {
    A0, A1, A2, A3, A4
};
#else
const uint8_t BEND_CHANNEL_MAPPING[] = {
    4, 3, 2, 1, 0
};
#endif

const uint8_t FORCE_PIN_MAPPING[] = {
    A0, A1, A2, A3, A4
};

//Hand opened/closed hysteresis values
const uint8_t bend_level_hand_open = 30;
const uint8_t bend_level_hand_closed = 70;

//Minimum and maximum bend values for each finger
uint16_t BEND_CALIB_DATA[][2] = {
    {1676, 779},
    {1617, 808},
    {1590, 939},
    {1648, 1007},
    {1771, 1361}
};

SensorSystem::SensorSystem(SharedData* sharedData) : System(SENSOR_SYSTEM_UPDATE_RATE), _sharedData(sharedData) {

}

/* Initialization of the system */
void SensorSystem::init(SystemManager* systemManger) {
    //Initialize sensor SPI
    pinMode(MCP3208_SPI_CS, OUTPUT);
    digitalWrite(MCP3208_SPI_CS, HIGH);
    SPI.begin();
}

/* Update the system */
void SensorSystem::update() {
    if(_sharedData->finger_data.cmd_advance_calibration) {
        _sharedData->finger_data.cmd_advance_calibration = false;
        //Button on the display was pressed, switch to the next calibration step
        switch (_sharedData->finger_data.calibration_state)
        {
        case FingerCalibrationState::CalibrationDone:      //done->open
            _sharedData->finger_data.calibration_state = FingerCalibrationState::CalibrationOpenHand;
            break;
        case FingerCalibrationState::CalibrationOpenHand:  //open->close
            _sharedData->finger_data.calibration_state = FingerCalibrationState::CalibrationCloseHand;
            break;
        case FingerCalibrationState::CalibrationCloseHand: //close->done
            _sharedData->finger_data.calibration_state = FingerCalibrationState::CalibrationDone;
            break;
        default:
            break;
        }
    }
    
    #ifndef READ_BEND_INTERNAL
    //Read the bend values from the external adc
    for (int i = 0; i < 5; i++) {
        _rawBend[i] = readADC(BEND_CHANNEL_MAPPING[i]);
    }
    #endif
    
    //Variable to calculate the average bend level of all fingers combined
    float average_bend_level = 0;
    for (int i = 0; i < 5; i++)
    {
        #ifdef READ_BEND_INTERNAL
        _rawBend[i] = analogRead(BEND_CHANNEL_MAPPING[i]);
        #endif

        //Force sensores are not yet used, because the es brakes are unused
        //_rawForce[i] = analogRead(FORCE_PIN_MAPPING[i]);

        //Filter sensor values
        static const float alpha = 0.1;
        _raw_bend_average[i] = _raw_bend_average[i]*alpha + _rawBend[i]*(1-alpha);
        
        //Map the raw ADC values to a value from 0% (open) to 100% (closed)
        long bend = map(_raw_bend_average[i], BEND_CALIB_DATA[i][0], BEND_CALIB_DATA[i][1], 0L, 100L);
        bend = constrain(bend, 0, 100);
        //Map the force sensor values to the range 0 - 255
        uint8_t force = map(_rawForce[i], 0, 1024, 0, 255);

        //Sum the bend levels
        average_bend_level += bend;

        Finger* finger = &_sharedData->finger_data.fingers[i];

        switch (_sharedData->finger_data.calibration_state)
        {
        case FingerCalibrationState::CalibrationDone:
            //Calibration done, output scaled values
            finger->bendLevel = (uint8_t) bend;
            finger->force = force;
            break;
        case FingerCalibrationState::CalibrationOpenHand:
            //Set the raw bend value which is mapped to 0%
            BEND_CALIB_DATA[i][0] = _raw_bend_average[i];
            break;
        case FingerCalibrationState::CalibrationCloseHand:
            //Setthe raw bend value which is mapped to 100%
            BEND_CALIB_DATA[i][1] = _raw_bend_average[i];
            break;
        default:
            break;
        }
        
    }

    #ifdef OUTPUT_RAW_VALUES_INTERVAL
    //Output the raw bend sensor values
    _raw_output_counter++;
    if(_raw_output_counter > OUTPUT_RAW_VALUES_INTERVAL) {
        _raw_output_counter = 0;
        Serial.println("Finger Bend raw values (filtered):");
        for (int i = 0; i < 5; i++)
        {
            Serial.printf("[Finger %d] = %d\n", i, _raw_bend_average[i]);
        }
    }
    #endif

    //Calculate the average bend level and update the hand_closed flag
    average_bend_level /= 5;
    if(average_bend_level < bend_level_hand_open) {
        _sharedData->finger_data.hand_closed = false;
    } else if(average_bend_level > bend_level_hand_closed) {
        _sharedData->finger_data.hand_closed = true;
    }

}

/* Reads from a channel of the external ADC */
uint16_t SensorSystem::readADC(uint8_t channel) {
    //Start the SPI transaction and set chip-select to LOW
    SPI.beginTransaction(SPISettings(1600000, MSBFIRST, SPI_MODE0));
    digitalWrite(MCP3208_SPI_CS, LOW);

    uint16_t cmd = 0;
    uint16_t data = 0;
    cmd |= bit(10); //Start bit
    cmd |= bit(9);  //Single ended bit
    cmd |= (channel & 0b111) << 6; //Set channel

    SPI.transfer((cmd >> 8) & 0xFF); //Transfer high byte of command
    data |= (SPI.transfer(cmd & 0xFF) & 0x0F) << 8; //Transfer low byte of command and receive high byte of result
    data |= SPI.transfer(0x00); //Transfer 0 and receive low byte of result

    //Disable the adc with chip-select and end the transaction
    digitalWrite(MCP3208_SPI_CS, HIGH);
    SPI.endTransaction();

    return data;
}