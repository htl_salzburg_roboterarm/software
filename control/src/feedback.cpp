#include "feedback.h"
#include "Arduino.h"
#include "ArduinoLog.h"

#define MCP_ADDRESS 0

#define FINGER_THUMB 0
#define FINGER_INDEX 1
#define FINGER_MIDDLE 2
#define FINGER_RING 3
#define FINGER_PINKY 4

#define MAPPING_STRIP 0
#define MAPPING_VIBRO 1

#define HV_ENABLE_PIN 12

#define FORCE_RELEASE_THRESHOLD 300

//Mapping table for each finger, strip and vibro
const uint8_t FEEDBACK_PIN_MAPPING[][2] = {
    {12, 3}, //Pinky
    {11, 4}, //Ring
    {10, 5}, //Middle
    { 9, 6}, //Index
    { 8, 7}  //Thumb
};

FeedbackSystem::FeedbackSystem(SharedData* sharedData) : System(FEEDBACK_SYSTEM_UPDATE_RATE), _sharedData(sharedData) {

}

/* Initialization of the system */
void FeedbackSystem::init(SystemManager* systemManager) {
    pinMode(HV_ENABLE_PIN, OUTPUT);
    digitalWrite(HV_ENABLE_PIN, LOW);
    //initialize MCP23017 I/O expander
    _mcp.begin(MCP_ADDRESS);
    //setup all outputs
    for(int i = 0; i < 5; i++) {
        _mcp.pinMode(FEEDBACK_PIN_MAPPING[i][MAPPING_STRIP], OUTPUT);
        _mcp.pinMode(FEEDBACK_PIN_MAPPING[i][MAPPING_VIBRO], OUTPUT);
    }

    //Turn off all outputs
    _mcpPortState = 0;
    _mcp.writeGPIOAB(_mcpPortState);
}

/* Update the system */
void FeedbackSystem::update() {
    uint16_t newPortState = 0;
    uint32_t time = millis();

    //For each finger
    for(int i = 0; i < 5; i++) {
        Finger* finger = &_sharedData->finger_data.fingers[i];

        //Process vibrate mode commands
        switch (finger->vibrateMode)
        {
        case VibrateMode::Cancel: _patternPlayers[i].play(nullptr); break;
        case VibrateMode::Short: _patternPlayers[i].play(PATTERN_SHORT); break;
        case VibrateMode::Long: _patternPlayers[i].play(PATTERN_LONG); break;
        case VibrateMode::Double: _patternPlayers[i].play(PATTERN_DOUBLE); break;
        default: break;
        }
        finger->vibrateMode = VibrateMode::None; //Reset vibrate mode command
        finger->vibrate = _patternPlayers[i].update(time); //Update the pattern player to get the current state

        //Brake switching
        if(finger->bendLevel >= finger->brakeLevel) {
            finger->brake = true;
        }

        //Release the brake if the user wants to open the hand
        if(finger->force >= FORCE_RELEASE_THRESHOLD) {
            finger->brake = false;
        }

        //Set output states
        if(finger->brake) {
            newPortState |= bit(FEEDBACK_PIN_MAPPING[i][MAPPING_STRIP]);
        }
        if(finger->vibrate) {
            newPortState |= bit(FEEDBACK_PIN_MAPPING[i][MAPPING_VIBRO]);
        }
    }

    //Write outputs if they have changed
    if(newPortState != _mcpPortState) {
        _mcpPortState = newPortState;
        _mcp.writeGPIOAB(_mcpPortState);
    }

}