#include "communication.h"
#include "system_ids.h"
#include "error.h"
#include "ArduinoLog.h"

CommunicationSystem::CommunicationSystem(SharedData* sharedData) : System(COMM_UPDATE_RATE_US), _sharedData(sharedData), _ble(BLUEFRUIT_SPI_CS, BLUEFRUIT_SPI_IRQ, BLUEFRUIT_SPI_RST)
{
    //Register write-function and message callbacks
    _protocol.setWriteFunction(handleWrite, this);
    _protocol.registerMessageCallback(msg::init, handleInitMsg, this);
    _protocol.registerMessageCallback(msg::heartbeat, handleHeartbeatMsg, this);
    _protocol.registerMessageCallback(msg::vibro_trigger, handleVibroMsg, this);
    _protocol.registerMessageCallback(msg::brake_update, handleBrakeMsg, this);
    _protocol.registerMessageCallback(msg::battery_state, handleBatteryStateMsg, this);
    _protocol.registerMessageCallback(msg::robot_state, handleRobotStateMsg, this);
    _protocol.registerMessageCallback(msg::config_data_mode, handleConfigDataModeMsg, this);
    _protocol.registerMessageCallback(msg::robot_move, handleRobotMoveMsg, this);
}

/* Message callbacks */
void CommunicationSystem::handleBrakeMsg(uint8_t* msg_data, void* user_data) {
    CommunicationSystem* comm = (CommunicationSystem*) user_data;
    msg_data_brake_update_t* msg = (msg_data_brake_update_t*) msg_data;
    for (int i = 0; i < 5; i++)
    {
        comm->_sharedData->finger_data.fingers[i].brakeLevel = msg->brake_levels[i];
    }
    
}

void CommunicationSystem::handleVibroMsg(uint8_t* msg_data, void* user_data) {
    CommunicationSystem* comm = (CommunicationSystem*) user_data;
    msg_data_vibro_trigger_t* msg = (msg_data_vibro_trigger_t*) msg_data;
    for (int i = 0; i < 5; i++)
    {
        //If the corresponding bit in the mask is set, set the vibrate mode of the finger
        if((msg->mask >> i) & 1) {
            comm->_sharedData->finger_data.fingers[i].vibrateMode = msg->mode;
        }
    }
}

void CommunicationSystem::handleConfigDataModeMsg(uint8_t* msg_data, void* user_data) {
    CommunicationSystem* comm = (CommunicationSystem*) user_data;
    msg_data_config_data_mode_t* msg = (msg_data_config_data_mode_t*) msg_data;
    comm->_sendImuData = msg->send_imu_data;
    Log.notice("Output Data Mode: %T", comm->_sendImuData);
}

void CommunicationSystem::handleBatteryStateMsg(uint8_t* msg_data, void* user_data) {
    CommunicationSystem* comm = (CommunicationSystem*) user_data;
    msg_data_battery_state_t* msg = (msg_data_battery_state_t*) msg_data;
    comm->_sharedData->robot.battery_level = msg->level;
    comm->_sharedData->robot.charging = msg->charging;
}

void CommunicationSystem::handleRobotStateMsg(uint8_t* msg_data, void* user_data) {
    CommunicationSystem* comm = (CommunicationSystem*) user_data;
    msg_data_robot_state_t* msg = (msg_data_robot_state_t*) msg_data;
    comm->_sharedData->robot.drive_state = msg->drive;
    comm->_sharedData->robot.arm_state = msg->arm;
}

void CommunicationSystem::handleRobotMoveMsg(uint8_t* msg_data, void* user_data) {
    CommunicationSystem* comm = (CommunicationSystem*) user_data;
    msg_data_robot_move_t* msg = (msg_data_robot_move_t*) msg_data;
    comm->_sharedData->robot.alignment_x = msg->position_x;
    comm->_sharedData->robot.alignment_y = msg->position_y;
    comm->_z_pos = msg->position_z;
}

/* Protocol Interface Implementation */
void CommunicationSystem::handleWrite(uint8_t* data, uint16_t length, void* user_data) {
    CommunicationSystem* comm = (CommunicationSystem*) user_data;
    comm->_ble.write(data, length);
}

/* Lifecycle message callbacks */
void CommunicationSystem::handleInitMsg(uint8_t* msg_data, void* user_data) {
    CommunicationSystem* comm = (CommunicationSystem*) user_data;
    msg_data_init_t* data = (msg_data_init_t*) msg_data;
    msg_data_init_ack_t ack_msg;
    //Check if the protocol version matches
    if(data->version_code == PROTOCOL_VERSION) {
        Log.notice(F("Connection to receiver established"));
        comm->_sharedData->connection_status = ConnectionStatus::connected;
        comm->_heartBeatCounter = 0;
        comm->_timeOutCounter = 0;
        ack_msg.success = true;
    } else {
        Log.notice(F("Protocol version mismatch"));
        comm->_sharedData->connection_status = ConnectionStatus::error_version_mismatch;
        ack_msg.success = false;
    }
    comm->_protocol.send(&ack_msg);
}

void CommunicationSystem::handleHeartbeatMsg(uint8_t* msg_data, void* user_data) {
    CommunicationSystem* comm = (CommunicationSystem*) user_data;
    msg_data_heartbeat_t* msg = (msg_data_heartbeat_t*) msg_data;
    //Reset the timeout when a heartbeat is received
    comm->_timeOutCounter = 0;
    //Check if the receiver is connected to the robot
    comm->_sharedData->robot_link_up = msg->link_up;
}

/* Initialization of the System */
void CommunicationSystem::init(SystemManager* manager) {
    //Initialize BLE module
    Log.notice("Initializing Bluefruit");
    if (!_ble.begin(VERBOSE_MODE))
    {
        manager->notify_error(EC_BLUEFRUIT_NOT_DETECTED);
    }
    Log.notice("Bluefruit initialized");

    if(do_ble_factory_reset) {
        //Execute a factory reset
        _ble.factoryReset();
        Log.notice("Performed Bluefruit factory reset");
        //Set the device name
        if(!_ble.atcommand("AT+GAPDEVNAME", PROTOCOL_BLE_DEVICE_NAME)) {
            Log.warning("Failed to set BLE device name");
        }
        //Restart the device so the changes take effect
        _ble.reset();
    }

    //Output information about the BLE module
    Log.notice("-- BLE Info --");
    _ble.info();
    Log.notice("--------------");

    //Switch the module to data mode
    _ble.setMode(BLUEFRUIT_MODE_DATA);
}

/* Update the system */
void CommunicationSystem::update() {
    //Handle incoming messages
    uint16_t available;
    while((available = _ble.available())) {
        uint16_t length = _ble.readBytes(rx_buffer, min(available, sizeof(rx_buffer)));
        _protocol.handle(rx_buffer, length);
    }

    //If we are not connected to the receiver, there is also no connection to the robot
    if(_sharedData->connection_status != ConnectionStatus::connected) {
        _sharedData->robot_link_up = false;
        _sendImuData = false;
        return;
    }

    //Send heartbeats
    _heartBeatCounter++;
    if(_heartBeatCounter >= HEARTBEAT_TICKS) {
        _heartBeatCounter = 0;
        msg_data_heartbeat_t msg;
        _protocol.send(&msg);
    }

    //Check for timeouts
    _timeOutCounter++;
    if(_timeOutCounter >= TIMEOUT_TICKS) {
        _timeOutCounter = 0;
        Log.notice("Connection timed out");
        _sharedData->connection_status = ConnectionStatus::error_timeout;
    }

    //Send robot control messages (msg::enable_arm, msg::enable_drive or msg::error_reset)
    if(_sharedData->robot.cmd_disable_arm) {
        msg_data_enable_arm_t msg;
        msg.enabled = false;
        msg.try_auto_home = false;
        _protocol.send(&msg);
        _sharedData->robot.cmd_disable_arm = false;
    } else if(_sharedData->robot.cmd_enable_arm) {
        msg_data_enable_arm_t msg;
        msg.enabled = true;
        msg.try_auto_home = true;
        _protocol.send(&msg);
        _sharedData->robot.cmd_enable_arm = false;
    }
    if(_sharedData->robot.cmd_disable_drive) {
        msg_data_enable_drive_t msg;
        msg.enabled = false;
        _protocol.send(&msg);
        _sharedData->robot.cmd_disable_drive = false;
    } else if(_sharedData->robot.cmd_enable_drive) {
        msg_data_enable_drive_t msg;
        msg.enabled = true;
        _protocol.send(&msg);
        _sharedData->robot.cmd_enable_drive = false;
    }
    if(_sharedData->robot.home_flags != nullptr) {
        msg_data_confirm_home_t msg;
        memcpy(msg.home_flags, _sharedData->robot.home_flags, 5);
        _protocol.send(&msg);
        _sharedData->robot.home_flags = nullptr;
    }
    if(_sharedData->robot.cmd_reset_error_arm) {
        msg_data_reset_error_t msg;
        msg.component = RobotComponent::RobotArm;
        _protocol.send(&msg);
        _sharedData->robot.cmd_reset_error_arm = false;
    }
    if(_sharedData->robot.cmd_reset_error_drive) {
        msg_data_reset_error_t msg;
        msg.component = RobotComponent::Drive;
        _protocol.send(&msg);
        _sharedData->robot.cmd_reset_error_drive = false;
    }
    if(_sharedData->robot.cmd_transport_mode != TransportMode::None) {
        msg_data_transport_mode_t msg;
        msg.mode = _sharedData->robot.cmd_transport_mode;
        _protocol.send(&msg);
        _sharedData->robot.cmd_transport_mode = TransportMode::None;
    }

    if(_sharedData->operation_mode != _lastOpMode) {
        _lastOpMode = _sharedData->operation_mode;
        requireAlignment();
    }

    bool moveMode = _sharedData->operation_mode == OperationMode::move &&
                    _sharedData->finger_data.calibration_state == FingerCalibrationState::CalibrationDone;
    bool driveMode = _sharedData->operation_mode == OperationMode::drive;

    if((moveMode || driveMode)) {
        //Alignment of control move data with current robot position
        if(_sharedData->robot.align_positions) {
            if(driveMode) {
                _sharedData->robot.alignment_x = 0;
                _sharedData->robot.alignment_y = 0;
            } else if(moveMode) {
                //Request position data
                _moveRequestCounter++;
                if(_moveRequestCounter >= MOVE_REQUEST_INTERVAL) {
                    _moveRequestCounter = 0;
                    msg_data_request_position_t msg;
                    _protocol.send(&msg);
                }
            }

            int16_t dx = _sharedData->robot.alignment_x - _sharedData->imu.move.x;
            int16_t dy = _sharedData->robot.alignment_y - _sharedData->imu.move.y;
            //Serial.printf("RX: %d, RY: %d\n", _sharedData->robot.pos_x, _sharedData->robot.pos_y);
            int16_t dst_squared = dx*dx + dy*dy;
            if(dst_squared < ALIGN_THRESHOLD*ALIGN_THRESHOLD) {
                _alignCountDown--;
                if(_alignCountDown <= 0) {
                    _sharedData->robot.align_positions = false;
                }
            } else {
                _alignCountDown = ALIGN_TIME;
            }
        }
    } else {
        //Need to align control and robot positions when switching back to "move" later
        requireAlignment();
    }
    
    _sharedData->robot.align_countdown = _alignCountDown / UPDATE_HZ;

    //Handle up/down button commands
    if(_sharedData->robot.cmd_arm_down) {
        _sharedData->robot.cmd_arm_down = false;
        if(!_sharedData->robot.align_positions) {
            _z_pos -= Z_DELTA;
            _z_pos = constrain(_z_pos, -100, 100);
        }
    }
    if(_sharedData->robot.cmd_arm_up) {
        _sharedData->robot.cmd_arm_up = false;
        if(!_sharedData->robot.align_positions) {
            _z_pos += Z_DELTA;
            _z_pos = constrain(_z_pos, -100, 100);
        }
    }

    //Send movement data
    _moveSendCounter++;
    if(_moveSendCounter >= MOVE_SEND_INTERVAL) {
        _moveSendCounter = 0;        

        if(_sharedData->operation_mode == OperationMode::drive) {   
            if(!_sharedData->robot.align_positions) {
                //Send drive command
                msg_data_drive_velocity_t drive_msg;
                if(_sharedData->finger_data.hand_closed) {
                    //Turn mode
                    drive_msg.velocity_x = _sharedData->imu.move.x;
                    drive_msg.velocity_y = 0;
                    drive_msg.angle = _sharedData->imu.move.y;
                } else {
                    //Sideways motion mode
                    drive_msg.velocity_x = _sharedData->imu.move.x;
                    drive_msg.velocity_y = _sharedData->imu.move.y;
                    drive_msg.angle = 0;
                }
                _protocol.send(&drive_msg);
            }
        } else if(_sharedData->operation_mode == OperationMode::move) {
            if(!_sharedData->robot.align_positions) {
                //Send robot arm move command
                msg_data_robot_move_t move_msg;
                move_msg.position_x = _sharedData->imu.move.x;
                move_msg.position_y = _sharedData->imu.move.y;
                move_msg.position_z = _z_pos;
                _protocol.send(&move_msg);
            }
            //Send gripper move command
            msg_data_finger_update_t finger_msg;
            for (uint8_t i = 0; i < 5; i++)
            {
                finger_msg.finger_levels[i] = _sharedData->finger_data.fingers[i].bendLevel;
            }
            _protocol.send(&finger_msg);
        }
    }    

    //Send full imu data
    if(_sendImuData) {
        _imuSendCounter++;
        if(_imuSendCounter >= IMU_SEND_INTERVAL) {
            _imuSendCounter = 0;

            msg_data_imu_update_t imu_msg;
            imu_msg.orientation_x = (int16_t) _sharedData->imu.orientation.yaw;
            imu_msg.orientation_y = (int16_t) _sharedData->imu.orientation.pitch;
            imu_msg.orientation_z = (int16_t) _sharedData->imu.orientation.roll;
            imu_msg.cal_sys = _sharedData->imu.calibration.sys;
            imu_msg.cal_gyro = _sharedData->imu.calibration.gyro;
            imu_msg.cal_accel = _sharedData->imu.calibration.accel;
            imu_msg.cal_mag = _sharedData->imu.calibration.mag;
            imu_msg.accel_x = (int16_t) _sharedData->imu.acceleration.x;
            imu_msg.accel_y = (int16_t) _sharedData->imu.acceleration.y;
            imu_msg.accel_z = (int16_t) _sharedData->imu.acceleration.z;
            _protocol.send(&imu_msg);
        }
    }
}

void CommunicationSystem::requireAlignment() {
    _sharedData->robot.align_positions = true;
    _sharedData->robot.alignment_x = 255;
    _sharedData->robot.alignment_y = 255;
    _alignCountDown = ALIGN_TIME;
}