#include "display.h"

#define VBATPIN A7

// This is calibration data for the raw touch data to the screen coordinates
#define TS_MINX 3800
#define TS_MAXX 100
#define TS_MINY 100
#define TS_MAXY 3750

#define IMG_SPLASH "/splash.bmp"
#define IMG_BACKGROUND "/display.bmp"
#define IMG_CONN_OFF "/conn_off.bmp"
#define IMG_CONN_ON "/conn_on.bmp"
#define IMG_CONN_RECV "/conn_recv.bmp"

#define BTN_MODE_X 243
#define BTN_MODE_Y 49
#define BTN_MODE_DRIVE 0
#define BTN_MODE_MOVE 1
#define BTN_MODE_INFO 2

static const Rect rect_mode_touch = {
    .x = 243, .y = 49, .w = 78, .h = 191
};

static const Rect rect_bat = {.x = 255, .y = 11, .w = 54, .h = 26};
static const Rect rect_mode_buttons[] = {
    {.x = 308, .y = 56, .w = 12, .h = 49},
    {.x = 308, .y = 120, .w = 12, .h = 49},
    {.x = 308, .y = 184, .w = 12, .h = 49}
};
static const int8_t operation_mode_button[] = {-1, 1, 2, 0};
static const OperationMode button_operation_mode[] = {
    OperationMode::info,
    OperationMode::drive,
    OperationMode::move
};

static const Rect rect_info_page = {
    .x = 0, .y = 49, .w = 242, .h = 191
};

static const Rect rect_info_line = {
    .x = 4, .y = 53, .w = 234, .h = 24
};

static const Rect rect_info_button_big = {
    .x = 24, .y = 185, .w = 194, .h = 47
};

static const Rect rect_info_button_1 = {
    .x = 24, .y = 185, .w = 85, .h = 47
};

static const Rect rect_info_button_2 = {
    .x = 133, .y = 185, .w = 85, .h = 47
};

static const Rect rect_info_button_3 = {
    .x = 24, .y = 120, .w = 85, .h = 47
};

static const Rect rect_info_button_4 = {
    .x = 133, .y = 120, .w = 85, .h = 47
};

//Bounds of the alignment drawing area
//x, y -> center coordinates w, h -> half width/height
static const Rect rect_align {
    .x = 121, .y = 120, .w = 50, .h = 50
};

#define BAT_INDICATOR_BG ILI9341_WHITE
#define BAT_INDICATOR_FG ILI9341_GREEN

DisplaySystem::DisplaySystem(SharedData* sharedData) :
System(DISPLAY_UPDATE_RATE_US), _sharedData(sharedData),
_tft(TFT_CS, TFT_DC), _ts(STMPE_CS), _sd(), _reader(_sd)
{

}

//Display the boot splash screen
void DisplaySystem::preBoot() {
    _tft.begin(24000000);
    _tft.setRotation(1);
    if(!_sd.begin(SD_CS, SD_SCK_MHZ(10))) { // ESP32 requires 25 MHz limit
        Serial.println("SD begin() failed");
        for(;;); // Fatal error, do not continue
    }
    _reader.drawBMP(IMG_SPLASH, _tft, 0, 0);
}

/* Initialization of the system */
void DisplaySystem::init(SystemManager* systemManager) {
    //Load the connection symbols from the SD card
    _reader.loadBMP(IMG_CONN_OFF, _image_conn_off);
    _reader.loadBMP(IMG_CONN_ON, _image_conn_on);
    _reader.loadBMP(IMG_CONN_RECV, _image_conn_recv);

    //Draw the static background image
    _reader.drawBMP(IMG_BACKGROUND, _tft, 0, 0);
    _ts.begin();
    _update_all_values = true;

    pinMode(VBATPIN, INPUT);
}

void DisplaySystem::handleTouch(TS_Point p, bool justPressed) {
    //Handle mode switch buttons
    if(justPressed) {
        if(p.x > rect_mode_touch.x) {
            //Operating mode switch button pressed
            uint8_t button = (p.y - rect_mode_touch.y) / (rect_mode_touch.h / 3);
            _sharedData->operation_mode = button_operation_mode[button];
        } else {
            if(_current_values.operation_mode == OperationMode::info) {
                uint8_t index = _current_values.robot_home_index;
                if(_current_values.calibration_state != FingerCalibrationState::CalibrationDone) {
                    if(rect_info_button_big.inside(p.x, p.y)) {
                        _sharedData->finger_data.cmd_advance_calibration = true;
                    }
                } else if(_current_values.arm_state == RobotArmState::HomeDirectionConfirmation) {
                    if(rect_info_button_1.inside(p.x, p.y)) {
                        //Switch homing flag
                        if(index < 5) {
                            _robot_home_flags[index] = !(_robot_home_flags[index]);
                        }
                    } else if(rect_info_button_2.inside(p.x, p.y)) {
                        //Switch to next axis index
                        _current_values.robot_home_index++;
                        if(_current_values.robot_home_index == 5) {
                            //Flags checked, resume homing
                            _sharedData->robot.home_flags = _robot_home_flags;
                        }
                    }
                } else if(_current_values.arm_state == RobotArmState::Homing) {
                    if(rect_info_button_big.inside(p.x, p.y)) {
                        //Abort homing
                        _sharedData->robot.cmd_disable_arm = true;
                    }
                } else {
                    if(rect_info_button_1.inside(p.x, p.y)) {
                        switch (_current_values.arm_state)
                        {
                        case RobotArmState::Off:
                            //Power on arm
                            _sharedData->robot.cmd_enable_arm = true;
                            break;
                        case RobotArmState::On:
                            //Power off arm
                            _sharedData->robot.cmd_disable_arm = true;
                            break;
                        case RobotArmState::Error:
                            //Reset arm error
                            _sharedData->robot.cmd_reset_error_arm = true;
                            break;
                        default:
                            break;
                        }
                    }
                    if(rect_info_button_2.inside(p.x, p.y)) {
                        switch (_current_values.drive_state)
                        {
                        case DriveState::Off:
                            //Power on drive
                            _sharedData->robot.cmd_enable_drive = true;
                            break;
                        case DriveState::On:
                            //Power off drive
                            _sharedData->robot.cmd_disable_drive = true;
                            break;
                        case DriveState::Error:
                            //Reset drive error
                            _sharedData->robot.cmd_reset_error_drive = true;
                            break;
                        }
                    }
                    if(rect_info_button_3.inside(p.x, p.y)) {
                        if(_current_values.arm_state == RobotArmState::On) {
                            _sharedData->robot.cmd_transport_mode = _transport_mode;
                            if(_transport_mode == TransportMode::Mode1) {
                                _transport_mode = TransportMode::Mode2;
                            } else {
                                _transport_mode = TransportMode::Mode1;
                            }
                        }
                    }
                    if(rect_info_button_4.inside(p.x, p.y)) {
                        _sharedData->finger_data.cmd_advance_calibration = true;
                    }
                }
            } else if(_current_values.operation_mode == OperationMode::move) {
                if(rect_info_button_1.inside(p.x, p.y)) {
                    _sharedData->robot.cmd_arm_down = true;
                }
                if(rect_info_button_2.inside(p.x, p.y)) {
                    _sharedData->robot.cmd_arm_up = true;
                }
            }
        }
    }
}

void DisplaySystem::update() {
    bool pressed = false;

    //Get new touchscreen data
    while(!_ts.bufferEmpty()) {
        pressed = true;
        TS_Point raw_point = _ts.getPoint();
        // Scale from ~0->4000 to tft.width using the calibration values
        // Remap coordinates to account for display rotation
        _point.x = map(raw_point.y, TS_MINY, TS_MAXY, 0, _tft.width());
        _point.y = map(raw_point.x, TS_MAXX, TS_MINX, 0, _tft.height());
        _point.z = raw_point.z;
    }

    bool justPressed = pressed && !_last_pressed;
    _last_pressed = pressed;

    //Handle touch screen presses
    if(pressed) {
        if(_point.x < _tft.width() && _point.y < _tft.height()) {
            handleTouch(_point, justPressed);
        }
    }

    // Battery voltage calculation
    // we divided by 2, so multiply back
    // Multiply by 3.3V, our reference voltage
    // convert to voltage, convert from V to mV
    static const float factor = 2.0 * 3.3 / 1024.0 * 1000.0;
    static const float filter_alpha = 0.2;
    static const uint32_t battery_min = 3000; //mV
    static const uint32_t battery_max = 4200; //mV
    uint32_t measuredvbat = (uint32_t) ((float)analogRead(VBATPIN) * factor); //Voltage in mV
    measured_vbat_average = measured_vbat_average*(1-filter_alpha) + measuredvbat*filter_alpha;
    int32_t battery_percent = map(measured_vbat_average, battery_min, battery_max, 0, 100);
    battery_percent = constrain(battery_percent, 0, 100);
    
    //Get new values
    int32_t diff = (int32_t)last_vbat - (int32_t)measured_vbat_average;
    if(abs(diff) > 5) { //5mV hysteresis
        last_vbat = measured_vbat_average;
        _current_values.battery_level = battery_percent;
    }
    if(_sharedData->connection_status == ConnectionStatus::connected) {
        if(_sharedData->robot_link_up) {
            _current_values.connection = DisplayValues::Connection::connected;
        } else {
            _current_values.connection = DisplayValues::Connection::receiver_connected;
        }
    } else {
        _current_values.connection = DisplayValues::Connection::disconnected;
    }
    _current_values.operation_mode = _sharedData->operation_mode;
    _current_values.arm_state = _sharedData->robot.arm_state;
    _current_values.drive_state = _sharedData->robot.drive_state;
    _current_values.calibration_state = _sharedData->finger_data.calibration_state;
    _current_values.hand_closed = _sharedData->finger_data.hand_closed;
    _current_values.align_countdown = _sharedData->robot.align_countdown;
    _current_values.align_positions = _sharedData->robot.align_positions;
    _current_values.imu_calibrated = _sharedData->imu.available;

    /* Draw new values only if they have changed */
    //Update connection symbol
    if(_update_all_values || _last_values.connection != _current_values.connection) {
        switch (_current_values.connection)
        {
        case DisplayValues::Connection::connected: _image_conn_on.draw(_tft, 0, 0); break;
        case DisplayValues::Connection::disconnected: _image_conn_off.draw(_tft, 0, 0); break;
        case DisplayValues::Connection::receiver_connected: _image_conn_recv.draw(_tft, 0, 0); break;
        }
    }
    //Update battery level indicator
    if(_update_all_values || _last_values.battery_level != _current_values.battery_level) {
        uint16_t width = map(_current_values.battery_level, 0, 100, 0, rect_bat.w);
        _tft.fillRect(rect_bat.x, rect_bat.y, rect_bat.w-width, rect_bat.h, BAT_INDICATOR_BG);
        _tft.fillRect(rect_bat.x+rect_bat.w-width, rect_bat.y, width, rect_bat.h, BAT_INDICATOR_FG);
        _tft.setCursor(rect_bat.x + 5, rect_bat.y + 5);
        _tft.setTextSize(2);
        _tft.setTextColor(ILI9341_BLACK);
        if(_current_values.battery_level < 100) {
            _tft.print(' ');
        }
        if(_current_values.battery_level < 10) {
            _tft.print(' ');
        }
        _tft.print(_current_values.battery_level);
        _tft.print('%');
    }
    //Update the operation mode indicator
    if(_update_all_values || _last_values.operation_mode != _current_values.operation_mode) {
        int8_t prev = operation_mode_button[static_cast<size_t>(_last_values.operation_mode)];
        int8_t curr = operation_mode_button[static_cast<size_t>(_current_values.operation_mode)];
        if(prev != -1) {
            rect_mode_buttons[prev].fill(_tft, ILI9341_WHITE);
        }
        if(curr != -1) {
            rect_mode_buttons[curr].fill(_tft, ILI9341_CYAN);
        }
        rect_info_page.fill(_tft, ILI9341_WHITE);
        _update_page = true;
    }

    //OperationMode info page
    if(_current_values.operation_mode == OperationMode::info) {
        if(_update_page || _last_values.arm_state != _current_values.arm_state || _last_values.drive_state != _current_values.drive_state || _last_values.calibration_state != _current_values.calibration_state) {
            rect_info_page.fill(_tft, ILI9341_WHITE);
            _tft.setTextSize(2);
            _tft.setTextColor(ILI9341_BLACK, ILI9341_WHITE);

            setInfoCursor(0);
            _tft.print("Robot Information");

            if(_current_values.calibration_state != FingerCalibrationState::CalibrationDone) {
                setInfoCursor(1);
                _tft.print("Calibrating sensors");
                rect_info_button_big.draw(_tft, ILI9341_BLACK);

                switch (_current_values.calibration_state)
                {
                case FingerCalibrationState::CalibrationWorking:
                    _tft.setTextColor(ILI9341_LIGHTGREY, ILI9341_WHITE);
                    rect_info_button_big.drawCenteredText(_tft, "Wait...");
                    break;

                case FingerCalibrationState::CalibrationOpenHand:
                    _tft.setTextColor(ILI9341_BLACK, ILI9341_WHITE);
                    setInfoCursor(2);
                    _tft.print("Open your hand");
                    rect_info_button_big.drawCenteredText(_tft, "OK");
                    break;

                case FingerCalibrationState::CalibrationCloseHand:
                    _tft.setTextColor(ILI9341_BLACK, ILI9341_WHITE);
                    setInfoCursor(2);
                    _tft.print("Close your hand");
                    rect_info_button_big.drawCenteredText(_tft, "OK");
                    break;
                
                default:
                    break;
                }
            } else {
                _tft.setTextSize(2);
                if(_current_values.arm_state == RobotArmState::Homing) {
                    setInfoCursor(4);
                    _tft.print("Homing...");
                    rect_info_button_big.draw(_tft, ILI9341_BLACK);
                    rect_info_button_big.drawCenteredText(_tft, "Abort Homing");
                } else if(_current_values.arm_state == RobotArmState::HomeDirectionConfirmation) {
                    setInfoCursor(4);
                    _tft.print("Home Direction");
                    rect_info_button_1.draw(_tft, ILI9341_BLACK);
                    rect_info_button_2.draw(_tft, ILI9341_BLACK);
                    rect_info_button_2.drawCenteredText(_tft, "Confirm");
                    _current_values.robot_home_index = 0;
                    _update_flag_value = true;
                } else {
                    rect_info_button_1.draw(_tft, ILI9341_BLACK);
                    rect_info_button_2.draw(_tft, ILI9341_BLACK);
                    rect_info_button_3.draw(_tft, ILI9341_BLACK);
                    rect_info_button_4.draw(_tft, ILI9341_BLACK);
                    _tft.setTextSize(1);
                    _tft.setTextColor(ILI9341_BLACK, ILI9341_WHITE);

                    switch(_current_values.arm_state) {
                    case RobotArmState::Off:
                        //Power on arm
                        rect_info_button_1.drawCenteredText(_tft, "Enable Arm");
                        break;
                    case RobotArmState::On:
                        //Power off arm
                        rect_info_button_1.drawCenteredText(_tft, "Disable Arm");
                        break;
                    case RobotArmState::Error:
                        //Reset arm error
                        rect_info_button_1.drawCenteredText(_tft, "Reset Arm Error");
                        break;
                    default: break; //Other cases handled in if condition above
                    }
                    switch (_current_values.drive_state)
                    {
                    case DriveState::Off:
                        //Power on drive
                        rect_info_button_2.drawCenteredText(_tft, "Enable Drive");
                        break;
                    case DriveState::On:
                        //Power off drive
                        rect_info_button_2.drawCenteredText(_tft, "Disable Drive");
                        break;
                    case DriveState::Error:
                        //Reset drive error
                        rect_info_button_2.drawCenteredText(_tft, "Reset Drive Error");
                        break;
                    }

                    rect_info_button_4.drawCenteredText(_tft, "Calibrate Sensors");

                    //Transport mode
                    if(_current_values.arm_state != RobotArmState::On) {
                        _tft.setTextColor(ILI9341_LIGHTGREY, ILI9341_WHITE);
                    }
                    rect_info_button_3.drawCenteredText(_tft, "Transport Mode");
                }
            }
        }
        //Display the manual homing buttons
        uint8_t index = _current_values.robot_home_index;
        if(index < 5) {
            if(_update_flag_value || _last_values.robot_home_index != index) {
                setInfoCursor(4);
                advanceCursor("Home Direction ");
                _tft.setTextColor(ILI9341_BLACK, ILI9341_WHITE);
                _tft.print(index);
            }
            _current_values.flag = _robot_home_flags[_current_values.robot_home_index];
            if(_update_flag_value || _last_values.flag != _current_values.flag) {
                _tft.setTextColor(ILI9341_BLACK, ILI9341_WHITE);
                rect_info_button_1.drawCenteredText(_tft, _current_values.flag ? "Pos" : "Neg");
            }
        }
    }

    //OperationMode move page
    if(_current_values.operation_mode == OperationMode::move) {
        if(_update_page) {
            //Draw content when page is opened or values change
            _tft.setTextSize(2);
            rect_info_page.fill(_tft, ILI9341_WHITE);
            setInfoCursor(0);
            _tft.print("Control Arm");
            rect_info_button_1.draw(_tft, ILI9341_BLACK);
            rect_info_button_2.draw(_tft, ILI9341_BLACK);
            rect_info_button_1.drawCenteredText(_tft, "Down");
            rect_info_button_2.drawCenteredText(_tft, "Up");
            _update_alignment_data = true;
        }
    }

    //OperationMode drive page
    if(_current_values.operation_mode == OperationMode::drive) {
        _tft.setTextSize(2);
        _tft.setTextColor(ILI9341_BLACK, ILI9341_WHITE);
        if(_update_page) {
            _update_alignment_data = true;
            setInfoCursor(0);
            _tft.print("Control Drive");
        }
        if(_update_page || _last_values.hand_closed != _current_values.hand_closed) {
            clearInfoLine(3);
            setInfoCursor(3);
            if(_current_values.hand_closed) {
                _tft.print("Turn");
            } else {
                _tft.print("Side");
            }
        }
    }

    if(
        _current_values.operation_mode == OperationMode::drive ||
        (_current_values.operation_mode == OperationMode::move && _current_values.calibration_state == FingerCalibrationState::CalibrationDone)
    ) {
        //Draw alignment target
        if(_current_values.calibration_state == FingerCalibrationState::CalibrationDone) {
            //Clear background
            _tft.fillRect(
                rect_align.x-rect_align.w-1, rect_align.y-rect_align.h-1,
                2*rect_align.w+3, 2*rect_align.h+3, ILI9341_WHITE
            );
            //Draw position and target
            if(_current_values.align_positions) {
                drawAlignmentMarker(_sharedData->robot.alignment_x, _sharedData->robot.alignment_y, ILI9341_GREEN);
            }
            drawAlignmentMarker(_sharedData->imu.move.x, _sharedData->imu.move.y, ILI9341_RED);

            if(_update_alignment_data || _last_values.imu_calibrated != _current_values.imu_calibrated) {
                clearInfoLine(1);
                setInfoCursor(1);
                if(_current_values.imu_calibrated) {
                    _tft.print("C: OK");
                } else {
                    _tft.print("C: X");
                }
            }

            if(_update_alignment_data || _last_values.align_countdown != _current_values.align_countdown ||
                _last_values.align_positions != _current_values.align_positions) {
                clearInfoLine(2);
                setInfoCursor(2);
                if(_current_values.align_positions) {
                    _tft.print("A: ");
                    _tft.print(_current_values.align_countdown);
                } else {
                    _tft.print("A: OK");
                }
            }
        }
    }

    _update_all_values = false;
    _update_flag_value = false;
    _update_page = false;
    _update_alignment_data = false;

    _last_values = _current_values;

    
    //Serial.printf("Drawing time: %dms\n", millis()-time);
}

void DisplaySystem::setInfoCursor(uint8_t line) {
    _tft.setCursor(rect_info_line.x, rect_info_line.y + line*rect_info_line.h);
}

void DisplaySystem::clearInfoLine(uint8_t line) {
    _tft.fillRect(
        rect_info_line.x, rect_info_line.y + line*rect_info_line.h,
        rect_info_line.w, rect_info_line.h,
        ILI9341_WHITE
    );
}

void DisplaySystem::advanceCursor(const char* text) {
    int16_t x, y;
    uint16_t w, h;
    int16_t cx = _tft.getCursorX();
    int16_t cy = _tft.getCursorY();
    _tft.getTextBounds(text, cx, cy, &x, &y, &w, &h);
    _tft.setCursor(x+w, y);
}

void DisplaySystem::drawAlignmentMarker(int16_t ix, int16_t iy, uint16_t color) {
    if(ix < -100 || ix > 100) return;
    if(iy < -100 || iy > 100) return;
    int16_t x = -iy;
    int16_t y = -ix;
    x = rect_align.x + x * rect_align.w / 100;
    y = rect_align.y + y * rect_align.h / 100;
    _tft.fillRect(x-1, y-1, 3, 3, color);
}