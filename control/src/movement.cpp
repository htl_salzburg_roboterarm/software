#include "movement.h"
#include "error.h"
#include "imu_calibration_data.h"

#include "ArduinoLog.h"

const uint32_t CALIBRATION_UPDATE_INTERVAL = 50; //Every 50 imu readings

/**************************************************************************/
/*
    Displays some basic information on this sensor from the unified
    sensor API sensor_t type (see Adafruit_Sensor for more information)
*/
/**************************************************************************/
void displaySensorDetails(Adafruit_BNO055* bno)
{
  sensor_t sensor;
  bno->getSensor(&sensor);
  Log.trace("------------------------------------");
  Log.trace("IMU Sensor Information");
  Log.trace("------------------------------------");
  Log.trace("Sensor:       %s", sensor.name);
  Log.trace("Driver Ver:   %d", sensor.version);
  Log.trace("Unique ID:    %d", sensor.sensor_id);
  Log.trace("Max Value:    %F", sensor.max_value);
  Log.trace("Min Value:    %F", sensor.min_value);
  Log.trace("Resolution:   %F", sensor.resolution);
  Log.trace("------------------------------------");

  /* Get the system status values (mostly for debugging purposes) */
  uint8_t system_status, self_test_results, system_error;
  system_status = self_test_results = system_error = 0;
  bno->getSystemStatus(&system_status, &self_test_results, &system_error);

  /* Display the results in the Serial Monitor */
  Log.trace("System Status: %X", system_status);
  Log.trace("Self Test:     %X", self_test_results);
  Log.trace("System Error:  %X", system_error);
  Log.trace("------------------------------------");
}

MovementSystem::MovementSystem(SharedData* sharedData): System(IMU_UPDATE_RATE_US), _sharedData(sharedData), _bno(IMU_SENSOR_ID, IMU_ADDRESS) {
}

MovementSystem::~MovementSystem() {
}

/* Initialization of the system */
void MovementSystem::init(SystemManager* manager) {
  /* Initialise the sensor */
  if(!_bno.begin())
  {
    /* There was a problem detecting the BNO055 ... check your connections */
    manager->notify_error(EC_IMU_NOT_DETECTED);
  }
   
  delay(200);

  /* Use external crystal for better accuracy */
  _bno.setExtCrystalUse(true);

  /* Load calibration data */
  _bno.setSensorOffsets(IMU_CALIBRATION_DATA);
   
  /* Display some basic information on this sensor */
  displaySensorDetails(&_bno);
}


static const double GRAVITY_MAGNITUDE = 9.8;
static const int16_t PITCH_FULL_ANGLE = 30;
static const int16_t ROLL_FULL_ANGLE = 30;

/* Update the system */
void MovementSystem::update() {
  ImuData* imu = &(_sharedData->imu);

  //Read sensor calibration status
  _bno.getCalibration(
    &(imu->calibration.sys),
    &(imu->calibration.gyro),
    &(imu->calibration.accel),
    &(imu->calibration.mag)
  );

  //Check if the imu has a calibration level of at least 2
  imu->available = imu->calibration.sys > 0;

  if(imu->available) {
    //Get a new sensor event
    sensors_event_t event;
    _bno.getEvent(&event);

    imu::Vector<3> gravity = _bno.getVector(Adafruit_BNO055::VECTOR_GRAVITY) / GRAVITY_MAGNITUDE;
    const float sensitivity = 100.0 / 0.5;

    int32_t mapped_x = gravity.y() * sensitivity;
    int32_t mapped_y = -gravity.x() * sensitivity;

    if(mapped_x == 0 && mapped_y == 0) {
      Log.warning("IMU ZERO");
    }
    
    if(gravity.z() > 0.3) {
      //Gravity points down -> imu in correct orientation


      /*
      //Map BNO axes to yaw, pitch and roll
      imu->orientation.yaw = event.orientation.x;
      imu->orientation.pitch = -event.orientation.z;
      imu->orientation.roll = -event.orientation.y;

      if(imu->orientation.pitch > 180) imu->orientation.pitch -= 360; //Keep angles in range -180° to +180°
      if(imu->orientation.roll > 180) imu->orientation.roll -= 360; //Keep angles in range -180° to +180°

      //Create scaled move command values
      int32_t mapped_x = map(imu->orientation.pitch, -PITCH_FULL_ANGLE, PITCH_FULL_ANGLE, -100, 100);
      int32_t mapped_y = map(imu->orientation.roll, -ROLL_FULL_ANGLE, ROLL_FULL_ANGLE, -100, 100);*/
      imu->move.x = constrain(mapped_x, -100, 100);
      imu->move.y = constrain(mapped_y, -100, 100);
    } else {
      imu->available = false;
    }

    //Read calibration data
    /* This code is probably the cause for the short intervals of wrong imu readings
    _calib_count++;
    if(_calib_count > CALIBRATION_UPDATE_INTERVAL) {
      _calib_count = 0;
      _bno.getSensorOffsets(imu->calibration.data);
    }
    */
  }
}