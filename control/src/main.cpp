#include "Arduino.h"
#include "ArduinoLog.h"

#include "error.h"

#include "shared_data.h"
#include "system.h"
#include "movement.h"
#include "communication.h"
#include "sensors.h"
#include "feedback.h"
#include "display.h"

#define VBATPIN A7
#define MIN_UPDATE_RATE_US 20000

#define RED_LED 13

SharedData sharedData;

MovementSystem movement(&sharedData);
CommunicationSystem comm(&sharedData);
SensorSystem sensors(&sharedData);
FeedbackSystem feedback(&sharedData);
DisplaySystem display(&sharedData);

System* systems[] = {
  &movement, &comm, &sensors, &feedback, &display
};
SystemManager manager(systems, 5, MIN_UPDATE_RATE_US);

void notify_error(uint8_t error_level, uint8_t error_code) {
  switch(error_level) {
    case LEVEL_ERROR:
      Log.fatal(F("Critical Error: code %X"), error_code);
      while(1);

    case LEVEL_WARNING:
      Log.warning(F("Warning notification: code %X"), error_code);
  }
}
void printNewLine(Print* _logOutput) {
  _logOutput->print('\n');
}

void setup() {
  pinMode(RED_LED, OUTPUT);
  digitalWrite(RED_LED, HIGH);

  Serial.begin(115200);
  //while (!Serial) { yield(); }
  Log.begin(LOG_LEVEL_TRACE, &Serial);
  Log.setSuffix(printNewLine);

  Log.notice(F("Processor Starting..."));

  //Disable all SPI devices
  pinMode(BLUEFRUIT_SPI_CS, OUTPUT);
  digitalWrite(BLUEFRUIT_SPI_CS, HIGH);
  pinMode(MCP3208_SPI_CS, OUTPUT);
  digitalWrite(MCP3208_SPI_CS, HIGH);
  pinMode(TFT_CS, OUTPUT);
  digitalWrite(TFT_CS, HIGH);
  pinMode(STMPE_CS, OUTPUT);
  digitalWrite(STMPE_CS, HIGH);
  pinMode(SD_CS, OUTPUT);
  digitalWrite(SD_CS, HIGH);

  display.preBoot();

  manager.setErrorCallback(notify_error);

  manager.init(micros());

  digitalWrite(RED_LED, LOW);
}

void loop() {
  manager.update(micros());  
}