# Development of a mobile, gesture-controlled robotic gripping system with haptic feedback

This repository conatains the software for the final thesis project "Development of a mobile, gesture-controlled robot gripping system with haptic feedback",
developed in 2019/20 at HTBLuVA Salzburg, Department of Electrical Engineering by Jakob Buchsteiner, Thomas Eibl, Sebastian Neuhofer and Moritz Taferner.

This repository contains software for four different devices:

- microcontroller in the remote control glove (Adafruit Feather M0 Bluefruit LE)

- microcontroller on the robot (ESP32)

- two slave microcontrollers to the ESP32, responsible for regularly reading the ultrasonic sensors and sending the values over I2C (Atmega328p)

- PLC on the the robot (B&R Industrial Automation)

The software is structured in six subprojects:

## common

Common C++ libraries including common communication protocol definition and partial implementation (serialization and deserialization of messages)
The protocol is binary and serializes structs to bytes by pointer casting, this works because all used systems have the same endianess.

## control

PlatformIO project for the remote control glove.

Reads out motion and bend sensor values, converts the raw values to movement commands and sends them over Bluetooth LE.
Also controls the haptic feedback vibration motors in the glove as well as the display user interface.

## receiver

PlatformIO project for the receiver microcontroller.

Responsible for bridging Bluetooth LE and UART, controlling the mechanical hand and measuring the servo load, as well as monitoring the ultrasonic sensors.

## ultrasonic

PlatformIO project for the ultrasonic reading Atmega328p's.

Implements a simple I2C API to read out up to 8 connected ultrasonic sensors.

## roboterarm

B&R Automation Studio project for the PLC.

Responsible for executing the received movement commands by controlling the 5 stepper motors in the robotic arm, as well as the 4 synchronous motors driving the chassis.

## live-demo

p5.js project visualizing motion, bend and ultrasonic sensor data.