#ifndef STATUS_LEDS_H
#define STATUS_LEDS_H

#include "system.h"
#include "shared_data.h"
#include "pattern.h"

#define LED_SYSTEM_UPDATE_RATE 100000 //100ms or 10Hz

/*
This system is responsible for controlling the status leds
*/
class StatusLedSystem : public System {
    public:
        StatusLedSystem(SharedData* sharedData);

        //System implementation
        void init(SystemManager* manager) override;
        void update() override;

    private:
        SharedData* _sharedData;
        PatternPlayer _patternPlayers[2];

        void initLed(uint8_t index);
        void updateLed(uint8_t index, ConnectionStatus status, uint32_t time);
};

#endif