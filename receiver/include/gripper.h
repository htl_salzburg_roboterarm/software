#ifndef GRIPPER_H
#define GRIPPER_H

#include "system.h"
#include "shared_data.h"

#define GRIPPER_SYSTEM_UPDATE_RATE 100000 //every 100ms or 10Hz

/*
This system is responsible for controlling the gripper and checking the individual
motor currents to detect if the gripper is blocking
*/
class GripperSystem : public System {
    public:
        GripperSystem(SharedData* sharedData);

        //System implementation
        void init(SystemManager* manager) override;
        void update() override;

    private:
        SharedData* _sharedData;

        void disableFinger(uint8_t finger);
        void writeFingerPosition(uint8_t finger, float position);
};

#endif