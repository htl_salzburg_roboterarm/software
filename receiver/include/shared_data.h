#ifndef SHARED_DATA_H
#define SHARED_DATA_H

#include "inttypes.h"
#include "connection_lifecycle.h"

/*
Contains all variables which are associated with one finger
*/
struct Finger {
    float target_position;  //Current target position from control
    float current_position; //The current position of the servo in the gripper
    uint16_t current_load;  //The current load of the servo
    bool block;             //Is the finger / servo currently blocking
};

struct SharedData {
    ConnectionStatus bleStatus = ConnectionStatus::disconnected;    //Connection status of the BLE interface
    ConnectionStatus serialStatus = ConnectionStatus::disconnected; //Connection status of the RS232 interface
    bool gripperEnabled = false;        //Is the gripper enabled
    Finger fingers[5];                  //State of the fingers
    bool ultrasonic1_available = false; //Is the connection to the 1st ultrasonic reader (Atmega328p) available?
    bool ultrasonic2_available = false; //Is the connection to the 2nd ultrasonic reader (Atmega328p) available?
    uint8_t distances[16];              //Distance values of all ultrasonic sensors
};

#endif