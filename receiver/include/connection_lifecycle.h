#ifndef CONNECTION_LIFECYCLE_H
#define CONNECTION_LIFECYCLE_H

#include "inttypes.h"
#include "protocol.h"

/*
Describes the current status of the connection
*/
enum class ConnectionStatus {
    //General connection states
    disconnected = 0x01,
    connecting,
    connected,

    //Connetion error states
    error_version_mismatch = 0xE1,
    error_timeout,
    error_unknown
};

/*
This class manages the lifecycle of a connection:
1) sending the initialization messages (msg::init)
2) checking for the initialization acknowledgement (msg::init_ack)
3) sending heartbeats (msg::heartbeat) at the appropriate intervals
4) checking if heartbeats are still being received from the other end of the connection
*/
class ConnectionLifecycle {
    public:
        ConnectionStatus connection_status = ConnectionStatus::disconnected;

        ConnectionLifecycle(Protocol* protocol, uint16_t timeOutTicks, uint16_t heartBeatInterval, const char* interfaceName);
        
        inline ConnectionStatus getConnectionStatus() {
            return connection_status;
        }
        //Sets the lifecycle of another connection to monitor, the status of this
        //connection is send with the link_up flag of the heartbeat messages
        void setMonitorLifecycle(ConnectionLifecycle* lifecycle);
        //Updates the ConnectionLifecycle
        void update();
        //Start the connection initialization
        void connect();

    private:
        Protocol* _protocol;
        ConnectionLifecycle* _monitor_lifecycle = nullptr;
        uint16_t _timeOutTicks;         //How many update cycles between heartbeats is considered a timeout
        uint16_t _timeOutCounter = 0;   //Number of updates since the last received heartbeat
        uint16_t _heartBeatInterval;    //How many updates between sending heartbeats
        uint16_t _heartBeatCounter = 0; //Number of updates since the last sent heartbeat
        const char* _interfaceName;     //Name of the interface for debugging messages

        void setConnectionStatus(ConnectionStatus status);

        //Message handlers for msg::init_ack and msg::heartbeat
        static void handleInitAckMsg(uint8_t* data, void* user_data);
        static void handleHeartbeatMsg(uint8_t* data, void* user_data);

};

#endif