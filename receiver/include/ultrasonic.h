#ifndef ULTRASONIC_H
#define ULTRASONIC_H

#include "system.h"
#include "shared_data.h"
#include "ultrasonic_registers.h"
#include "Wire.h"

#define ULTRASONIC_SYSTEM_UPDATE_RATE 200000 //200ms or 5Hz

/*
This class is provides methods to interface with the ultrasonic reader microcontrollers (Atmega328p) over I2C
*/
class UltrasonicReader {
    public:
        //Construct the UltrasonicReader class which connects to the reader at the given I2C address
        UltrasonicReader(uint8_t address);

        //Start a connection to the ultrasonic reader
        //numTries: how many times a connection attempt is made, 0 means unlimited
        bool begin(uint8_t numTries = 10, uint32_t interval = 500);
        //Enable the reading of the ultrasonic sensors
        void setEnabled(bool enabled);
        //Set the interval at which the reader evaluates the ultrasonic sensor
        void setPingInterval(uint16_t millis);
        //Read the values of all ultrasonic sensors connected to the reader
        void readAllSensors(uint8_t* readings);

    private:
        uint8_t _address;

        //Read a registere with any datatype and cound
        template<class T>
        bool readRegister(UltrasonicRegister reg, T* data, uint8_t count = 1) {
            Wire.beginTransmission(_address);
            Wire.write(static_cast<uint8_t>(reg));
            Wire.endTransmission();
            //Serial.printf("Reading '%X' with size: %d and count: %d\n", (uint8_t)reg, sizeof(T), count);
            uint8_t size = (uint8_t) sizeof(T) * count;
            int numBytes = Wire.requestFrom(_address, size);
            if(numBytes < sizeof(T)) {
                //Serial.printf("Available: %d\n", numBytes);
                return false;
            } else {
                Wire.readBytes((uint8_t*)data, size);
                return true;
            }
        }

        //Write a register with any datatype
        template<class T>
        void writeRegister(UltrasonicRegister reg, T data) {
            //Serial.printf("Writing register %X with size: %d, data = %d\n", (uint8_t)reg, sizeof(T), data);
            Wire.beginTransmission(_address);
            Wire.write(static_cast<uint8_t>(reg));
            Wire.write((uint8_t*)&data, sizeof(T));
            Wire.endTransmission();
        }
};

/*
This system is responsible for reading the ultrasonic sensor values from the Atmega328p microcontrollers
*/
class UltrasonicSystem : public System {
    public:
        UltrasonicSystem(SharedData* sharedData);

        //System implementation
        void init(SystemManager* systemManager) override;
        void update() override;

    private:
        SharedData* _sharedData;

        UltrasonicReader _reader1;
        UltrasonicReader _reader2;
};

#endif