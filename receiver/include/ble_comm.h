#ifndef BLE_COMMUNICATION_H
#define BLE_COMMUNICATION_H

#include "inttypes.h"
#include "system.h"
#include "shared_data.h"
#include "protocol.h"
#include "connection_lifecycle.h"
#include "BLEDevice.h"
#include "map"
#include "freertos/queue.h"

#define BLE_COMM_UPDATE_RATE_US 10000 //100Hz

#define BLE_TIMEOUT_TICKS ( PROTOCOL_TIMEOUT_TIME / (BLE_COMM_UPDATE_RATE_US / 1000) )
#define BLE_HEARTBEAT_TICKS ( PROTOCOL_HEARTBEAT_TIME / (BLE_COMM_UPDATE_RATE_US / 1000) )

#define BLE_RECONNECT_INTERVAL 3000 //3s
#define BLE_RECONNECT_CYCLES (BLE_RECONNECT_INTERVAL*1000/BLE_COMM_UPDATE_RATE_US)

// The remote service we wish to connect to (Nordic UART Service)
static BLEUUID serviceUUID("6e400001-b5a3-f393-e0a9-e50e24dcca9e");
// The characteristic of the remote service we are interested in.
static BLEUUID  charTxUUID("6e400002-b5a3-f393-e0a9-e50e24dcca9e");
static BLEUUID  charRxUUID("6e400003-b5a3-f393-e0a9-e50e24dcca9e");

// Struct to send data from BLE task to main loop task
struct rx_data_t {
    uint8_t data[20];
    uint8_t data_length;
};

/*
This system is responsible for the communication over the Bluetooth Low Energy Link.

The implementation is running on its own FreeRTOS task as most BLE API functions are blocking
and would interfere with the other systems.
*/
class BLECommunication : public System, BLEClientCallbacks, BLEAdvertisedDeviceCallbacks {
    public:
        BLECommunication(SharedData* sharedData);
        ~BLECommunication();

        //System implementation
        void init(SystemManager* manager) override;
        void update() override;

        //Get a pointer the Protocol object
        Protocol* getProtocol();
        //Get a pointer to the ConnectionLifecycle
        ConnectionLifecycle* getLifecycle();
        //Set the link which is used to forward unknown messages, as well as its ConnectionLifecycle which
        //is monitored and whose status is sent with the heartbeat messages of this link
        void setForwardLink(Protocol* protocol, ConnectionLifecycle* lifecycle);

        //BLEAdvertisedDeviceCallbacks implementation
        void onResult(BLEAdvertisedDevice advertisedDevice) override;

        //BLEClientCallbacks implementation
        void onConnect(BLEClient *pClient) override;
        void onDisconnect(BLEClient *pClient) override;

        //Protocol implementation
        static void handleWrite(uint8_t* data, uint16_t length, void* user_data);
        static void notifyCallback(BLERemoteCharacteristic* pBLERemoteCharacteristic, uint8_t* pData, size_t length, bool isNotify);

        //Message callbacks
        static void defaultMsgCallback(bool handled, msg msg_code, uint16_t msg_size, uint8_t* data, void* user_data);
        static void handleFingerUpdateMsg(uint8_t* data, void* user_data);
        static void handleCalibMsg(uint8_t* data, void* user_data);

    private:
        SharedData* _sharedData;
        Protocol _protocol;
        Protocol* _forwardProtocol = nullptr;

        uint8_t _gripper_block_flags = 0;

        /* Protocol implementation */
        void bleTask(); //Main function of the BLE Task
        static void bleTaskWrapper(void* arg); //Helper function to start the BLE Task
        void interfaceUpdate(); //Handles all the interface implementation, called from the update() function

        QueueHandle_t _rx_data_queue; //Queue to send received messages from BLE Task to the main loop task

        ConnectionLifecycle _lifecycle; //Handles the connection initialization and heartbeat sequence
        uint32_t _connect_counter; //Counter for the reconnect delay

        static std::map<BLERemoteCharacteristic*, BLECommunication*> _notfiyCharacteristicMap;

        BLERemoteCharacteristic* _pTxCharacteristic = nullptr; //TX-Characterstic to send data
        BLERemoteCharacteristic* _pRxCharacteristic = nullptr; //RX-Characterstic to receive data

        bool _connected = false;
        bool _device_found = false;
        BLEAdvertisedDevice _device;
        FreeRTOS::Semaphore _connectionSemaphore = FreeRTOS::Semaphore("ConnectionStatus");
        /* End Protocol implementation */
};

#endif