#ifndef PC_INTERFACE_H
#define PC_INTERFACE_H

#include <deque>
#include <string>
#include "system.h"
#include "shared_data.h"
#include "protocol.h"

#define PC_INTERFACE_UPDATE_RATE_US 100000 //100ms = 10Hz

#define PC_INTERFACE_BUFFER_SIZE 256

//Which format to output imu data
enum class ImuOutputMode {
    none,
    demo,
    csv,
    binary
};

//Result of command parsing
enum class CmdError {
    none,
    cmd_unknown,
    not_enough_arguments,
    invalid_arguments
};

/*
This system is used for communication with the PC over the USB serial port and is mostly used for debugging
purposes.
*/
class PCInterface : public System {
    public:
        PCInterface(SharedData* sharedData);
        ~PCInterface();

        void init(SystemManager* manager) override;
        void update() override;

        //Message handlers
        static void handleImuMsg(uint8_t* data, void* user_data);

    private:
        SharedData* _sharedData;
        Protocol* _protocol;
        bool _sendFingerLevels;
        ImuOutputMode _imuOutputMode;
        bool _sendDistances;
        bool _sendFingerLoad;

        //Buffer to store from serial interface
        char _rxbuffer[PC_INTERFACE_BUFFER_SIZE];
        //Stores the split parts from PC commands
        std::deque<std::string> _cmdParts;
        bool _msg_start = false;
        uint8_t _buffer_index = 0;

        CmdError handleCmd(std::deque<std::string> &parts);
};

#endif