#ifndef SERIAL_COMMUNICATION_H
#define SERIAL_COMMUNICATION_H

#include "inttypes.h"
#include "system.h"
#include "shared_data.h"
#include "protocol.h"
#include "connection_lifecycle.h"

#define SERIAL_COMM_UPDATE_RATE_US 10000 //100Hz

#define SERIAL_TIMEOUT_TICKS ( PROTOCOL_TIMEOUT_TIME / (SERIAL_COMM_UPDATE_RATE_US / 1000) )
#define SERIAL_HEARTBEAT_TICKS ( PROTOCOL_HEARTBEAT_TIME / (SERIAL_COMM_UPDATE_RATE_US / 1000) )

#define ULTRASONIC_SEND_INTERVAL 20

#define SERIAL_RECONNECT_INTERVAL 3000 //3s
#define SERIAL_RECONNECT_CYCLES (SERIAL_RECONNECT_INTERVAL*1000/SERIAL_COMM_UPDATE_RATE_US)

/*
This System is responsible for communication over the Serial (UART/RS232) interface.
*/
class SerialCommunication : public System {
    public:
        SerialCommunication(SharedData* _sharedData);

        //System implementation
        void init(SystemManager* manager) override;
        void update() override;

        //Get the Protocol object
        Protocol* getProtocol();
        //Get a pointer to the ConnectionLifecycle 
        ConnectionLifecycle* getLifecycle();
        void setForwardLink(Protocol* protocol, ConnectionLifecycle* lifecycle);

        //Protocol implementation
        static void handleWrite(uint8_t* data, uint16_t length, void* user_data);
        //Default message handler
        static void defaultMsgCallback(bool handled, msg msg_code, uint16_t msg_size, uint8_t* data, void* user_data);

    private:
        SharedData* _sharedData;
        Protocol _protocol;
        ConnectionLifecycle _lifecycle;
        uint32_t _connect_counter;

        uint8_t rx_buffer[256];

        Protocol* _forwardProtocol = nullptr;

        uint8_t _ultrasonic_send_counter = 0;
};

#endif