#include "pc_interface.h"
#include "system_ids.h"
#include "ble_comm.h"
#include "Arduino.h"
#include "ArduinoLog.h"

#define STARTUP_MARKER F("[STARTUP]")
#define DATA_PREFIX F("[DATA]")
#define DATA_PREFIX_IMU_ORIENTATION F("[DATA]O;")
#define DATA_PREFIX_IMU_CALIBRATION F("[DATA]C;")
#define DATA_PREFIX_IMU_CSV F("[CSV]")
#define DATA_PREFIX_FINGER F("[DATA]F")
#define DATA_PREFIX_DISTANCE F("[DATA]D")
#define DATA_PREFIX_LOAD F("[DATA]L")
#define DATA_SEPARATOR F(";")

#define PC_PORT Serial

PCInterface::PCInterface(SharedData* sharedData) : System(PC_INTERFACE_UPDATE_RATE_US), _sharedData(sharedData) {
    _sendFingerLevels = false;
    _sendDistances = false;
    _sendFingerLoad = false;
    _imuOutputMode = ImuOutputMode::none;
}

PCInterface::~PCInterface() {

}

/* Initialization of the system*/
void PCInterface::init(SystemManager* manager) {
    _protocol = ((BLECommunication*) manager->getSystem(SYSTEM_BLE_COMM))->getProtocol();
    _protocol->registerMessageCallback(msg::imu_update, handleImuMsg, this);
    PC_PORT.println(STARTUP_MARKER); //Used by live-demo project to detect that the receiver has started
}

void PCInterface::handleImuMsg(uint8_t* data, void* user_data) {
    PCInterface* pc = (PCInterface*) user_data;
    msg_data_imu_update_t* msg = (msg_data_imu_update_t*) data;
    static bool header_printed = false;

    switch (pc->_imuOutputMode)
    {
    case ImuOutputMode::demo:
        //The processing sketch expects data as roll, pitch, heading
        PC_PORT.print(DATA_PREFIX_IMU_ORIENTATION);
        PC_PORT.print((float)msg->orientation_x);
        PC_PORT.print(DATA_SEPARATOR);
        PC_PORT.print((float)msg->orientation_y);
        PC_PORT.print(DATA_SEPARATOR);
        PC_PORT.print((float)msg->orientation_z);
        PC_PORT.println(DATA_SEPARATOR);

        //Also send calibration data for each sensor.
        PC_PORT.print(DATA_PREFIX_IMU_CALIBRATION);
        PC_PORT.print(msg->cal_sys, DEC);
        PC_PORT.print(DATA_SEPARATOR);
        PC_PORT.print(msg->cal_gyro, DEC);
        PC_PORT.print(DATA_SEPARATOR);
        PC_PORT.print(msg->cal_accel, DEC);
        PC_PORT.print(DATA_SEPARATOR);
        PC_PORT.println(msg->cal_mag, DEC);
        break;
    case ImuOutputMode::csv:
        if(!header_printed) {
            header_printed = true;
            PC_PORT.print(DATA_PREFIX_IMU_CSV);
            PC_PORT.print("Ox;Oy;Oz;Ow;Ax;Ay;Az;Cs;Cg;Ca;Cm\r\n");
        }

        /* Ouput raw imu data as csv */
        PC_PORT.print(DATA_PREFIX_IMU_CSV);
        PC_PORT.print(msg->orientation_x);
        PC_PORT.print(';');
        PC_PORT.print(msg->orientation_y);
        PC_PORT.print(';');
        PC_PORT.print(msg->orientation_z);
        PC_PORT.print(';');
        PC_PORT.print(msg->orientation_w);
        PC_PORT.print(';');
        PC_PORT.print(msg->accel_x);
        PC_PORT.print(';');
        PC_PORT.print(msg->accel_y);
        PC_PORT.print(';');
        PC_PORT.print(msg->accel_z);
        PC_PORT.print(';');
        PC_PORT.print(msg->cal_sys);
        PC_PORT.print(';');
        PC_PORT.print(msg->cal_gyro);
        PC_PORT.print(';');
        PC_PORT.print(msg->cal_accel);
        PC_PORT.print(';');
        PC_PORT.print(msg->cal_mag);
        PC_PORT.print("\r\n");
        break;
    case ImuOutputMode::binary:
        PC_PORT.write(data, sizeof(msg_data_imu_update_t));
        break;
    case ImuOutputMode::none:
        break;
    default:
        break;
    }
}

//Utility method to remove the next word from the command string and return it
static std::string popAndReturn(std::deque<std::string> &parts) {
    std::string result = parts.front();
    parts.pop_front();
    return result;
}

//Handle a command received from the PC
CmdError PCInterface::handleCmd(std::deque<std::string> &parts) {
    if(parts.empty()) return CmdError::none;

    //Get the command part
    std::string cmd = popAndReturn(parts);
    if(cmd == "calibrate") {
        //NOTE this command is no longer used, calibration is via the graphical interface of the control
        //calibrate requires two arguments
        if(parts.size() < 2) return CmdError::not_enough_arguments;
        std::string type = popAndReturn(parts);
        std::string highLow = popAndReturn(parts);
        
        msg_data_config_sensor_t m;
        m.config_type = 0;
        m.sensor_index = CONFIG_SENSOR_INDEX_ALL;

        if(type == "bend") {
            m.config_type |= CONFIG_SENSOR_BEND;
        } else if(type == "force") {
            m.config_type |= CONFIG_SENSOR_FORCE;
        } else {
            return CmdError::invalid_arguments;
        }

        if(highLow == "high") {
            m.config_type |= CONFIG_SENSOR_HIGH;
        } else if(highLow == "low") {
            m.config_type |= CONFIG_SENSOR_LOW;
        } else {
            return CmdError::invalid_arguments;
        }

        //Send the calibration message
        _protocol->send(&m);

    } else if(cmd == "vibro") {
        //Toggle the vibration motors inside the control, requires one or two arguments
        if(parts.empty()) return CmdError::not_enough_arguments;
        std::string type = popAndReturn(parts);

        //Parse the vibration type
        msg_data_vibro_trigger_t m;
        if (type == "cancel") {
            m.mode = VibrateMode::Cancel;
        } else if (type == "short") {
            m.mode = VibrateMode::Short;
        } else if(type == "long") {
            m.mode = VibrateMode::Long;
        } else if(type == "double") {
            m.mode = VibrateMode::Double;
        } else {
            return CmdError::invalid_arguments;
        }

        //Parse the affected figners
        if(parts.empty()) {
            m.mask = 0b00011111; //All five fingers
        } else {
            m.mask = 0;
            std::string fingers = popAndReturn(parts);
            uint8_t index = 0;
            for(const char &c : fingers) {
                if(c == '1') {
                    m.mask |= (1<<index); //Set vibrate bit
                }
                index++;
                if(index == 5) break;
            }
            Log.notice("Vibrate mask: %d", m.mask);
        }

        //Send the message
        Log.notice("Vibrate sent");
        _protocol->send(&m);
        
    } else if(cmd == "loglevel") {
        //Change the loglevel of the receiver
        if(parts.empty()) return CmdError::not_enough_arguments;

        std::string level = popAndReturn(parts);
        uint8_t log_level = 0;
        if(level == "silent") log_level = LOG_LEVEL_SILENT;
        else if(level == "fatal") log_level = LOG_LEVEL_FATAL;
        else if(level == "error") log_level = LOG_LEVEL_ERROR;
        else if(level == "warning") log_level = LOG_LEVEL_WARNING;
        else if(level == "notice") log_level = LOG_LEVEL_NOTICE;
        else if(level == "trace") log_level = LOG_LEVEL_TRACE;
        else if(level == "verbose") log_level = LOG_LEVEL_VERBOSE;
        else return CmdError::invalid_arguments;

        Log.begin(log_level, &PC_PORT);  //Change loglevel
    } else if(cmd == "output") {
        //Change the output modes of the various control sensors
        if(parts.size() < 2) return CmdError::not_enough_arguments;

        std::string sensor = popAndReturn(parts);       //Get the sensor
        std::string outputType = popAndReturn(parts);   //Get the requestet outptu type

        if(sensor == "finger") {
            //Turn finger data output on/off
            if(outputType == "on") {
                _sendFingerLevels = true;
            } else if(outputType == "off") {
                _sendFingerLevels = false;
            } else {
                return CmdError::invalid_arguments;
            }
        } else if(sensor == "motion") {
            //Switch imu output mode
            msg_data_config_data_mode_t msg;
            if(outputType == "off") {
                _imuOutputMode = ImuOutputMode::none;
                msg.send_imu_data = false;
            } else if(outputType == "demo") {
                _imuOutputMode = ImuOutputMode::demo;
                msg.send_imu_data = true;
            } else if(outputType == "csv") {
                _imuOutputMode = ImuOutputMode::csv;
                msg.send_imu_data = true;
            } else if(outputType == "binary") {
                _imuOutputMode = ImuOutputMode::binary;
                msg.send_imu_data = true;
            } else {
                return CmdError::invalid_arguments;
            }

            //if the imu output mode is not none, send a message which enables sending
            //the complete imu data
            _protocol->send(&msg);
        } else if(sensor == "distance") {
            //Switch distance sensor output on/off
            if(outputType == "on") {
                _sendDistances = true;
            } else if(outputType == "off") {
                _sendDistances = false;
            } else {
                return CmdError::invalid_arguments;
            }
        } else if(sensor == "load") {
            //Switch finger load sensing output on/off
            if(outputType == "on") {
                _sendFingerLoad = true;
            } else if(outputType == "off") {
                _sendFingerLoad = false;
            } else {
                return CmdError::invalid_arguments;
            }
        } else {
            return CmdError::invalid_arguments;
        }

    } else {
        return CmdError::cmd_unknown;
    }

    return CmdError::none;
}

/* Update the system */
void PCInterface::update() {
    //Check for messages
    while(PC_PORT.available()) {
        char c = PC_PORT.read();
        if(c == '\r') continue; //ignore carriage return

        if(c == '\n' || c == ' ') {
            //End of a command part
            if(_buffer_index != 0) { //Ignore double whitespace
                _rxbuffer[_buffer_index] = '\0';
                _buffer_index = 0;
                //Add the part to the parts list
                std::string part(_rxbuffer);
                _cmdParts.push_back(part);
            }

            if(c == '\n') {
                //End of message
                if(!_cmdParts.empty()) {
                    //Process the received command
                    CmdError result = handleCmd(_cmdParts);
                    //Clear the parts list
                    _cmdParts.clear();
                    //Print out errors
                    switch (result)
                    {
                    case CmdError::none:
                        break;
                    case CmdError::cmd_unknown:
                        PC_PORT.println("Error: unknown command");
                        break;
                    case CmdError::not_enough_arguments:
                        PC_PORT.println("Error: not enough arguments");
                        break;
                    case CmdError::invalid_arguments:
                        PC_PORT.println("Error: invalid arguments");
                        break;
                    }
                }
            }
        } else {
            //Add character to buffer
            _rxbuffer[_buffer_index] = c;
            _buffer_index++;
            if(_buffer_index >= PC_INTERFACE_BUFFER_SIZE) _buffer_index = 0; //Handle overflow
        }
    }

    //Send distance sensor distances
    if(_sendDistances) {
        PC_PORT.print(DATA_PREFIX_DISTANCE);
        for (uint8_t i = 0; i < 16; i++)
        {
            PC_PORT.print(";");
            PC_PORT.print(_sharedData->distances[i]);
        }
        PC_PORT.println();
    }

    //Send finger bend levels
    if(_sendFingerLevels) {
        PC_PORT.print(DATA_PREFIX_FINGER);
        for (uint8_t i = 0; i < 5; i++)
        {
            PC_PORT.print(";");
            PC_PORT.print(_sharedData->fingers[i].target_position);
        }
        PC_PORT.println();
    }

    //Send finger load levels
    if(_sendFingerLoad) {
        PC_PORT.print(DATA_PREFIX_LOAD);
        for (uint8_t i = 0; i < 5; i++)
        {
            PC_PORT.print(";");
            PC_PORT.print(_sharedData->fingers[i].current_load);
        }
        PC_PORT.println();
    }
}