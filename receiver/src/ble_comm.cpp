#include "ble_comm.h"
#include "ArduinoLog.h"

#include "FreeRTOS.h"

std::map<BLERemoteCharacteristic*, BLECommunication*> BLECommunication::_notfiyCharacteristicMap;

BLECommunication::BLECommunication(SharedData* sharedData) : System(BLE_COMM_UPDATE_RATE_US),
_sharedData(sharedData),
_lifecycle(&_protocol, BLE_TIMEOUT_TICKS, BLE_HEARTBEAT_TICKS, "BLE")
{
    //Register write-function and message callbacks
    _protocol.setWriteFunction(handleWrite, this);
    _protocol.registerDefaultCallback(defaultMsgCallback, this);
    _protocol.registerMessageCallback(msg::finger_update, handleFingerUpdateMsg, this);
    
    //Create the queue to transfer received messages from the BLE task to the main loop task
    _rx_data_queue = xQueueCreate(10, sizeof(rx_data_t));
}

BLECommunication::~BLECommunication() {
}

/* Initialization of the system */
void BLECommunication::init(SystemManager* manager) {
    //Start the task responsible for handling the communication via BLE
    FreeRTOS::startTask(bleTaskWrapper, "BLECommTask", this, 8192);
}

/* Update the system */
void BLECommunication::update() {
    //Updated the BLE interface
    interfaceUpdate();

    //Send gripper feedback:
    //Check the 'blocking' state of all fingers and convert it into a single byte
    uint8_t new_block_flags = 0;
    for (uint8_t i = 0; i < 5; i++)
    {
        if(_sharedData->fingers[i].block) {
            new_block_flags |= (1<<i);
        }
    }
    //Check if the 'blocking' state has changed since the last update
    if(new_block_flags != _gripper_block_flags) {
        //Calculate all fingers which where not blocked before but are now
        uint8_t added_flags = new_block_flags & (~_gripper_block_flags);
        //Set the last updated state to the current one
        _gripper_block_flags = new_block_flags;

        //Send a message with the levels at which the ES-Brakes should engage
        msg_data_brake_update_t brake_update;
        for (uint8_t i = 0; i < 5; i++)
        {
            brake_update.brake_levels[i] = (uint8_t) _sharedData->fingers[i].current_position;
        }

        //Trigger a short vibration pattern for newly blocked fingers
        msg_data_vibro_trigger_t vibro_trigger;
        vibro_trigger.mode = VibrateMode::Short;
        vibro_trigger.mask = added_flags;

        //Send both messages
        _protocol.send(&brake_update);
        _protocol.send(&vibro_trigger);
    }

    //TODO send feedback from distance sensors (proximity alert)
    
}

/* Message Handlers */
void BLECommunication::defaultMsgCallback(bool handled, msg msg_type, uint16_t msg_size, uint8_t* data, void* user_data) {
    //Forward unknown messages (= have not been handled by other message handlers)
    if(!handled) {
        BLECommunication* comm = (BLECommunication*) user_data;
        if(comm->_forwardProtocol != nullptr) {
            comm->_forwardProtocol->sendRaw(msg_type, msg_size, data);
        }
    }
}

void BLECommunication::handleFingerUpdateMsg(uint8_t* data, void* user_data) {
    BLECommunication* comm = (BLECommunication*) user_data;
    msg_data_finger_update_t* msg = (msg_data_finger_update_t*) data;
    //Update the target position of the gripper with the received finger positions
    for (uint8_t i = 0; i < 5; i++)
    {
        comm->_sharedData->fingers[i].target_position = msg->finger_levels[i];
    }
}

/* Protocol Implementation */

/* BLE communication task */
void BLECommunication::bleTask() {
    //Create a client which is responsible to connect to the server
    BLEClient* client = BLEDevice::createClient();
    //First empty the semaphore from initial give() call (Constructor of semaphore)
    _connectionSemaphore.take("BLETask");
    while (true)
    {
        //Sleep 1 second before attempting to connect
        FreeRTOS::sleep(1000);
        Log.trace("[BLETask] start");
        //Start a BLE scan
        BLEScan* pBLEScan = BLEDevice::getScan();
        pBLEScan->setAdvertisedDeviceCallbacks(this, true);
        pBLEScan->setInterval(1349);
        pBLEScan->setWindow(449);
        pBLEScan->setActiveScan(true);
        _device_found = false;
        //Scan for one second, results will be processed in the onResult method
        BLEScanResults results = pBLEScan->start(1);
        Log.trace("[BLETask] scan completed");

        //Check if the onResult method found the correct device
        if(!_device_found) {
            continue;
        }

        Log.trace("[BLETask] device found");
        //Connect to the device with the client
        client->connect(&_device);

        //Check if the connection was successful
        if(!client->isConnected()) {
            Log.error("[BLETask] connection failed");
            continue;
        }

        Log.trace("[BLETask] connected to server");

        // Obtain a reference to the service we are after in the remote BLE server.
        BLERemoteService* pRemoteService = client->getService(serviceUUID);
        if (pRemoteService == nullptr) {
            Log.error("[BLETask] Failed to find our service UUID: %s", serviceUUID.toString().c_str());
            client->disconnect();
            continue;
        }
        Log.trace("[BLETask] service found");


        // Obtain a reference to the characteristic in the service of the remote BLE server.
        _pTxCharacteristic = pRemoteService->getCharacteristic(charTxUUID);
        if (_pTxCharacteristic == nullptr) {
            Log.error("[BLETask] Failed to find our characteristic UUID: %s", charTxUUID.toString().c_str());
            client->disconnect();
            continue;
        }
        _pRxCharacteristic = pRemoteService->getCharacteristic(charRxUUID);
        if (_pRxCharacteristic == nullptr) {
            Log.error("[BLETask] Failed to find our characteristic UUID: %s", charRxUUID.toString().c_str());
            client->disconnect();
            continue;
        }
        Log.trace("[BLETask] characteristics found");

        // Try to register for notifcations on the RX characterstic
        if(!_pRxCharacteristic->canNotify()) {
            Log.error("[BLETask] RX-Characterstic doesn't support 'notify'");
            client->disconnect();
            continue;
        }
        _pRxCharacteristic->registerForNotify(notifyCallback);
        BLECommunication::_notfiyCharacteristicMap[_pRxCharacteristic] = this;
        Log.trace("[BLETask] registered for notify");

        //set the connect counter to connect immediately
        _connect_counter = BLE_RECONNECT_CYCLES; 
        _connected = true;
        //Setup callbacks for onConnect / onDisconnect events
        client->setClientCallbacks(this);
        Log.trace("[BLETask] set client callbacks");

        Log.notice("[BLETask] connected");

        while(_connected){
            //Waits until the connection semaphore is given from the onDisconnected method,
            //which also sets the _connected flag. This allows other tasks to run in the meantime
            _connectionSemaphore.take("BLETask");
        }

        Log.notice("[BLETask] disconnected");

        //Remove callbacks
        client->setClientCallbacks(nullptr);
    }
}

//Wrapper to start the BLE communication task, as only static functions can be used as a task function
//Casts the user data argument to the BLECommunication type and invokes the real BLE task method
void BLECommunication::bleTaskWrapper(void* arg) {
    ((BLECommunication*)arg)->bleTask();
}

//BLEAdvertisedDeviceCallbacks implementation
//Called when scan results arrive
void BLECommunication::onResult(BLEAdvertisedDevice advertisedDevice) {
    if(!_device_found) {
        Log.trace("[BLETask] Advertised Device found: %s", advertisedDevice.toString().c_str());
        //Check if the name of the device matches
        if(advertisedDevice.haveName() && advertisedDevice.getName() == PROTOCOL_BLE_DEVICE_NAME) {
            //Check if the device has the correct services
            if (advertisedDevice.haveServiceUUID() && advertisedDevice.isAdvertisingService(serviceUUID)) {
                _device_found = true;
                _device = advertisedDevice;
            }
        }
    }
}

//Updates the BLE communication, called from update
//Interacts with the BLE communcation task
void BLECommunication::interfaceUpdate() {
    //Retrieve incoming messages from the message queue
    rx_data_t rx_data;
    while(pdPASS == xQueueReceive(_rx_data_queue, &rx_data, 0)) {
        _protocol.handle(rx_data.data, rx_data.data_length);
    }
    
    if(_connected) {
        //Update the connection lifecycle and try to reconnect in case of errors
        //This handles the initialization and heartbeat messages
        if(_lifecycle.connection_status != ConnectionStatus::connected && _lifecycle.connection_status != ConnectionStatus::connecting) {
            _connect_counter++;
            if(_connect_counter >= BLE_RECONNECT_CYCLES) {
                _connect_counter = 0;
                _lifecycle.connect();
            }
        }
        _lifecycle.update();
    }

    //Set the correct status of the ble communcation interface in the shared data structure
    //This is used for the status leds
    _sharedData->bleStatus = _lifecycle.connection_status;
    if(_connected && _lifecycle.connection_status == ConnectionStatus::disconnected) {
        _sharedData->bleStatus = ConnectionStatus::connecting;
    }
}

//Handles sending the bytes over the BLE interface, called by the Protocol object
void BLECommunication::handleWrite(uint8_t* data, uint16_t length, void* user_data) {
    BLECommunication* comm = (BLECommunication*) user_data;
    if(!comm->_connected) {
        Log.warning(F("[BLETask] Interface not connected, cannot write messages"));
        return;
    }
    while(length > 0) {
        //A BLE characterstic can only handle 20 bytes at a time
        uint16_t to_write = min(length, (uint16_t)20);
        if(comm->_pTxCharacteristic == nullptr) {
            Log.error("[BLETask] TXCharacteristic == null");
            return;
        }
        //Write the data
        comm->_pTxCharacteristic->writeValue(data, to_write);
        length -= to_write;
        data += to_write;
    }
}

//Callback which is called when a BLE notification is received
void BLECommunication::notifyCallback(
  BLERemoteCharacteristic* pBLERemoteCharacteristic,
  uint8_t* pData,
  size_t length,
  bool isNotify) {
    BLECommunication* comm = BLECommunication::_notfiyCharacteristicMap[pBLERemoteCharacteristic];
    if(comm != nullptr) {
        if(length > 20) {
            //A ble characterstic should never contain more than 20 bytes
            Log.error("Invalid Notify Callback Data Length: %d", length);
            return;
        }
        //Send data to the queue to be handled by the main task
        rx_data_t data_to_queue;
        data_to_queue.data_length = length;
        memcpy(data_to_queue.data, pData, length);
        xQueueSend(comm->_rx_data_queue, &data_to_queue, 100);
    }
}

//BLEClientCallbacks implementation
void BLECommunication::onConnect(BLEClient* pclient) {

}

//BLEClientCallbacks implementation
void BLECommunication::onDisconnect(BLEClient* pclient) {
    Log.trace("[BLETask] >>>onDisconnect");
    if(_connected) {
        //Set the _connected flag to false and give the connection semaphore,
        //which will wake the BLE communication task
        _connected = false;
        _connectionSemaphore.give();
    }
    Log.trace("[BLETask] <<<onDisconnect");
}

/* End Protocol implementation */

Protocol* BLECommunication::getProtocol() {
    return &_protocol;
}

ConnectionLifecycle* BLECommunication::getLifecycle() {
    return &_lifecycle;
}


void BLECommunication::setForwardLink(Protocol* protocol, ConnectionLifecycle* lifecycle) {
    _forwardProtocol = protocol;
    _lifecycle.setMonitorLifecycle(lifecycle);
}