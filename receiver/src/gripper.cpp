#include "gripper.h"
#include "Arduino.h"
#include "ArduinoLog.h"

//Constant definitions, index 0 = pinky, index 4 = thumb

#define PWM_FREQUENCY 50 //Hz
#define PWM_RESOLUTION 16 //Bits

static const float MAX_FINGER_SPEED = 100.0; // %/s
static const float MAX_FINGER_CHANGE = MAX_FINGER_SPEED * GRIPPER_SYSTEM_UPDATE_RATE * 1e-6;

static const float LOAD_FILTER_ALPHA = 0.8;

//Pins which are used to measure the gripper load
static const uint8_t GRIPPER_LOAD_PINS[] = {
    39, 34, 35, 32, 33
};

//Pins which are used to control the gripper position
static const uint8_t GRIPPER_PWM_PINS[] = {
    26, 27, 14, 12, 13
};

//PWM channels which generate the signals for each finger
static const uint8_t GRIPPER_PWM_CHANNELS[] = {
    0, 1, 2, 3, 4
};

//Minium and maximum duty cycle values [0]->0% [1]->100% closed
static const uint32_t GRIPPER_PWM_CALIB_DATA[][2] = {
    {6400, 3300},
    {6400, 3300},
    {6400, 3300},
    {6400, 3300},
    {6400, 3300},
};

//Thresholds at which a finger is considred blocked
static const uint16_t GRIPPER_BLOCK_THRESHOLD[] = {
    75, 75, 75, 75, 75
};

GripperSystem::GripperSystem(SharedData* sharedData) : System(GRIPPER_SYSTEM_UPDATE_RATE), _sharedData(sharedData) {
}

/* Initialization of the system*/
void GripperSystem::init(SystemManager* systemManager) {
    for (uint8_t i = 0; i < 5; i++)
    {
        //Setup PWM signal for finger
        ledcSetup(GRIPPER_PWM_CHANNELS[i], PWM_FREQUENCY, PWM_RESOLUTION);
        pinMode(GRIPPER_PWM_PINS[i], OUTPUT);
        ledcAttachPin(GRIPPER_PWM_PINS[i], GRIPPER_PWM_CHANNELS[i]);
        disableFinger(i);
        //Setup load measuring pins
        pinMode(GRIPPER_LOAD_PINS[i], INPUT);
        analogSetPinAttenuation(GRIPPER_LOAD_PINS[i], adc_attenuation_t::ADC_0db);
    }

    _sharedData->gripperEnabled = true;
}

//Function which disables a finger
void GripperSystem::disableFinger(uint8_t finger) {
    ledcWrite(GRIPPER_PWM_CHANNELS[finger], 0); //Stopping the PWM pulses stops the servo positioning controller
}

//Function which drives the finger to the specified position given in percent
void GripperSystem::writeFingerPosition(uint8_t finger, float position) {
    const float MIN = GRIPPER_PWM_CALIB_DATA[finger][0];
    const float MAX = GRIPPER_PWM_CALIB_DATA[finger][1];
    uint32_t dutyCycle = MIN + (MAX-MIN) * position / 100.0;
    Log.trace("Finger %d: pos = %F, duty = %d", finger, position, dutyCycle);
    ledcWrite(GRIPPER_PWM_CHANNELS[finger], dutyCycle);
}

/* Update the system */
void GripperSystem::update() {
    for (uint8_t i = 0; i < 5; i++)
    {
        float delta = 0.0;
        if(_sharedData->gripperEnabled) {
            //Update finger position
            float target = _sharedData->fingers[i].target_position;
            float current = _sharedData->fingers[i].current_position;
            //Calculate the difference to the target position
            delta = target - current;
            //Constrain the change to +-MAX_FINGER_CHANGE
            delta = constrain(delta, -MAX_FINGER_CHANGE, +MAX_FINGER_CHANGE);
            //Change the current position by delta and write the new position
            current += delta;
            writeFingerPosition(i, current);
            _sharedData->fingers[i].current_position = current;
        } else {
            //Disable finger
            disableFinger(i);
        }

        //Update finger load reading
        uint16_t value = analogRead(GRIPPER_LOAD_PINS[i]);   //Read new value
        uint16_t last_value = _sharedData->fingers[i].current_load; //Read last value
        uint16_t new_value = last_value*LOAD_FILTER_ALPHA + value*(1-LOAD_FILTER_ALPHA); //Average new value with old value
        _sharedData->fingers[i].current_load = new_value;

        //Block fingers when closing hand and load is over the threshold
        _sharedData->fingers[i].block = (delta > 0) && (new_value > GRIPPER_BLOCK_THRESHOLD[i]);
    }
}