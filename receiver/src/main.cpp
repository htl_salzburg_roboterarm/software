#include "Arduino.h"
#include "ArduinoLog.h"
#include "inttypes.h"
#include "shared_data.h"
#include "system.h"
#include "ble_comm.h"
#include "serial_comm.h"
#include "gripper.h"
#include "ultrasonic.h"
#include "status_leds.h"
#include "pc_interface.h"
#include "math.h"

#include "Wire.h"

//Structure to share data between systems
SharedData sharedData;

//Construct all the systems
BLECommunication ble_comm(&sharedData);
SerialCommunication serial_comm(&sharedData);
GripperSystem gripper_system(&sharedData);
UltrasonicSystem ultrasonic_system(&sharedData);
StatusLedSystem led_system(&sharedData);
PCInterface pc_interface(&sharedData);

//Add all the systems to an array
System* systems[] = {
  &ble_comm,
  &serial_comm,
  &gripper_system,
  &led_system,
  &ultrasonic_system,
  &pc_interface,
};
//Create the SystemManager which updates the systems
SystemManager manager(systems, sizeof(systems)/sizeof(System*), 10000);

void printNewLine(Print* _logOutput) {
  _logOutput->print('\n');
}

void notify_error(uint8_t error_level, uint8_t error_code) {
  switch(error_level) {
    case LEVEL_ERROR:
      Log.fatal(F("Critical Error: code %X"), error_code);
      while(1);

    case LEVEL_WARNING:
      Log.warning(F("Warning notification: code %X"), error_code);
  }
}

void setup() {
  //Start the USB serial interface
  Serial.begin(115200);
  Serial.println("Starting Arduino BLE Client application...");
  BLEDevice::init("");

  //Start the logging library
  Log.begin(LOG_LEVEL_NOTICE, &Serial);
  Log.setSuffix(printNewLine);

  manager.setErrorCallback(notify_error);
 
  ble_comm.setForwardLink(serial_comm.getProtocol(), serial_comm.getLifecycle());
  serial_comm.setForwardLink(ble_comm.getProtocol(), ble_comm.getLifecycle());
  //Initialize all systems
  manager.init(micros());
}

void loop() {
  //Update all systems
  manager.update(micros());
}