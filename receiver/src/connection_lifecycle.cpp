#include "connection_lifecycle.h"
#include "ArduinoLog.h"

ConnectionLifecycle::ConnectionLifecycle(Protocol* protocol, uint16_t timeOutTicks, uint16_t heartBeatInterval, const char* interfaceName) :
_protocol(protocol), _timeOutTicks(timeOutTicks), _heartBeatInterval(heartBeatInterval), _interfaceName(interfaceName) 
{
    //Register message callbacks
    protocol->registerMessageCallback(msg::init_ack, handleInitAckMsg, this);
    protocol->registerMessageCallback(msg::heartbeat, handleHeartbeatMsg, this);
}

//msg::init_ack handler
void ConnectionLifecycle::handleInitAckMsg(uint8_t* data, void* user_data) {
    msg_data_init_ack_t* msg = (msg_data_init_ack_t*) data;
    ConnectionLifecycle* cl = (ConnectionLifecycle*) user_data;
    //Check if we are currently trying to connect
    if(cl->connection_status == ConnectionStatus::connecting) {    
        //Check if the connection was successfull or there was a mismatch between protocol versions
        if(msg->success) {
            Log.notice(F("[Interface %s] Connection established."), cl->_interfaceName);
            cl->setConnectionStatus(ConnectionStatus::connected);
            cl->_timeOutCounter = 0; // Reset timeout
        } else {
            Log.notice(F("[Interface %s] Connection Version Mismatch."), cl->_interfaceName);
            cl->setConnectionStatus(ConnectionStatus::error_version_mismatch);
        }
    } else {
        Log.warning(F("[Interface %s] Received INIT_ACK message, but connection status is not CONN_STATUS_CONNECTING"), cl->_interfaceName);
    }
}

//msg::heartbeat handler
void ConnectionLifecycle::handleHeartbeatMsg(uint8_t* data, void* user_data) {
    ConnectionLifecycle* cl = (ConnectionLifecycle*) user_data;
    if(cl->connection_status == ConnectionStatus::connected) {
        Log.trace(F("[Interface %s] Received heartbeat: %d ticks"), cl->_interfaceName, cl->_timeOutCounter);
        //Reset counter
        cl->_timeOutCounter = 0;
    }
}

void ConnectionLifecycle::update() {
    //Check heartbeat messages
    if(connection_status == ConnectionStatus::connected || connection_status == ConnectionStatus::connecting) {
        //Check for timeout
        _timeOutCounter++;
        if(_timeOutCounter >= _timeOutTicks) {
            setConnectionStatus(ConnectionStatus::error_timeout);
            Log.warning(F("[Interface %s] Connection timed out"), _interfaceName);
        }
    }

    if(connection_status == ConnectionStatus::connected) {
        //Send heartbeat messages at regular intervals
        _heartBeatCounter++;
        if(_heartBeatCounter >= _heartBeatInterval) {
            _heartBeatCounter = 0;
            msg_data_heartbeat_t m;
            //set the link_up flag to true if the monitored connection is 'connected'
            m.link_up = _monitor_lifecycle == nullptr || _monitor_lifecycle->connection_status == ConnectionStatus::connected;
            
            Log.trace(F("[Interface %s] Sending heartbeat (linkup = %d)"), _interfaceName, m.link_up);
            _protocol->send(&m);
        }
    }
}

void ConnectionLifecycle::connect() {
    if(connection_status == ConnectionStatus::connected || connection_status == ConnectionStatus::connecting) {
        return;
    }
    //Set the status to connecting
    setConnectionStatus(ConnectionStatus::connecting);
    _timeOutCounter = 0;
    //Send an initialization message with the protocol version
    msg_data_init_t m;
    m.version_code = PROTOCOL_VERSION;
    _protocol->send(&m);
}

void ConnectionLifecycle::setConnectionStatus(ConnectionStatus status) {
    connection_status = status;
    switch (connection_status)
    {
    case ConnectionStatus::disconnected: Log.notice(F("[Interface %s] Status: Disconnected"), _interfaceName); break;
    case ConnectionStatus::connecting: Log.notice(F("[Interface %s] Status: Connecting"), _interfaceName); break;
    case ConnectionStatus::connected: Log.notice(F("[Interface %s] Status: Connected"), _interfaceName); break;
    case ConnectionStatus::error_version_mismatch: Log.notice(F("[Interface %s] Status: Version Mismatch"), _interfaceName); break;
    case ConnectionStatus::error_timeout: Log.notice(F("[Interface %s] Status: Timeout"), _interfaceName); break;
    case ConnectionStatus::error_unknown: Log.notice(F("[Interface %s] Status: Unknown Error"), _interfaceName); break;
    }
}

void ConnectionLifecycle::setMonitorLifecycle(ConnectionLifecycle* lifecycle) {
    _monitor_lifecycle = lifecycle;
}