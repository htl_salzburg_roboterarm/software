#include "serial_comm.h"
#include "Arduino.h"
#include "ArduinoLog.h"

#define SERIAL_COMM_INTERFACE Serial2

SerialCommunication::SerialCommunication(SharedData* sharedData) : System(SERIAL_COMM_UPDATE_RATE_US),
_sharedData(sharedData),
_lifecycle(&_protocol, SERIAL_TIMEOUT_TICKS, SERIAL_HEARTBEAT_TICKS, "Serial")
{
    //Register write-function and message callbacks
    _protocol.setWriteFunction(handleWrite, this);
    _protocol.registerDefaultCallback(defaultMsgCallback, this);
}

//Implement the write-function for the Protocol object
void SerialCommunication::handleWrite(uint8_t* data, uint16_t length, void* user_data) {
    SERIAL_COMM_INTERFACE.write(data, length);
}

void SerialCommunication::defaultMsgCallback(bool handled, msg msg_code, uint16_t msg_size, uint8_t* data, void* user_data) {
    //Forward unknown messages (= have not been handled by other message handlers)
    if(!handled) {
        SerialCommunication* comm = (SerialCommunication*) user_data;
        if(comm->_forwardProtocol != nullptr) {
            comm->_forwardProtocol->sendRaw(msg_code, msg_size, data);
        }
    }
}

/* Initialization of the system */
void SerialCommunication::init(SystemManager* manager) {
    SERIAL_COMM_INTERFACE.begin(115200);
    _connect_counter = SERIAL_RECONNECT_CYCLES; //Try to connect immediately
}

/* Update the system */
void SerialCommunication::update() {
    //Interface update: hand over all received bytes to the Protocol object for decoding
    int available;
    while((available = SERIAL_COMM_INTERFACE.available())) {
        uint16_t length = SERIAL_COMM_INTERFACE.readBytes(rx_buffer, min((int)sizeof(rx_buffer), available));
        _protocol.handle(rx_buffer, length);
    }

    //Update the connection lifecycle and try to reconnect in case of errors
    //This handles the initialization and heartbeat messages
    if(_lifecycle.connection_status != ConnectionStatus::connected && _lifecycle.connection_status != ConnectionStatus::connecting) {
        _connect_counter++;
        if(_connect_counter >= SERIAL_RECONNECT_CYCLES) {
            _connect_counter = 0;
            _lifecycle.connect();
        }
    }

    _lifecycle.update();

    //Set the correct status of the serial communcation interface in the shared data structure
    //This is used for the status leds
    _sharedData->serialStatus = _lifecycle.connection_status;

    //Transfer ultrasonic readings
    _ultrasonic_send_counter++;
    if(_ultrasonic_send_counter >= ULTRASONIC_SEND_INTERVAL) {
        _ultrasonic_send_counter = 0;
        msg_data_distance_readings_t msg;
        memcpy(msg.distances, _sharedData->distances, sizeof(msg.distances));
        _protocol.send(&msg);
    }
}

Protocol* SerialCommunication::getProtocol() {
    return &_protocol;
}

ConnectionLifecycle* SerialCommunication::getLifecycle() {
    return &_lifecycle;
}

void SerialCommunication::setForwardLink(Protocol* protocol, ConnectionLifecycle* lifecycle) {
    _forwardProtocol = protocol;
    _lifecycle.setMonitorLifecycle(lifecycle);
}