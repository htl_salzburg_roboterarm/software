#include "ultrasonic.h"
#include "Arduino.h"
#include "ArduinoLog.h"

static const uint8_t RESET_SLAVE = 15;
static const uint8_t US_SDA = 21;
static const uint8_t US_SDL = 22;

static const uint8_t SLAVE_ADDR_1 = 32;
static const uint8_t SLAVE_ADDR_2 = 33;

/* UltrasonicSystem implementation */

UltrasonicSystem::UltrasonicSystem(SharedData* sharedData) :
System(ULTRASONIC_SYSTEM_UPDATE_RATE), _sharedData(sharedData),
_reader1(SLAVE_ADDR_1), _reader2(SLAVE_ADDR_2) {

}

/* Initialization of the system */
void UltrasonicSystem::init(SystemManager* systemManager) {
    pinMode(RESET_SLAVE, OUTPUT);
    digitalWrite(RESET_SLAVE, LOW); //Enable slave
    Wire.begin(US_SDA, US_SDL);

    _sharedData->ultrasonic1_available = _reader1.begin();
    if(!_sharedData->ultrasonic1_available) {
        Log.fatal("Couldn't connect to ultrasonic slave 1");
    } else {
        _reader1.setPingInterval(200);
        _reader1.setEnabled(true);
    }
    _sharedData->ultrasonic2_available = _reader2.begin();
    if(!_sharedData->ultrasonic2_available) {
        Log.fatal("Couldn't connect to ultrasonic slave 2");
    } else {
        _reader2.setPingInterval(200);
        _reader2.setEnabled(true);
    }
}

/* update the system */
void UltrasonicSystem::update() {
    //Read all sensor values if the sensors are available
    if(_sharedData->ultrasonic1_available) _reader1.readAllSensors(&_sharedData->distances[0]); //Read distances 0-7
    if(_sharedData->ultrasonic2_available) _reader2.readAllSensors(&_sharedData->distances[8]); //Read distances 8-16
}


/* UltrasonicReader implementation */

UltrasonicReader::UltrasonicReader(uint8_t address) : _address(address) {

}

bool UltrasonicReader::begin(uint8_t numTries, uint32_t interval) {
    uint8_t attemt = 0;
    while(numTries == 0 || attemt < numTries) {
        //Use low byte from micros as test value
        uint8_t test_value = (uint8_t) (micros() & 0xFF);
        //Write the value to the test register
        writeRegister(UltrasonicRegister::comm_test, test_value);
        uint8_t compare_value = 0;
        //Read the value back from the register
        readRegister(UltrasonicRegister::comm_test, &compare_value);

        //Check if the values match
        if(test_value == compare_value) {
            return true;
        }

        //Wait and retry
        Log.warning("Connection to UltrasonicReader (addr = %d) failed, retrying in %dms\n", _address, interval);
        delay(interval);
        attemt++;
    }
    return false;
}

void UltrasonicReader::setEnabled(bool enabled) {
    writeRegister(UltrasonicRegister::enable, enabled);
}

void UltrasonicReader::setPingInterval(uint16_t millis) {
    writeRegister(UltrasonicRegister::ping_interval, millis);
}

void UltrasonicReader::readAllSensors(uint8_t* readings) {
    readRegister(UltrasonicRegister::read_sensors_all, readings, 8);
}