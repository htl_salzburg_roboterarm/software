#include "status_leds.h"
#include "Arduino.h"

static const uint8_t INDEX_BLE = 0;
static const uint8_t INDEX_SERIAL = 1;

static const uint8_t LED_PINS[] = {
    18, //BLE
    5   //Serial
};

static const uint32_t* PATTERN_DISCONNECTED = nullptr;          //LED off
static const uint32_t PATTERN_CONNECTING[] = {100, 100, 0};      //5Hz blinking
//static const uint8_t PATTERN_CONNECTED = {0, 0};              //Unused LED always on
static const uint32_t* PATTERN_ERROR_TIMEOUT = nullptr;           //LED off
static const uint32_t PATTERN_ERROR_VERSION_MISMATCH[] = {25, 25, 25, 25, 25, 175, 0}; //5Hz triple blink
static const uint32_t PATTERN_ERROR_UNKNOWN[] = {50, 50, 50, 50, 50, 150, 0};  //3.3Hz triple blink



StatusLedSystem::StatusLedSystem(SharedData* sharedData) : System(LED_SYSTEM_UPDATE_RATE), _sharedData(sharedData) {

}

/* Initialization of the system */
void StatusLedSystem::init(SystemManager* systemManager) {
    initLed(INDEX_BLE);
    initLed(INDEX_SERIAL);
}

void StatusLedSystem::initLed(uint8_t index) {
   pinMode(LED_PINS[index], OUTPUT);
}

/* Update the system */
void StatusLedSystem::update() {
    uint32_t time = millis();
    updateLed(INDEX_BLE, _sharedData->bleStatus, time);
    updateLed(INDEX_SERIAL, _sharedData->serialStatus, time);
}


/* updates a status led based on the current connection status and time */
void StatusLedSystem::updateLed(uint8_t index, ConnectionStatus status, uint32_t time) {
    bool state; //State of the led (on / off)
    if(status == ConnectionStatus::connected) {
        state = true; //led permanently on
    } else {
        const uint32_t* pattern = nullptr; //Choose pattern based on connection state
        switch (status)
        {
        case ConnectionStatus::disconnected: pattern = PATTERN_DISCONNECTED; break;
        case ConnectionStatus::connecting: pattern = PATTERN_CONNECTING; break;
        case ConnectionStatus::error_timeout: pattern = PATTERN_ERROR_TIMEOUT; break;
        case ConnectionStatus::error_version_mismatch: pattern = PATTERN_ERROR_VERSION_MISMATCH; break;
        case ConnectionStatus::error_unknown: pattern = PATTERN_ERROR_UNKNOWN; break;
        default:  pattern = PATTERN_ERROR_UNKNOWN; break;
        }
    
        //Change the pattern if it is different from the current one
        if(pattern != _patternPlayers[index].getCurrentPattern()) {
            _patternPlayers[index].play(pattern, true);
        }

        //Execute the PatternPlayer and set the led state
        state = _patternPlayers[index].update(time);
    }

    //Write the led state
    digitalWrite(LED_PINS[index], state);
    
}