#ifndef ULTRASONIC_REGISTERS_H
#define ULTRASONIC_REGISTERS_H

#include "inttypes.h"

enum class UltrasonicRegister : uint8_t {
    none = 0x00,
    comm_test = 0x01,           //1 byte read/write

    //Configuration registers
    enable = 0x10,              //1 byte write
    ping_interval = 0x11,       //2 byte write

    //Sensor data registers
    read_sensor_0 = 0x20,       //1 byte read
    read_sensor_1,              //1 byte read
    read_sensor_2,              //1 byte read
    read_sensor_3,              //1 byte read
    read_sensor_4,              //1 byte read
    read_sensor_5,              //1 byte read
    read_sensor_6,              //1 byte read
    read_sensor_7,              //1 byte read

    read_sensors_all = 0x30,    //8 byte read
};

#endif