#define DISABLE_LOGGING

#include <Arduino.h>
#include <Wire.h>
#include <ArduinoLog.h>

#include "ultrasonic_registers.h"

#ifndef I2C_SLAVE_ADDRESS
#define I2C_SLAVE_ADDRESS 32
#endif

//Sensor pins in format {TRIG_PIN, ECHO_PIN}
constexpr uint8_t SENSOR_PINS[][2] = {
    {8, 7},
    {6, 5},
    {2, 3},
    {4, 9},
    {10, 11},
    {12, A3},
    {A2, A1},
    {A0, 13}
};

constexpr uint8_t trigPin(uint8_t sensor) {
    return SENSOR_PINS[sensor][0];
}

constexpr uint8_t echoPin(uint8_t sensor) {
    return SENSOR_PINS[sensor][1];
}

//Minimum sensor pinging interval in ms
constexpr uint16_t minPingInterval = 200; 

//Current I2C reading register
UltrasonicRegister i2cReg = UltrasonicRegister::none; 

//Current communication test register
uint8_t comm_test_register = 0;
//Current enabled state
bool enabled = false;
//Current pinging interval
uint16_t pingInterval = minPingInterval;
//Sensor data buffers, one is used to serve I2C request, while the other is filled with new data
uint8_t sensor_data_1[8];
uint8_t sensor_data_2[8];

//Pointer to the buffer containing the data to send over I2C
uint8_t* current_data = sensor_data_1;
//Pointer to the buffer to store the new sensor values
uint8_t* new_data = sensor_data_2;

//Last sensor ping time
uint32_t last_ping;

//Forward declaration of functions
uint8_t readSensor(uint8_t sensor);     //Perform ultrasonic sensor reading for a specific sensor
void sendSensorData(uint8_t sensor);    //Sends the sensor data for a specific sensor
template<class T>
void writeReg(T* dest);                 //Write a register with data from I2C
template<class T>
void readReg(T* src);                   //Send a register over I2C
void wireReceive(int bytes);            //Wire receive handler   
void wireRequest();                     //Wire request handler



void setup() {
    Serial.begin(9600);
    //Log.begin(LOG_LEVEL_NOTICE, &Serial);
    Serial.println("Ultrasonic Controller booting...");
    Serial.print("I2C Address: ");
    Serial.println(I2C_SLAVE_ADDRESS);
    Wire.begin(I2C_SLAVE_ADDRESS);
    Wire.onRequest(wireRequest);
    Wire.onReceive(wireReceive);
    Wire.setTimeout(0);

    for (uint8_t i = 0; i < 8; i++)
    {
        pinMode(trigPin(i), OUTPUT);
        pinMode(echoPin(i), INPUT);
        digitalWrite(trigPin(i), LOW);
    }
    
    last_ping = millis();
}

void loop() {
    if (pingInterval < minPingInterval) pingInterval = minPingInterval;

    if(enabled) {
        uint32_t time = millis();
        if(time - last_ping >= pingInterval) {
            Serial.printf("Pinging (t = %d)...\n", time);
            last_ping = time;
            for (uint8_t i = 0; i < 8; i++)
            {
                //Read new sensor data into new_data buffer
                new_data[i] = readSensor(i);
                //new_data[i] = (I2C_SLAVE_ADDRESS == 32 ? 0 : 8) + i;
            }
            //Swap data buffers, after this reads will return the new values
            uint8_t* tmp = current_data;
            current_data = new_data;
            new_data = tmp;
        }
    }
}

uint8_t readSensor(uint8_t sensor) {
    digitalWrite(trigPin(sensor), HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin(sensor), LOW);
    uint32_t duration = pulseIn(echoPin(sensor), HIGH, 15000); //max 15ms, ca. 255cm
    if(duration == 0) return 255; //0 duration means timeout -> return maximum distance
    uint32_t cm = duration / 58; //distance = v*t / 2 = 343 m/s * 0.000001 s/µs * 100 cm/m * t[µs] / 2 = t[µs] / 58
    if(cm > 255) cm = 255;       //clamp to 255cm max
    return (uint8_t) cm;
}

//Write a register
template<class T>
void writeReg(T* dest) {
    //Serial.println(Wire.available());
    size_t length = sizeof(T);
    int available = Wire.available();
    if(Wire.available() < (int32_t)length) {
        Log.trace("WriteReg: size=%d, available=%d => no write\n", length, available);
        return;
    }
    Wire.readBytes((uint8_t*)dest, length);
    Log.trace("WriteReg: size=%d, available=%d => value %d\n", length, available, *dest);
}

//Called when data is received from I2C
void wireReceive(int bytes) {
    i2cReg = static_cast<UltrasonicRegister>(Wire.read());
    Log.trace("I2C Receive: reg = %d, data-length=%d\n", static_cast<uint8_t>(i2cReg), bytes);
    
    if(bytes > 1) {
        //Write to a register
        switch (i2cReg)
        {
        case UltrasonicRegister::comm_test: writeReg(&comm_test_register); break;
        case UltrasonicRegister::enable: writeReg(&enabled); break;
        case UltrasonicRegister::ping_interval: writeReg(&pingInterval); break;
        default: break;
        }
        i2cReg = UltrasonicRegister::none;
    }

    while (Wire.available()) {
        Wire.read(); //Read all remaining bytes in buffer
    }
}

//Called when data is requested from I2C
void wireRequest() {
    Log.trace("I2C Request: current-reg=%d\n", static_cast<uint8_t>(i2cReg));
    switch (i2cReg)
    {
    case UltrasonicRegister::comm_test:
        {
            Wire.write(comm_test_register);
        }
        break;
    case UltrasonicRegister::read_sensor_0:
    case UltrasonicRegister::read_sensor_1:
    case UltrasonicRegister::read_sensor_2:
    case UltrasonicRegister::read_sensor_3:
    case UltrasonicRegister::read_sensor_4:
    case UltrasonicRegister::read_sensor_5:
    case UltrasonicRegister::read_sensor_6:
    case UltrasonicRegister::read_sensor_7:
        {
            uint8_t sensor = static_cast<uint8_t>(i2cReg) - static_cast<uint8_t>(UltrasonicRegister::read_sensor_0);
            Wire.write(current_data[sensor]);
        }
        break;
    case UltrasonicRegister::read_sensors_all:
        Wire.write(current_data, 8);
        break;
    default:
        Wire.write(0xEE);
    }
}