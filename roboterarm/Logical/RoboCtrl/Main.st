PROGRAM _INIT
	
	robo.AxisRefs[0] := ADR(gAxisQ1);
	robo.AxisRefs[1] := ADR(gAxisQ2);
	robo.AxisRefs[2] := ADR(gAxisQ3);
	robo.AxisRefs[3] := ADR(gAxisQ4);
	robo.AxisRefs[4] := ADR(gAxisQ5);
    
	robo.MpRobo.MpLink := ADR(g5AxRobA);
	robo.MpRobo.Enable := TRUE;
	robo.MpRobo.Parameters := ADR(robo.MpRoboPars);
	robo.MpRobo.Override := 100.0;
	
	robo.MpRoboPars.Jog.PathLimits.Velocity := 200;
	robo.MpRoboPars.Jog.PathLimits.Acceleration := 200;
	robo.MpRoboPars.Jog.PathLimits.Deceleration := 200;
	
	robo.speed := 150;
	robo.angle_speed := 400;
	robo.p := 0.01;
	
	robo.McGroupHome.AxesGroup := ADR(g5AxRobA);
	FOR tmp.index := 0 TO 4 DO
		robo.McGroupHome.HomingMode[tmp.index] := mcHOMING_INIT;
	END_FOR;
	//robo.McGroupHome.HomingMode[4] := mcHOMING_DIRECT;
	
	
	robo.cmd.follow := FALSE;
	robo.cmd.enable := FALSE;
	robo.cmd.reset_error := FALSE;
	robo.cmd.home_directions_checked := FALSE;
	robo.step := robot_manual;
	
	robo.center_positions[0] := 250;
	robo.center_positions[1] := 0;
	robo.center_positions[2] := 700;
	robo.center_positions[3] := -90;
	robo.center_positions[4] := -90;
	
	robo.transport_modes.mode1[0] := 0;
	robo.transport_modes.mode1[1] := -55;
	robo.transport_modes.mode1[2] := 25;
	robo.transport_modes.mode1[3] := -10;
	robo.transport_modes.mode1[4] := 0;
	
	robo.transport_modes.mode2[0] := 0;
	robo.transport_modes.mode2[1] := 80;
	robo.transport_modes.mode2[2] := -80;
	robo.transport_modes.mode2[3] := -80;
	robo.transport_modes.mode2[4] := 0;
	
	robo.transport_modes.default_mode[0] := 0;
	robo.transport_modes.default_mode[1] := 0;
	robo.transport_modes.default_mode[2] := 0;
	robo.transport_modes.default_mode[3] := 0;
	robo.transport_modes.default_mode[4] := 0;
END_PROGRAM

PROGRAM _CYCLIC
	robo.cmd.enable := global.robo.enable;
	robo.cmd.follow := global.robo.activate;
	robo.cmd.home_directions_checked := global.robo.confirm_home;
	global.robo.confirm_home := FALSE;
	IF global.robo.reset_error THEN
		robo.cmd.reset_error := TRUE;
		global.robo.reset_error := FALSE;
	END_IF;
	
	IF robo.MpRobo.IsHomed THEN
		FOR tmp.index := 0 TO 4 DO
			robo_positive_home_flags[tmp.index] := robo.MpRobo.Info.JointAxisPosition[tmp.index] < 0;
			robo_position[tmp.index] := robo.MpRobo.Info.JointAxisPosition[tmp.index];
		END_FOR;
	END_IF;
	
	IF robo.MpRobo.Error AND (robo.step <> robot_manual) THEN
		robo.step := robot_error;
	END_IF;
	
	IF (NOT robo.cmd.enable) AND (robo.step <> robot_error) THEN
		IF (robo.step = robot_home) OR (robo.step = robot_home_initial_position) THEN
			robo_auto_home := FALSE;
		END_IF;
		robo.step := robot_reset;
	END_IF;
	
	CASE robo.step OF
		robot_reset:
			robo.MpRobo.Power := FALSE;
			robo.MpRobo.Jog := FALSE;
			robo.MpRobo.MoveDirect := FALSE;
			robo.McInitHome.Execute := FALSE;
			robo.McGroupHome.Execute := FALSE;
			global.robo.move.x := 0;
			global.robo.move.y := 0;
			global.robo.move.z := 0;
			IF robo.cmd.enable THEN
				robo.step := robot_poweron;
			END_IF;
		
		robot_poweron:
			robo.MpRobo.Power := TRUE;
			IF robo.MpRobo.PowerOn THEN
				robo.step := robot_home_confirm;
			END_IF;
			
		robot_home_confirm:
			IF robo_auto_home OR EDGEPOS(robo.cmd.home_directions_checked) THEN
				tmp.home_index := 0;
				robo.step := robot_home_init;
				robo.direct_homing := robo_auto_home;
			END_IF;
			
		robot_home_init: //Index set to 0 before calling
			robo.McInitHome.Axis := robo.AxisRefs[tmp.home_index];
			IF robo.direct_homing THEN
				robo.McInitHome.HomingParameters.HomingMode := mcHOMING_DIRECT;
				robo.McInitHome.HomingParameters.Position := robo_position[tmp.home_index];
			ELSE
				robo.McInitHome.HomingParameters.HomingMode := mcHOMING_SWITCH_GATE;
				robo.McInitHome.HomingParameters.Position := 0.0;
			END_IF;
			
			IF tmp.home_index = 4 THEN
				robo.McInitHome.HomingParameters.StartVelocity := 2;
				robo.McInitHome.HomingParameters.HomingVelocity := 0.5;
			ELSE	
				robo.McInitHome.HomingParameters.StartVelocity := 2;
				robo.McInitHome.HomingParameters.HomingVelocity := 0.5;
			END_IF;
			
			robo.McInitHome.HomingParameters.Acceleration := 100;
			robo.McInitHome.HomingParameters.ReferencePulse := mcSWITCH_OFF;
			robo.McInitHome.HomingParameters.SwitchEdge := mcDIR_POSITIVE;
			robo.McInitHome.HomingParameters.HomingDirection := mcDIR_POSITIVE;
			IF robo_positive_home_flags[tmp.home_index] THEN
				robo.McInitHome.HomingParameters.StartDirection := mcDIR_POSITIVE;
			ELSE
				robo.McInitHome.HomingParameters.StartDirection := mcDIR_NEGATIVE;
			END_IF;
			robo.McInitHome.Execute := TRUE;
			
			robo.step := robot_home_init_wait;
			
		robot_home_init_wait:
			IF robo.McInitHome.Error THEN
				robo.step := robot_error;
			ELSIF robo.McInitHome.Done THEN
				robo.McInitHome.Execute := FALSE;
				tmp.home_index := tmp.home_index + 1;
				IF tmp.home_index > 4 THEN
					tmp.home_index := 0;
					robo.McGroupHome.Execute := TRUE;
					robo.step := robot_home;
				ELSE
					robo.step := robot_home_init;
				END_IF;
			END_IF;
		
		robot_home:
			IF robo.McGroupHome.Done AND (NOT robo.MpRobo.CommandBusy) THEN
				robo.McGroupHome.Execute := FALSE;
				
				//Drive to robot initial position or position near referencing switches
				robo.step := robot_home_initial_position;
				robo.MpRoboPars.CoordSystem := SEL(robo.direct_homing, mcMCS, mcACS);
				robo.MpRoboPars.ManualMoveType := mcMOVE_ABSOLUTE;
				FOR tmp.index := 0 TO 4 DO
					robo.MpRoboPars.Position[tmp.index] := SEL(robo.direct_homing, robo.center_positions[tmp.index], 5);
				END_FOR;
				robo.MpRobo.MoveDirect := TRUE;
			END_IF;
			
		robot_home_initial_position:
			IF robo.MpRobo.MoveDone THEN
				robo.MpRobo.MoveDirect := FALSE;
				IF robo.direct_homing THEN
					tmp.home_index := 0;
					robo.step := robot_home_init;
					robo.direct_homing := FALSE; //Do referencing
				ELSE
					robo.step := robot_idle;
				END_IF;
			END_IF;
		
		robot_idle:
			robo_auto_home := TRUE;
			IF robo.cmd.follow THEN
				robo.step := robot_follow;
			END_IF;
		
		robot_follow:			
			IF NOT robo.cmd.follow THEN
				robo.MpRobo.Jog := FALSE;
				robo.step := robot_idle;
			ELSE
				robo.MpRobo.Jog := TRUE;		
					
				tmp.move.x := robo.center_positions[0] + Constrain(global.robo.move.x, -100, 100) / 100.0 * 200.0;
				tmp.move.y := robo.center_positions[1] + Constrain(global.robo.move.y, -100, 100) / 100.0 * 200.0;
				tmp.move.z := robo.center_positions[2] + Constrain(global.robo.move.z, -100, 100) / 100.0 * 100.0;
				tmp.c := LREAL_TO_REAL(robo.center_positions[4] + robo_position[0]);
				
				tmp.delta.x := LREAL_TO_REAL(tmp.move.x - robo.MpRobo.X);
				tmp.delta.y := LREAL_TO_REAL(tmp.move.y - robo.MpRobo.Y);
				tmp.delta.z := LREAL_TO_REAL(tmp.move.z - robo.MpRobo.Z);
				tmp.b_delta := LREAL_TO_REAL(tmp.b - robo.MpRobo.B);
				tmp.c_delta := LREAL_TO_REAL(tmp.c - robo.MpRobo.C);
				
				tmp.delta.x := Constrain(tmp.delta.x * robo.p, -1.0, 1.0);
				tmp.delta.y := Constrain(tmp.delta.y * robo.p, -1.0, 1.0);
				tmp.delta.z := Constrain(tmp.delta.z * robo.p, -1.0, 1.0);
				tmp.b_delta := Constrain(tmp.b_delta * 0.01, -1.0, 1.0);
				tmp.c_delta := Constrain(tmp.c_delta * 0.01, -1.0, 1.0);
				
				robo.MpRoboPars.Jog.Velocity[0] := tmp.delta.x * robo.speed;
				robo.MpRoboPars.Jog.Velocity[1] := tmp.delta.y * robo.speed;
				robo.MpRoboPars.Jog.Velocity[2] := tmp.delta.z * robo.speed;
				robo.MpRoboPars.Jog.Velocity[3] := 0;
				robo.MpRoboPars.Jog.Velocity[4] := tmp.c_delta * robo.angle_speed;
				robo.MpRoboPars.Jog.CoordSystem := mcMCS;
				
			END_IF;		
			
			IF global.robo.transport_mode <> 0 THEN
				tmp.transport_mode := MUX(global.robo.transport_mode - 1, robo.transport_modes.mode1, robo.transport_modes.mode2, robo.transport_modes.default_mode);
				robo.step := robot_transport_mode;
				robo.MpRobo.Jog := FALSE;
			END_IF;
			
		robot_transport_mode:
			IF NOT robo.MpRobo.MoveActive THEN
				//Drive to robot initial position
				robo.step := robot_wait_transport_mode;
				robo.MpRoboPars.CoordSystem := mcACS;
				robo.MpRoboPars.ManualMoveType := mcMOVE_ABSOLUTE;
				FOR tmp.index := 0 TO 4 DO
					robo.MpRoboPars.Position[tmp.index] := tmp.transport_mode[tmp.index];
				END_FOR;
				robo.MpRobo.MoveDirect := TRUE;
			END_IF;
			
		robot_wait_transport_mode:
			IF robo.MpRobo.MoveDone THEN
				robo.MpRobo.MoveDirect := FALSE;
				global.robo.enable := FALSE;
				robo.step := robot_reset;
			END_IF;
		
		robot_error:
			//Disable all movement commands
			global.robo.enable := FALSE;
			robo.MpRobo.Power := FALSE;
			robo.MpRobo.Jog := FALSE;
			robo.MpRobo.MoveDirect := FALSE;
			robo.McInitHome.Execute := FALSE;
			robo.McGroupHome.Execute := FALSE;
			
			IF robo.cmd.reset_error THEN
				robo.cmd.reset_error := FALSE;
				robo.MpRobo.ErrorReset := TRUE;
			END_IF;
		
			IF NOT robo.MpRobo.Error THEN
				robo.MpRobo.ErrorReset := FALSE;
				robo.step := robot_reset;
			END_IF;

	END_CASE;
	
	//Reset one-time command inputs
	global.robo.transport_mode := 0;
	
	IF (robo.step = robot_idle) OR (robo.step = robot_follow) OR (robo.step = robot_transport_mode) OR (robo.step = robot_wait_transport_mode) THEN
		global.robo.state := task_state_on;
	ELSIF (robo.step = robot_error) THEN
		global.robo.state := task_state_error;
	ELSIF (robo.step = robot_reset) THEN
		global.robo.state := task_state_off;
	ELSIF (robo.step = robot_home_confirm) THEN
		global.robo.state := task_state_wait_confirmation;;
	ELSE
		global.robo.state := task_state_homing;
	END_IF;
    
	robo.MpRobo();
	robo.McInitHome();
	robo.McGroupHome();
     
END_PROGRAM

PROGRAM _EXIT

	robo.MpRobo.Power  := FALSE;
	robo.MpRobo.Home   := FALSE;
	robo.MpRobo.MoveDirect := FALSE;
	robo.MpRobo.MoveLinear := FALSE;
	robo.MpRobo();

	robo.MpRobo.Enable := FALSE;
	robo.MpRobo();

END_PROGRAM
