
TYPE
	robo_step : 
		(
		robot_reset,
		robot_poweron,
		robot_home_confirm,
		robot_home_init,
		robot_home_init_wait,
		robot_home,
		robot_home_initial_position,
		robot_idle,
		robot_follow,
		robot_rehome,
		robot_wait_rehome,
		robot_error,
		robot_manual,
		robot_transport_mode,
		robot_wait_transport_mode
		);
	robo_typ : 	STRUCT 
		step : robo_step;
		cmd : robo_cmd_typ;
		MpRobo : MpRoboArm5Axis;
		MpRoboPars : MpRoboArm5AxisParType;
		McInitHome : MC_BR_InitHome_StpAx;
		McGroupHome : MC_BR_GroupHome_15;
		AxisRefs : ARRAY[0..4]OF UDINT;
		speed : REAL;
		angle_speed : REAL;
		center_positions : ARRAY[0..4]OF REAL;
		p : REAL;
		transport_modes : robo_transport_mode_typ;
		direct_homing : BOOL;
	END_STRUCT;
	robo_cmd_typ : 	STRUCT 
		enable : BOOL;
		follow : BOOL;
		reset_error : BOOL;
		home_directions_checked : BOOL;
	END_STRUCT;
	robo_tmp_typ : 	STRUCT 
		index : USINT;
		home_index : USINT;
		move : robo_move_cmd;
		delta : robo_move_cmd;
		b : REAL;
		c : REAL;
		b_delta : REAL;
		c_delta : REAL;
		transport_mode : ARRAY[0..4]OF REAL;
	END_STRUCT;
	robo_transport_mode_typ : 	STRUCT 
		mode1 : ARRAY[0..4]OF REAL;
		mode2 : ARRAY[0..4]OF REAL;
		default_mode : ARRAY[0..4]OF REAL;
	END_STRUCT;
END_TYPE
