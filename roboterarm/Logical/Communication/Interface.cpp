#include "CommunicationCommon.h"

void readData();
void writeData();
void writeFunction(uint8_t* data, uint16_t length, void* user_data);

void interfaceInit() {
	protocol.setWriteFunction(writeFunction, nullptr);
}

void interfaceExit() {

}

void interfaceUpdate() {
	switch (comm.step)
	{
		case COMM_WAIT:
			comm.read.step = COMM_READ;
			comm.write.step = COMM_GBUF;
			memset(&comm.writeBuffer, 0, sizeof(comm.writeBuffer));
			memset(&comm.sendBuffer, 0, sizeof(comm.sendBuffer));
			comm.dataToSend = false;
			
			if(comm.cmd.connect) {
				comm.cmd.connect = false;
				comm.step = COMM_OPEN;
			}
			break;
		
		case COMM_OPEN:	//open serial interface
			//Parameters for FRM_xopen()
			comm.FRM_xopen_0.enable = true;
			comm.FRM_xopen_0.device = (UDINT) "IF1";									//Devicename --> see your serial interface properties
			comm.FRM_xopen_0.mode = (UDINT) "/PHY=RS232 /BD=115200 /DB=8 /PA=N /SB=1"; 	//Modestring --> specifies the serial operation mode
			comm.FRM_xopen_0.config = (UDINT) &comm.xopenConfig;						//Additional Parameters, optional
		
			//Additional Parameters for FRM_xopen()
			comm.xopenConfig.idle = 4;						//Idle time between two characters, at 19200 baud about 1sec
			comm.xopenConfig.delimc = 0;					//deactivate delimeters					
			comm.xopenConfig.tx_cnt = 3;					//number of transmit buffers
			comm.xopenConfig.rx_cnt = 3; 					//number of receive buffers
			comm.xopenConfig.tx_len = 256; 					//length of transmit buffers
			comm.xopenConfig.rx_len = 256; 					//lenght of receive buffers
			comm.xopenConfig.argc = 0;						//activate additional options
			comm.xopenConfig.argv = 0;						//parameters for additional options (check help)

			FRM_xopen(&comm.FRM_xopen_0);					//call the FRM_xopen() function
		
			if(comm.FRM_xopen_0.status == 0) {
				comm.step = COMM_PREPARE_BUFFERS;			//Interface opend successfully --> next step
			} else if(comm.FRM_xopen_0.status == BUSY) {
				comm.step = COMM_OPEN;						//operation not finished yet --> call again
			} else {
				comm.step = COMM_ERROR;						//function returned errorcode --> check help
			}
			break;
		
		case COMM_PREPARE_BUFFERS:
			comm.FRM_gbuf_0.enable = true;
			comm.FRM_gbuf_0.ident = comm.FRM_xopen_0.ident;
			
			FRM_gbuf(&comm.FRM_gbuf_0);											//call the FRM_gbuf() function
			
			if(comm.FRM_gbuf_0.status == 0) {
				memset((void*)comm.FRM_gbuf_0.buffer,0,comm.FRM_gbuf_0.buflng);	//clear sendbuffer
				comm.writeBuffer.address = comm.FRM_gbuf_0.buffer;				//set new writeBuffer
				comm.writeBuffer.capacity = comm.FRM_gbuf_0.buflng;
				comm.writeBuffer.dataLength = 0;
				
				comm.step = COMM_RUNNING;										//system returned a valid buffer --> next step
			} else if(comm.FRM_gbuf_0.status == BUSY) {
				comm.step = COMM_PREPARE_BUFFERS;								//operation not finished yet --> call again
			} else {
				comm.step = COMM_ERROR;											//function returned errorcode --> check help
			}
			break;
		
		case COMM_RUNNING:
			if(comm.cmd.disconnect) {									//requst to close the serial port
				comm.cmd.disconnect = false;							//disable command open_send
				comm.step = COMM_CLOSE;
			} else {
				readData();
				if(comm.read.step == COMM_READ_ERROR) {
					comm.step = COMM_ERROR;
					break;
				}
				writeData();
				if(comm.write.step == COMM_WRITE_ERROR) {
					comm.step = COMM_ERROR;
					break;
				}
			}
			break;
		
		case COMM_CLOSE:	//--- close the interface
			comm.FRM_close_0.enable = true;
			comm.FRM_close_0.ident = comm.FRM_xopen_0.ident;				//ident from FRM_xopen()			
			
			FRM_close(&comm.FRM_close_0);									//call the FRM_close() function
			
			if(comm.FRM_close_0.status == 0) {
				comm.step = COMM_WAIT;										//closed interface successfully --> wait step
			} else if(comm.FRM_close_0.status == BUSY) {
				comm.step = COMM_CLOSE;										//operation not finished yet --> call again
			} else {
				comm.step = COMM_ERROR;										//function returned errorcode --> check help
			}
			break;

		case COMM_ERROR:
			; //not implementet yet, check help for error codes
			break;
	}
}

void readData() {

	switch (comm.read.step)
	{
		case COMM_READ:	//--- read data from serial interface
			comm.FRM_read_0.enable = true;
			comm.FRM_read_0.ident = comm.FRM_xopen_0.ident;
			
			FRM_read(&comm.FRM_read_0);												//call the FRM_read() function
			
			if(comm.FRM_read_0.status == 0) {
				protocol.handle((uint8_t*)comm.FRM_read_0.buffer, comm.FRM_read_0.buflng);	//system returned a valid data --> handle protocol
				comm.read.step = COMM_RBUF;											//--> next step						
			} else if(comm.FRM_read_0.status == frmERR_NOINPUT) {
				comm.read.step = COMM_READ;											//no data available --> call again
			} else if(comm.FRM_read_0.status == frmERR_INPUTERROR) {
				comm.read.step = COMM_RBUF;											//received Frame with defective characters, skip data
			} else {
				comm.read.step = COMM_READ_ERROR;									//function returned errorcode --> check help
			}
			break;
		
		case COMM_RBUF:	//--- release readbuffer
			//Parameters for FRM_rbuf()
			comm.FRM_rbuf_0.enable = true;
			comm.FRM_rbuf_0.ident = comm.FRM_xopen_0.ident;					//ident from FRM_xopen()
			comm.FRM_rbuf_0.buffer = comm.FRM_read_0.buffer;				//read buffer
			comm.FRM_rbuf_0.buflng = comm.FRM_read_0.buflng;				//length of sendbuffer
   			
			FRM_rbuf(&comm.FRM_rbuf_0);										//call the FRM_rbuf() function
			
			if(comm.FRM_rbuf_0.status == 0) {
				comm.read.step = COMM_READ;										//buffer released --> next step
			} else if(comm.FRM_rbuf_0.status == BUSY) {
				comm.read.step = COMM_RBUF;										//operation not finished yet --> call again
			} else if(comm.FRM_rbuf_0.status == frmERR_INVALIDBUFFER) {				
				comm.read.step = COMM_READ;										//buffer is invalid --> read again
			} else {
				comm.read.step = COMM_READ_ERROR;								//function returned errorcode --> check help
			}
			break;
		
		case COMM_READ_ERROR:
			break;
	}
   
	
}

void writeData() {
	switch (comm.write.step)
	{
		case COMM_GBUF:	//--- aquire new writeBuffer
			comm.FRM_gbuf_0.enable = true;
			comm.FRM_gbuf_0.ident = comm.FRM_xopen_0.ident;
			
			FRM_gbuf(&comm.FRM_gbuf_0);											//call the FRM_gbuf() function
			
			if(comm.FRM_gbuf_0.status == 0) {
				memset((void*)comm.FRM_gbuf_0.buffer,0,comm.FRM_gbuf_0.buflng);	//clear sendbuffer
				comm.write.step = COMM_WAIT_DATA;								//system returned a valid buffer --> next step
			} else if(comm.FRM_gbuf_0.status == BUSY) {
				comm.write.step = COMM_GBUF;									//operation not finished yet --> call again
			} else {
				comm.write.step = COMM_WRITE_ERROR;								//function returned errorcode --> check help
			}			
			break;
		
		case COMM_WAIT_DATA: //--- wait for data to send
			if(comm.dataToSend) {
				comm.sendBuffer = comm.writeBuffer;								//swap buffers, move writeBuffer to sendBuffer
				comm.writeBuffer.address = comm.FRM_gbuf_0.buffer;				//set new writeBuffer
				comm.writeBuffer.capacity = comm.FRM_gbuf_0.buflng;
				comm.writeBuffer.dataLength = 0;
				
				comm.dataToSend = false;										//clear data to send flag
				comm.write.step = COMM_WRITE;									//--> next step
			}
			break;
		
		case COMM_WRITE:	//--- write data to interface
			//Parameters for FRM_write()
			comm.FRM_write_0.enable = true;
			comm.FRM_write_0.ident = comm.FRM_xopen_0.ident;					//ident from FRM_xopen()
			comm.FRM_write_0.buffer = comm.sendBuffer.address;					//sendbuffer
			comm.FRM_write_0.buflng = comm.sendBuffer.dataLength;				//net length of senddata
   			
			FRM_write(&comm.FRM_write_0);										//call the FRM_write() function
			
			if(comm.FRM_write_0.status == 0) {
				comm.write.step = COMM_GBUF;									//writing successful --> get new writeBuffer and swap buffers
			} else if(comm.FRM_write_0.status == BUSY) {
				comm.write.step = COMM_WRITE;									//operation not finished yet --> call again
			} else {
				comm.write.step = COMM_ROBUF;									//function returned errorcode --> check help
			}
			break;
		
		case COMM_ROBUF:	//--- release sendbuffer in case of no successful write operation
			comm.FRM_robuf_0.enable = true;
			comm.FRM_robuf_0.buffer = comm.sendBuffer.address;				//sendbuffer
			comm.FRM_robuf_0.buflng = comm.sendBuffer.capacity;				//buffer length
			comm.FRM_robuf_0.ident = comm.FRM_xopen_0.ident;				//ident open
		
			FRM_robuf(&comm.FRM_robuf_0);									//call the FRM_robuf() function
		
			if(comm.FRM_robuf_0.status == 0) {
				comm.write.step = COMM_GBUF;								//released buffer successful --> get new writeBuffer and swap buffers
			} else if(comm.FRM_robuf_0.status == BUSY) {
				comm.write.step = COMM_ROBUF;								//operation not finished yet --> call again
			} else {
				comm.write.step = COMM_WRITE_ERROR;							//function returned errorcode --> check help
			}
			break;
		
		case COMM_WRITE_ERROR:
			break;
	}
}


void writeFunction(uint8_t* data, uint16_t length, void* user_data) {
	if(comm.step != COMM_RUNNING) {
		//TODO log warning
		return;	
	}
	UINT remaining = comm.writeBuffer.capacity - comm.writeBuffer.dataLength;
	if(remaining < length) {
		//Not enough space in buffer, drop last elements
		//TODO log warning
		length = remaining;	
	}
	
	memcpy((void*) (comm.writeBuffer.address+comm.writeBuffer.dataLength), data, length);
	comm.writeBuffer.dataLength += length;
	comm.dataToSend = true;
}