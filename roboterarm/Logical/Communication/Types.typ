
TYPE
	Intervals_typ : 	STRUCT 
		battery : UDINT;
		brake : UDINT;
		vibro : UDINT;
		collision : UDINT;
	END_STRUCT;
	enCOMM_WRITE_STEPS : 
		(
		COMM_GBUF := 0, (*Defines Step for FRM_rbuf()*)
		COMM_WAIT_DATA,
		COMM_WRITE, (*Defines Step for FRM_write()*)
		COMM_ROBUF, (*Defines Step for FRM_robuf()*)
		COMM_WRITE_ERROR := 255
		);
	enCOMM_STEPS : 
		(
		COMM_WAIT := 0, (*Defines the wait step*)
		COMM_OPEN, (*Defines Step for FRM_xopen() *)
		COMM_PREPARE_BUFFERS,
		COMM_RUNNING,
		COMM_CLOSE, (*Defines Step for FRM_close()*)
		COMM_ERROR := 255 (*Defines Step for Errorhandling*)
		);
	enCOMM_READ_STEPS : 
		(
		COMM_READ := 0, (*Defines Step for FRM_read()*)
		COMM_RBUF, (*Defines Step for FRM_rbuf()*)
		COMM_READ_ERROR := 255
		);
	enCOMM_PROTOCOL_STATUS : 
		(
		STATUS_DISCONNECTED,
		STATUS_CONNECTION_INIT,
		STATUS_CONNECTED,
		STATUS_ERROR_VERSION_MISMATCH,
		STATUS_ERROR_TIMEOUT
		);
	Communication_Write_typ : 	STRUCT 
		step : enCOMM_WRITE_STEPS;
	END_STRUCT;
	Communication_typ : 	STRUCT 
		read : Communication_Read_typ;
		write : Communication_Write_typ;
		cmd : Command_typ;
		intervals : Intervals_typ;
		step : enCOMM_STEPS; (*Step of the Statemachine*)
		xopenConfig : XOPENCONFIG; (*Configuration Type for FRM_xopen()*)
		writeBuffer : Buffer_typ;
		sendBuffer : Buffer_typ;
		dataToSend : BOOL;
		status : enCOMM_PROTOCOL_STATUS;
		heartBeatCounter : UDINT;
		controlLinkUp : BOOL;
		movement : Movement_typ;
		FRM_xopen_0 : FRM_xopen; (*Functionblock FRM_xopen()*)
		FRM_gbuf_0 : FRM_gbuf; (*Functionblock FRM_gbuf()*)
		FRM_robuf_0 : FRM_robuf; (*Functionblock FRM_robuf()*)
		FRM_write_0 : FRM_write; (*Functionblock FRM_write()*)
		FRM_read_0 : FRM_read; (*Functionblock FRM_read()*)
		FRM_rbuf_0 : FRM_rbuf; (*Functionblock FRM_rbuf()*)
		FRM_close_0 : FRM_close; (*Functionblock FRM_close()*)
		TON_heartbeat : TON;
		TON_timeout : TON;
		TON_robotstate : TON;
	END_STRUCT;
	Communication_Read_typ : 	STRUCT 
		step : enCOMM_READ_STEPS;
	END_STRUCT;
	Command_typ : 	STRUCT 
		connect : BOOL;
		disconnect : BOOL;
	END_STRUCT;
	Buffer_typ : 	STRUCT 
		address : UDINT;
		dataLength : UINT;
		capacity : UINT;
	END_STRUCT;
	Movement_typ : 	STRUCT 
		ImuX : REAL;
		ImuY : REAL;
		ImuZ : REAL;
	END_STRUCT;
END_TYPE
