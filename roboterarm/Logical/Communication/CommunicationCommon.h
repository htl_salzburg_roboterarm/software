#ifndef COMMUNICATION_COMMON_H
#define COMMUNICATION_COMMON_H

#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
#include <AsDefault.h>
#endif

#include <string.h>
#include <protocol.h> //Include Directory muss in den CPU-Properties eingetragen sein, GCC Version auf 6.x

extern Protocol protocol;

void interfaceInit();
void interfaceUpdate();
void interfaceExit();

#endif