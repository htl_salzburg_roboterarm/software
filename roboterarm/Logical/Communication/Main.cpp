
#include "CommunicationCommon.h"

// amount of memory to be allocated for heap storage must be specified for every ANSI C++ program with the bur_heap_size variable
unsigned long bur_heap_size = 0xFFFF;


Protocol protocol;

void handleInitMsg(uint8_t* data, void* user_data) {
	msg_data_init_t* msg = (msg_data_init_t*) data;
	msg_data_init_ack_t ack_msg;
	if(msg->version_code == PROTOCOL_VERSION) {
		comm.status = STATUS_CONNECTION_INIT;
		ack_msg.success = true;
	} else {
		comm.status = STATUS_ERROR_VERSION_MISMATCH;
		ack_msg.success = false;
	}
	protocol.send(&ack_msg);
}

void handleHeartbeatMsg(uint8_t* data, void* user_data) {
	msg_data_heartbeat_t* msg = (msg_data_heartbeat_t*) data;
	comm.controlLinkUp = msg->link_up;
	//Reset heartbeat timer
	comm.TON_timeout.IN = false;
	TON(&comm.TON_timeout);
	comm.heartBeatCounter++;
}

void handleDriveMsg(uint8_t* data, void* user_data) {
	msg_data_drive_velocity_t* msg = (msg_data_drive_velocity_t*) data;
	
	global.drive.move.duration = 200;
	global.drive.move.velocity.x = msg->velocity_x;
	global.drive.move.velocity.y = msg->velocity_y;
	global.drive.move.velocity.a = msg->angle;
}

void handleRoboMoveMsg(uint8_t* data, void* user_data) {
	msg_data_robot_move_t* msg = (msg_data_robot_move_t*) data;
	
	global.robo.move.x = msg->position_x;
	global.robo.move.y = msg->position_y;
	global.robo.move.z = msg->position_z;
}

void handleEnableDriveMsg(uint8_t* data, void* user_data) {
	msg_data_enable_drive_t* msg = (msg_data_enable_drive_t*) data;
	global.drive.enable = msg->enabled;
}

void handleEnableArmMsg(uint8_t* data, void* user_data) {
	msg_data_enable_arm_t* msg = (msg_data_enable_arm_t*) data;
	if(msg->enabled && !msg->try_auto_home) {
		robo_auto_home = false;
	}
	global.robo.enable = msg->enabled;
}

void handleConfirmHomeMsg(uint8_t* data, void* user_data) {
	msg_data_confirm_home_t* msg = (msg_data_confirm_home_t*) data;
	if(global.robo.state == task_state_wait_confirmation) {
		for(int i = 0; i < 5; i++) {
			robo_positive_home_flags[i] = msg->home_flags[i];
		}
		global.robo.confirm_home = true;
	}
}

void handleResetErrorMsg(uint8_t* data, void* user_data) {
	msg_data_reset_error_t* msg = (msg_data_reset_error_t*) data;
	switch(msg->component) {
		case RobotComponent::Drive:
			global.drive.reset_error = true;
			break;
		case RobotComponent::RobotArm:
			global.robo.reset_error = true;
			break;
	}
}

void handleDistanceMsg(uint8_t* data, void* user_data) {
	msg_data_distance_readings_t* msg = (msg_data_distance_readings_t*) data;
	//Only the first 14 sensors (of 16) are used
	for(int i = 0; i < 14; i++) {
		//Convert distance from cm to m
		global.sensors.distances[i] = msg->distances[i] * 0.01;
	}
}

void handleRequestPosition(uint8_t* data, void* user_data) {
	msg_data_robot_move_t pos_msg;
	pos_msg.position_x = global.robo.move.x;
	pos_msg.position_y = global.robo.move.y;
	pos_msg.position_z = global.robo.move.z;
	protocol.send(&pos_msg);
}

void handleTransportMode(uint8_t* data, void* user_data) {
	msg_data_transport_mode_t* msg = (msg_data_transport_mode_t*) data;
	if(msg->mode == TransportMode::Mode1) {
		global.robo.transport_mode = 1;
	} else if(msg->mode == TransportMode::Mode2) {
		global.robo.transport_mode = 2;
	}
}

void _INIT ProgramInit(void)
{
	memset(&comm, 0, sizeof(comm));
	comm.step = COMM_WAIT;
	
	protocol.registerMessageCallback(msg::init, handleInitMsg, nullptr);
	protocol.registerMessageCallback(msg::heartbeat, handleHeartbeatMsg, nullptr);
	protocol.registerMessageCallback(msg::drive_velocity, handleDriveMsg, nullptr);
	protocol.registerMessageCallback(msg::robot_move, handleRoboMoveMsg, nullptr);
	protocol.registerMessageCallback(msg::enable_drive, handleEnableDriveMsg, nullptr);
	protocol.registerMessageCallback(msg::enable_arm, handleEnableArmMsg, nullptr);
	protocol.registerMessageCallback(msg::confirm_home, handleConfirmHomeMsg, nullptr);
	protocol.registerMessageCallback(msg::reset_error, handleResetErrorMsg, nullptr);
	protocol.registerMessageCallback(msg::distance_readings, handleDistanceMsg, nullptr);
	protocol.registerMessageCallback(msg::request_position, handleRequestPosition, nullptr);
	protocol.registerMessageCallback(msg::transport_mode, handleTransportMode, nullptr);
	comm.heartBeatCounter = 0;
	comm.status = STATUS_DISCONNECTED;
	
	interfaceInit();
	
	comm.cmd.connect = true;
}

void _CYCLIC ProgramCyclic(void)
{
	interfaceUpdate();
	switch (comm.status)
	{
		case STATUS_DISCONNECTED:
			break;

		case STATUS_CONNECTION_INIT:
			comm.TON_heartbeat.PT = PROTOCOL_HEARTBEAT_TIME;
			comm.TON_timeout.PT = PROTOCOL_TIMEOUT_TIME;
			comm.heartBeatCounter = 0;
			
			comm.status = STATUS_CONNECTED;
			break;
		
		case STATUS_CONNECTED:
			comm.TON_heartbeat.IN = true;
			TON(&comm.TON_heartbeat);
			if(comm.TON_heartbeat.Q) {
				comm.TON_heartbeat.IN = false;
				TON(&comm.TON_heartbeat);
				msg_data_heartbeat_t msg;
				protocol.send(&msg);
			}
			
			comm.TON_timeout.IN = true;
			TON(&comm.TON_timeout);
			if(comm.TON_timeout.Q) {
				comm.TON_timeout.IN = false;
				TON(&comm.TON_timeout);
				comm.status = STATUS_ERROR_TIMEOUT;
			}
			break;
		
		case STATUS_ERROR_TIMEOUT:
			break;
		
		case STATUS_ERROR_VERSION_MISMATCH:
			break;
	}
	
	if(comm.status == STATUS_CONNECTED) {
		global.comm.receiver_connected = true;
		global.comm.control_connected = comm.controlLinkUp;
		
		comm.TON_robotstate.IN = true;
		comm.TON_robotstate.PT = 500;
		TON(&comm.TON_robotstate);
		if(comm.TON_robotstate.Q) {
			comm.TON_robotstate.IN = false;
			TON(&comm.TON_robotstate);
			msg_data_robot_state_t msg;
			switch (global.drive.state)
			{
				case task_state_off: msg.drive = DriveState::Off; break;
				case task_state_on: msg.drive = DriveState::On; break;
				case task_state_error: msg.drive = DriveState::Error; break;
				default: break; //No other states possible for drive
			}
			switch (global.robo.state)
			{
				case task_state_off: msg.arm = RobotArmState::Off; break;
				case task_state_on: msg.arm = RobotArmState::On; break;
				case task_state_error: msg.arm = RobotArmState::Error; break;
				case task_state_wait_confirmation: msg.arm = RobotArmState::HomeDirectionConfirmation; break;
				case task_state_homing: msg.arm = RobotArmState::Homing; break;
			}
			protocol.send(&msg);
		}
		
	} else {
		global.comm.receiver_connected = false;
		global.comm.control_connected = false;
		comm.controlLinkUp = false;
	}
	
	global.robo.activate = global.comm.control_connected;
}

void _EXIT ProgramExit(void)
{
	interfaceExit();
}
