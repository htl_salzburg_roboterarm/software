(*Global control variables*)

TYPE
	global_vars_typ : 	STRUCT 
		robo : global_robo_typ;
		drive : global_drive_typ;
		battery : global_battery_typ;
		comm : global_comm_typ;
		sensors : global_sensors_typ;
	END_STRUCT;
	global_robo_typ : 	STRUCT 
		enable : BOOL;
		activate : BOOL;
		confirm_home : BOOL;
		reset_error : BOOL;
		state : global_task_state;
		move : robo_move_cmd;
		transport_mode : USINT := 0;
	END_STRUCT;
	global_comm_typ : 	STRUCT 
		enable : BOOL;
		receiver_connected : BOOL;
		control_connected : BOOL;
	END_STRUCT;
	global_drive_typ : 	STRUCT 
		enable : BOOL;
		reset_error : BOOL;
		state : global_task_state;
		move : drive_move_cmd;
	END_STRUCT;
	global_battery_typ : 	STRUCT 
		level : USINT;
	END_STRUCT;
	global_sensors_typ : 	STRUCT 
		distances : ARRAY[0..13]OF REAL;
	END_STRUCT;
END_TYPE

(*Utility Datatypes*)

TYPE
	coordinate_typ : 	STRUCT 
		x : REAL;
		y : REAL;
		a : REAL;
	END_STRUCT;
	robo_move_cmd : 	STRUCT 
		x : REAL;
		y : REAL;
		z : REAL;
	END_STRUCT;
	drive_move_cmd : 	STRUCT 
		velocity : coordinate_typ;
		duration : TIME;
	END_STRUCT;
	global_task_state : 
		(
		task_state_off,
		task_state_on,
		task_state_error,
		task_state_wait_confirmation,
		task_state_homing
		);
END_TYPE
