
FUNCTION Accelerate
	delta := target - current;
	Accelerate := current + LIMIT(-acceleration, delta, +acceleration);
END_FUNCTION
