FUNCTION RestrictVelocity
	restrict := FALSE;
	FOR index := 0 TO 14 DO
		//check if bit in mask is set and sensor value is below threshold
		IF (sensorMask AND SHL(1, index)) <> 0 AND sensorValues[index] <= threshold THEN
			restrict := TRUE;
		END_IF;
	END_FOR;
	
	IF restrict THEN
		RestrictVelocity := Constrain(velocity, restrictMin, restrictMax);
	ELSE
		RestrictVelocity := velocity;
	END_IF;	
END_FUNCTION

FUNCTION RestrictVelocityAll
	velocity.x := RestrictVelocity(sensorValues, sensorMaskFront, threshold, -1.0, 0.0, velocity.x);
	velocity.x := RestrictVelocity(sensorValues, sensorMaskBack, threshold, 0.0, 1.0, velocity.x);
	velocity.y := RestrictVelocity(sensorValues, sensorMaskLeft, threshold, -1.0, 0.0, velocity.y);
	velocity.y := RestrictVelocity(sensorValues, sensorMaskRight, threshold, 0.0, 1.0, velocity.y);
	RestrictVelocityAll := FALSE;
END_FUNCTION