
PROGRAM _INIT
	drive.wheels[0].axis := ADR(gAxisD1);
	drive.wheels[1].axis := ADR(gAxisD2);
	drive.wheels[2].axis := ADR(gAxisD3);
	drive.wheels[3].axis := ADR(gAxisD4);
	
	FOR Index := 0 TO 3 DO
		drive.wheels[Index](init := TRUE);
	END_FOR;
	
	//Setup limit values
	params.acceleration.x := 3.0; // m/s�
	params.acceleration.y := 3.0; // m/s�
	params.acceleration.a := 3.0; // m/s�
	params.max_velocity.x := 1.0; // m/s
	params.max_velocity.y := 1.0; // m/s
	params.max_velocity.a := 1.0; // m/s�
	//Setup mechanical parameters
	params.mechanics.wheel_dist_front := 0.49 * 0.5; // m
	params.mechanics.wheel_dist_side := 0.50 * 0.5; // m
	params.mechanics.wheel_radius := 0.1; // m
	//Setup safety parameters
	params.sensor_restrict_distance := 0.5; // m
	params.sensor_stop_distance := 0.1; // m
	
	params.sensor_mask_right := 0;
	params.sensor_mask_right.0 := TRUE;
	params.sensor_mask_right.1 := TRUE;
	params.sensor_mask_right.2 := TRUE;
	params.sensor_mask_right.3 := TRUE;
	params.sensor_mask_right.4 := TRUE;
	
	params.sensor_mask_back := 0;
	params.sensor_mask_back.4 := TRUE;
	params.sensor_mask_back.5 := TRUE;
	params.sensor_mask_back.6 := TRUE;
	params.sensor_mask_back.7 := TRUE;
	
	params.sensor_mask_left := 0;
	params.sensor_mask_left.7 := TRUE;
	params.sensor_mask_left.8 := TRUE;
	params.sensor_mask_left.9 := TRUE;
	params.sensor_mask_left.10 := TRUE;
	params.sensor_mask_left.11 := TRUE;
	
	params.sensor_mask_front := 0;
	params.sensor_mask_front.11 := TRUE;
	params.sensor_mask_front.12 := TRUE;
	params.sensor_mask_front.13 := TRUE;
	params.sensor_mask_front.0 := TRUE;
	
	//Initialize position
	drive.location.x := 0;
	drive.location.y := 0;
	drive.location.a := 0;
END_PROGRAM

PROGRAM _CYCLIC
	IF global.drive.reset_error THEN
		drive.error_reset := TRUE;
	END_IF;
	
	//Move command processing
	IF global.drive.move.duration <> 0 THEN
		drive.move_command_timer(IN := FALSE, PT := global.drive.move.duration); //Reset timer and set new duration
		global.drive.move.duration := 0;
		drive.target_velocity.x := Constrain(global.drive.move.velocity.x, -100.0, 100.0) / 100.0 * params.max_velocity.x;
		drive.target_velocity.y := Constrain(global.drive.move.velocity.y, -100.0, 100.0) / 100.0 * params.max_velocity.y;
		drive.target_velocity.a := Constrain(global.drive.move.velocity.a, -100.0, 100.0) / 100.0 * params.max_velocity.a;
		drive.move_command_timer.IN := TRUE; //Start move timer
	ELSE
		drive.move_command_timer(); //Execute timer
		IF drive.move_command_timer.Q THEN
			drive.move_command_timer.IN := FALSE; //Stop move timer
			drive.target_velocity.x := 0;
			drive.target_velocity.y := 0;
			drive.target_velocity.a := 0;
		END_IF;
	END_IF;
	
				
	RestrictVelocityAll(
		global.sensors.distances, params.sensor_mask_front, params.sensor_mask_back, params.sensor_mask_left, params.sensor_mask_right,
		params.sensor_restrict_distance, drive.target_velocity
	);
		
	
	Count := 0;
	FOR Index := 0 TO 3 DO
		drive.wheels[Index].power := global.drive.enable;
		IF drive.wheels[Index].running THEN
			Count := Count + 1;
		END_IF;
		IF drive.wheels[Index].error THEN
			drive.state := drive_error;
		END_IF;
	END_FOR;
	
	CASE drive.state OF
		drive_reset:
			IF Count = 4 THEN
				drive.state := drive_running;
			END_IF;
		
		drive_running:
			//Distance emergency stop
			FOR Index := 0 TO 13 DO
				IF global.sensors.distances[Index] < params.sensor_stop_distance THEN
					drive.state := drive_error;
				END_IF;
			END_FOR;
			
			IF Count <> 4 THEN
				drive.state := drive_reset;
			ELSE
				//Integrate location from last cycle
				IF drive.global_linear_velocity THEN
					//Direct integration
					drive.location.x := drive.location.x + drive.current_velocity.x * DELTA_T;
					drive.location.y := drive.location.y + drive.current_velocity.y * DELTA_T;
					drive.location.a := drive.location.a + drive.current_velocity.a * DELTA_T;
				ELSE
					//Convert local velocity vector to global velocity and integrate (= rotate by 'a' counterclockwise)
					tmp.sin := SIN(drive.location.a);
					tmp.cos := COS(drive.location.a);
					drive.location.x := drive.location.x + (drive.current_velocity.x * tmp.cos - drive.current_velocity.y * tmp.sin) * DELTA_T;
					drive.location.y := drive.location.y + (drive.current_velocity.x * tmp.sin + drive.current_velocity.y * tmp.cos) * DELTA_T;
					drive.location.a := drive.location.a + (drive.current_velocity.a) * DELTA_T;
				END_IF;
				
				IF drive.location.a < -PI THEN
					drive.location.a := drive.location.a + 2*PI;
				ELSIF drive.location.a > PI THEN
					drive.location.a := drive.location.a - 2*PI;
				END_IF;
				//End location integration
				
				//Acceleration limitation
				drive.current_velocity.x := Accelerate(drive.current_velocity.x, drive.target_velocity.x, params.acceleration.x * DELTA_T);
				drive.current_velocity.y := Accelerate(drive.current_velocity.y, drive.target_velocity.y, params.acceleration.y * DELTA_T);
				drive.current_velocity.a := Accelerate(drive.current_velocity.a, drive.target_velocity.a, params.acceleration.a * DELTA_T);
				//End Acceleration limitation
				
				//Local linear velocity calculation
				IF drive.global_linear_velocity THEN
					//Convert global velocity vector to local vector (= rotate by 'a' clockwise)
					tmp.sin := SIN(-drive.location.a);
					tmp.cos := COS(-drive.location.a);
					tmp.local_velocity.x := drive.current_velocity.x * tmp.cos - drive.current_velocity.y * tmp.sin;
					tmp.local_velocity.y := drive.current_velocity.x * tmp.sin + drive.current_velocity.y * tmp.cos;
					tmp.local_velocity.a := drive.current_velocity.a;
				ELSE
					tmp.local_velocity := drive.current_velocity; //Directly set local velocity
				END_IF;
				//End local linear velocity calculation
				
				//Wheel velocity calculation
				(*
				positive x -> forward
				positive y -> left
				
				|w1|         |+1 +1 -(a+b)|   |vx|
				|w2| = 1/R * |+1 -1 +(a+b)| * |vy|
				|w3|         |+1 -1 -(a+b)|   | w|
				|w4|         |+1 +1 +(a+b)|
				*)
				tmp.lengths := params.mechanics.wheel_dist_front + params.mechanics.wheel_dist_side; //a + b
				tmp.v_omega := tmp.local_velocity.a * tmp.lengths; //omega * (a + b)
				tmp.inv_radius := 1.0 / params.mechanics.wheel_radius; // 1 / R
				
				drive.wheels[0].velocity := RAD_TO_DEG * tmp.inv_radius * (+ tmp.local_velocity.x + tmp.local_velocity.y - tmp.v_omega);
				drive.wheels[1].velocity := RAD_TO_DEG * tmp.inv_radius * (+ tmp.local_velocity.x - tmp.local_velocity.y + tmp.v_omega);
				drive.wheels[2].velocity := RAD_TO_DEG * tmp.inv_radius * (+ tmp.local_velocity.x - tmp.local_velocity.y - tmp.v_omega);
				drive.wheels[3].velocity := RAD_TO_DEG * tmp.inv_radius * (+ tmp.local_velocity.x + tmp.local_velocity.y + tmp.v_omega);
				//End wheel velocity calculation
			END_IF;
		
		drive_error:
			global.drive.enable := FALSE;
			IF drive.error_reset THEN
				drive.error_reset := FALSE;
				drive.state := drive_reset;
				
				FOR Index := 0 TO 3 DO
					IF drive.wheels[Index].error THEN
						drive.wheels[Index].error_reset := TRUE;
						drive.error_reset := TRUE;
						drive.state := drive_error;
					ELSE
						drive.wheels[Index].error_reset := FALSE; //Reset wheel error_reset
					END_IF;
				END_FOR;
			END_IF;
				

	END_CASE;
	
	FOR Index := 0 TO 3 DO
		drive.wheels[Index]();
	END_FOR;
	
	IF drive.state = drive_running THEN
		global.drive.state := task_state_on;
	ELSIF drive.state = drive_error THEN
		global.drive.state := task_state_error;
	ELSE
		global.drive.state := task_state_off;
	END_IF;
	
	//Reset one-time commands
	global.drive.reset_error := FALSE;
	
END_PROGRAM

PROGRAM _EXIT
	FOR Index := 0 TO 3 DO
		drive.wheels[Index](shutdown := TRUE);
	END_FOR;
END_PROGRAM

