
(* TODO: Add your comment here *)
FUNCTION_BLOCK Wheel
	IF init THEN
		init := FALSE;
		state := wheel_reset;
		
		MpAxis.MpLink := axis;
		MpAxis.Enable := TRUE;
		MpAxis.Parameters := ADR(Parameters);
		
		Parameters.Position := 100.0;
		Parameters.Velocity := 10.0;
		Parameters.Acceleration := 50.0;
		Parameters.Deceleration := 50.0;
		
		McCyclicVelocity.Axis := axis;
		McCyclicVelocity.CyclicVelocity := 0.0;
		
		McAutoTunePos.Axis := axis;
		//McAutoTunePos.Parameters.MaxCurrentPercent := 70;
		//McAutoTunePos.Parameters.MaxDistance := 720;
		//McAutoTunePos.Parameters.MaxPositionError := 720;
		
		McAutoTuneVel.Axis := axis;
	END_IF;
	
	IF shutdown THEN
		shutdown := FALSE;
		state := wheel_shutdown;
		
		MpAxis.Power  		:= FALSE;
		MpAxis.Home   		:= FALSE;
		MpAxis.MoveAbsolute := FALSE;
		MpAxis.MoveAdditive := FALSE;
		MpAxis.MoveVelocity := FALSE;
		MpAxis.Stop         := FALSE;
		MpAxis.ErrorReset   := FALSE;
		McCyclicVelocity.Enable := FALSE;
		MpAxis();
		McCyclicVelocity();
		MpAxis.Enable := FALSE;
		MpAxis();
	END_IF;
	
	IF state <> wheel_shutdown THEN
		IF MpAxis.Error THEN
			state := wheel_error;
		END_IF;
		
		IF NOT power AND state <> wheel_error AND state <> wheel_tune_pos AND state <> wheel_tune_vel THEN
			state := wheel_reset;
		END_IF;
		
		CASE state OF
			wheel_reset:
				MpAxis.Power  		:= FALSE;
				MpAxis.Home   		:= FALSE;
				MpAxis.MoveAbsolute := FALSE;
				MpAxis.MoveAdditive := FALSE;
				MpAxis.MoveVelocity := FALSE;
				MpAxis.Stop         := FALSE;
				MpAxis.ErrorReset   := FALSE;
				McCyclicVelocity.Enable := FALSE;
				IF power THEN
					state := wheel_power_on;
				ELSIF tune THEN
					tune := FALSE;
					state := wheel_tune_vel;
				END_IF;
				
			wheel_tune_pos:
				McAutoTunePos.Execute := TRUE;
				IF McAutoTunePos.Done THEN
					McAutoTunePos.Execute := FALSE;
					state := wheel_reset;
				END_IF;
				
			wheel_tune_vel:
				McAutoTuneVel.Execute := TRUE;
				IF McAutoTuneVel.Done THEN
					McAutoTuneVel.Execute := FALSE;
					state := wheel_tune_pos;
				END_IF;
		
			wheel_power_on:
				MpAxis.Power := TRUE;
				IF MpAxis.PowerOn THEN
					state := wheel_home;
				END_IF;
		
			wheel_home:
				IF MpAxis.IsHomed THEN
					state := wheel_running;
					MpAxis.Home := FALSE;
				ELSE
					MpAxis.Home := TRUE;
				END_IF;
			
			wheel_running:
				McCyclicVelocity.Enable := TRUE;
				McCyclicVelocity.CyclicVelocity := velocity;
			
			wheel_error:
				MpAxis.Power  		:= FALSE;
				MpAxis.Home   		:= FALSE;
				MpAxis.MoveAbsolute := FALSE;
				MpAxis.MoveAdditive := FALSE;
				MpAxis.MoveVelocity := FALSE;
				MpAxis.Stop         := FALSE;
				MpAxis.ErrorReset   := FALSE;
				McCyclicVelocity.Enable := FALSE;
				IF error_reset THEN
					MpAxis.ErrorReset := TRUE;
				END_IF;
				IF NOT MpAxis.Error THEN
					MpAxis.ErrorReset := FALSE;
					state := wheel_reset;
				END_IF;
			
		END_CASE;
		
		error := state = wheel_error;
		running := state = wheel_running;
		
		MpAxis();
		McCyclicVelocity();
		McAutoTunePos();
		McAutoTuneVel();
	END_IF;
	
END_FUNCTION_BLOCK
