
TYPE
	wheel_state : 
		(
		wheel_reset,
		wheel_power_on,
		wheel_home,
		wheel_running,
		wheel_error,
		wheel_shutdown,
		wheel_tune_pos,
		wheel_tune_vel
		);
	drive_state : 
		(
		drive_reset,
		drive_running,
		drive_error
		);
	mechanics_typ : 	STRUCT 
		wheel_dist_side : REAL;
		wheel_dist_front : REAL;
		wheel_radius : REAL;
	END_STRUCT;
	params_typ : 	STRUCT 
		acceleration : coordinate_typ;
		deceleration : coordinate_typ;
		mechanics : mechanics_typ;
		max_velocity : coordinate_typ;
		sensor_restrict_distance : REAL; (*Stops movement in this direction*)
		sensor_stop_distance : REAL; (*Executes an emergency stop*)
		sensor_mask_front : WORD;
		sensor_mask_back : WORD;
		sensor_mask_left : WORD;
		sensor_mask_right : WORD;
	END_STRUCT;
	drive_tmp_typ : 	STRUCT 
		index : USINT;
		delta_v : coordinate_typ;
		sin : REAL;
		cos : REAL;
		lengths : REAL;
		inv_radius : REAL;
		v_omega : REAL;
		local_velocity : coordinate_typ;
	END_STRUCT;
	drive_typ : 	STRUCT 
		wheels : ARRAY[0..3]OF Wheel;
		error_reset : BOOL;
		global_linear_velocity : BOOL;
		target_velocity : coordinate_typ;
		current_velocity : coordinate_typ;
		location : coordinate_typ;
		state : drive_state;
		move_command_timer : TON;
	END_STRUCT;
END_TYPE
