
{REDUND_ERROR} FUNCTION_BLOCK Wheel (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		axis : UDINT;
		velocity : {REDUND_UNREPLICABLE} REAL;
		power : BOOL;
		init : BOOL;
		shutdown : BOOL;
		error_reset : BOOL;
		tune : BOOL;
	END_VAR
	VAR_OUTPUT
		error : BOOL;
		running : BOOL;
	END_VAR
	VAR
		state : wheel_state := wheel_shutdown;
		MpAxis : MpAxisBasic;
		McCyclicVelocity : MC_BR_MoveCyclicVelocity;
		Parameters : MpAxisBasicParType;
		McAutoTunePos : MC_BR_AutoTunePositionCtrl_AcpAx;
		McAutoTuneVel : MC_BR_AutoTuneSpeedCtrl_AcpAx;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION Accelerate : REAL (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		current : REAL;
		target : REAL;
		acceleration : REAL;
	END_VAR
	VAR
		delta : REAL;
	END_VAR
END_FUNCTION

{REDUND_ERROR} FUNCTION Constrain : REAL (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		input : REAL;
		minVal : REAL;
		maxVal : REAL;
	END_VAR
END_FUNCTION

FUNCTION RestrictVelocity : REAL
	VAR_INPUT
		sensorValues : ARRAY[0..13] OF REAL;
		sensorMask : WORD;
		threshold : REAL;
		restrictMin : REAL;
		restrictMax : REAL;
		velocity : REAL;
	END_VAR
	VAR
		restrict : BOOL;
		index : USINT;
	END_VAR
END_FUNCTION

FUNCTION RestrictVelocityAll : BOOL
	VAR_INPUT
		sensorValues : ARRAY[0..13] OF REAL;
		sensorMaskFront : WORD;
		sensorMaskBack : WORD;
		sensorMaskLeft : WORD;
		sensorMaskRight : WORD;
		threshold : REAL;
	END_VAR
	VAR_IN_OUT
		velocity : coordinate_typ;
	END_VAR
END_FUNCTION
